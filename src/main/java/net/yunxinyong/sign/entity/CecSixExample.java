package net.yunxinyong.sign.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CecSixExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CecSixExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNull() {
            addCriterion("inv_id is null");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNotNull() {
            addCriterion("inv_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvIdEqualTo(Integer value) {
            addCriterion("inv_id =", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotEqualTo(Integer value) {
            addCriterion("inv_id <>", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThan(Integer value) {
            addCriterion("inv_id >", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("inv_id >=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThan(Integer value) {
            addCriterion("inv_id <", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThanOrEqualTo(Integer value) {
            addCriterion("inv_id <=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdIn(List<Integer> values) {
            addCriterion("inv_id in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotIn(List<Integer> values) {
            addCriterion("inv_id not in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdBetween(Integer value1, Integer value2) {
            addCriterion("inv_id between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotBetween(Integer value1, Integer value2) {
            addCriterion("inv_id not between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNull() {
            addCriterion("social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNotNull() {
            addCriterion("social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeEqualTo(String value) {
            addCriterion("social_credit_code =", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotEqualTo(String value) {
            addCriterion("social_credit_code <>", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThan(String value) {
            addCriterion("social_credit_code >", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("social_credit_code >=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThan(String value) {
            addCriterion("social_credit_code <", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("social_credit_code <=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLike(String value) {
            addCriterion("social_credit_code like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotLike(String value) {
            addCriterion("social_credit_code not like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIn(List<String> values) {
            addCriterion("social_credit_code in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotIn(List<String> values) {
            addCriterion("social_credit_code not in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("social_credit_code between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("social_credit_code not between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNull() {
            addCriterion("organization_code is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNotNull() {
            addCriterion("organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeEqualTo(String value) {
            addCriterion("organization_code =", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotEqualTo(String value) {
            addCriterion("organization_code <>", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThan(String value) {
            addCriterion("organization_code >", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("organization_code >=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThan(String value) {
            addCriterion("organization_code <", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("organization_code <=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLike(String value) {
            addCriterion("organization_code like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotLike(String value) {
            addCriterion("organization_code not like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIn(List<String> values) {
            addCriterion("organization_code in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotIn(List<String> values) {
            addCriterion("organization_code not in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeBetween(String value1, String value2) {
            addCriterion("organization_code between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("organization_code not between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNull() {
            addCriterion("unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNotNull() {
            addCriterion("unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameEqualTo(String value) {
            addCriterion("unit_detailed_name =", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotEqualTo(String value) {
            addCriterion("unit_detailed_name <>", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThan(String value) {
            addCriterion("unit_detailed_name >", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name >=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThan(String value) {
            addCriterion("unit_detailed_name <", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name <=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLike(String value) {
            addCriterion("unit_detailed_name like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotLike(String value) {
            addCriterion("unit_detailed_name not like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIn(List<String> values) {
            addCriterion("unit_detailed_name in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotIn(List<String> values) {
            addCriterion("unit_detailed_name not in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("unit_detailed_name between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("unit_detailed_name not between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBeginStockIsNull() {
            addCriterion("begin_stock is null");
            return (Criteria) this;
        }

        public Criteria andBeginStockIsNotNull() {
            addCriterion("begin_stock is not null");
            return (Criteria) this;
        }

        public Criteria andBeginStockEqualTo(BigDecimal value) {
            addCriterion("begin_stock =", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockNotEqualTo(BigDecimal value) {
            addCriterion("begin_stock <>", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockGreaterThan(BigDecimal value) {
            addCriterion("begin_stock >", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("begin_stock >=", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockLessThan(BigDecimal value) {
            addCriterion("begin_stock <", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockLessThanOrEqualTo(BigDecimal value) {
            addCriterion("begin_stock <=", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockIn(List<BigDecimal> values) {
            addCriterion("begin_stock in", values, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockNotIn(List<BigDecimal> values) {
            addCriterion("begin_stock not in", values, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("begin_stock between", value1, value2, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("begin_stock not between", value1, value2, "beginStock");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsIsNull() {
            addCriterion("total_current_assets is null");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsIsNotNull() {
            addCriterion("total_current_assets is not null");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsEqualTo(BigDecimal value) {
            addCriterion("total_current_assets =", value, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsNotEqualTo(BigDecimal value) {
            addCriterion("total_current_assets <>", value, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsGreaterThan(BigDecimal value) {
            addCriterion("total_current_assets >", value, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_current_assets >=", value, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsLessThan(BigDecimal value) {
            addCriterion("total_current_assets <", value, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_current_assets <=", value, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsIn(List<BigDecimal> values) {
            addCriterion("total_current_assets in", values, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsNotIn(List<BigDecimal> values) {
            addCriterion("total_current_assets not in", values, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_current_assets between", value1, value2, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andTotalCurrentAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_current_assets not between", value1, value2, "totalCurrentAssets");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsIsNull() {
            addCriterion("monetary_funds is null");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsIsNotNull() {
            addCriterion("monetary_funds is not null");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsEqualTo(BigDecimal value) {
            addCriterion("monetary_funds =", value, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsNotEqualTo(BigDecimal value) {
            addCriterion("monetary_funds <>", value, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsGreaterThan(BigDecimal value) {
            addCriterion("monetary_funds >", value, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("monetary_funds >=", value, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsLessThan(BigDecimal value) {
            addCriterion("monetary_funds <", value, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("monetary_funds <=", value, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsIn(List<BigDecimal> values) {
            addCriterion("monetary_funds in", values, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsNotIn(List<BigDecimal> values) {
            addCriterion("monetary_funds not in", values, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("monetary_funds between", value1, value2, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andMonetaryFundsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("monetary_funds not between", value1, value2, "monetaryFunds");
            return (Criteria) this;
        }

        public Criteria andCashIsNull() {
            addCriterion("cash is null");
            return (Criteria) this;
        }

        public Criteria andCashIsNotNull() {
            addCriterion("cash is not null");
            return (Criteria) this;
        }

        public Criteria andCashEqualTo(BigDecimal value) {
            addCriterion("cash =", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashNotEqualTo(BigDecimal value) {
            addCriterion("cash <>", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashGreaterThan(BigDecimal value) {
            addCriterion("cash >", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("cash >=", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashLessThan(BigDecimal value) {
            addCriterion("cash <", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashLessThanOrEqualTo(BigDecimal value) {
            addCriterion("cash <=", value, "cash");
            return (Criteria) this;
        }

        public Criteria andCashIn(List<BigDecimal> values) {
            addCriterion("cash in", values, "cash");
            return (Criteria) this;
        }

        public Criteria andCashNotIn(List<BigDecimal> values) {
            addCriterion("cash not in", values, "cash");
            return (Criteria) this;
        }

        public Criteria andCashBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cash between", value1, value2, "cash");
            return (Criteria) this;
        }

        public Criteria andCashNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cash not between", value1, value2, "cash");
            return (Criteria) this;
        }

        public Criteria andStockIsNull() {
            addCriterion("stock is null");
            return (Criteria) this;
        }

        public Criteria andStockIsNotNull() {
            addCriterion("stock is not null");
            return (Criteria) this;
        }

        public Criteria andStockEqualTo(BigDecimal value) {
            addCriterion("stock =", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotEqualTo(BigDecimal value) {
            addCriterion("stock <>", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThan(BigDecimal value) {
            addCriterion("stock >", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("stock >=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThan(BigDecimal value) {
            addCriterion("stock <", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThanOrEqualTo(BigDecimal value) {
            addCriterion("stock <=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockIn(List<BigDecimal> values) {
            addCriterion("stock in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotIn(List<BigDecimal> values) {
            addCriterion("stock not in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("stock between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("stock not between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andSecuritiesIsNull() {
            addCriterion("securities is null");
            return (Criteria) this;
        }

        public Criteria andSecuritiesIsNotNull() {
            addCriterion("securities is not null");
            return (Criteria) this;
        }

        public Criteria andSecuritiesEqualTo(BigDecimal value) {
            addCriterion("securities =", value, "securities");
            return (Criteria) this;
        }

        public Criteria andSecuritiesNotEqualTo(BigDecimal value) {
            addCriterion("securities <>", value, "securities");
            return (Criteria) this;
        }

        public Criteria andSecuritiesGreaterThan(BigDecimal value) {
            addCriterion("securities >", value, "securities");
            return (Criteria) this;
        }

        public Criteria andSecuritiesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("securities >=", value, "securities");
            return (Criteria) this;
        }

        public Criteria andSecuritiesLessThan(BigDecimal value) {
            addCriterion("securities <", value, "securities");
            return (Criteria) this;
        }

        public Criteria andSecuritiesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("securities <=", value, "securities");
            return (Criteria) this;
        }

        public Criteria andSecuritiesIn(List<BigDecimal> values) {
            addCriterion("securities in", values, "securities");
            return (Criteria) this;
        }

        public Criteria andSecuritiesNotIn(List<BigDecimal> values) {
            addCriterion("securities not in", values, "securities");
            return (Criteria) this;
        }

        public Criteria andSecuritiesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("securities between", value1, value2, "securities");
            return (Criteria) this;
        }

        public Criteria andSecuritiesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("securities not between", value1, value2, "securities");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentIsNull() {
            addCriterion("long_term_investment is null");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentIsNotNull() {
            addCriterion("long_term_investment is not null");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentEqualTo(BigDecimal value) {
            addCriterion("long_term_investment =", value, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentNotEqualTo(BigDecimal value) {
            addCriterion("long_term_investment <>", value, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentGreaterThan(BigDecimal value) {
            addCriterion("long_term_investment >", value, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("long_term_investment >=", value, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentLessThan(BigDecimal value) {
            addCriterion("long_term_investment <", value, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentLessThanOrEqualTo(BigDecimal value) {
            addCriterion("long_term_investment <=", value, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentIn(List<BigDecimal> values) {
            addCriterion("long_term_investment in", values, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentNotIn(List<BigDecimal> values) {
            addCriterion("long_term_investment not in", values, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("long_term_investment between", value1, value2, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andLongTermInvestmentNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("long_term_investment not between", value1, value2, "longTermInvestment");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsIsNull() {
            addCriterion("fixed_assets is null");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsIsNotNull() {
            addCriterion("fixed_assets is not null");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsEqualTo(BigDecimal value) {
            addCriterion("fixed_assets =", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsNotEqualTo(BigDecimal value) {
            addCriterion("fixed_assets <>", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsGreaterThan(BigDecimal value) {
            addCriterion("fixed_assets >", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fixed_assets >=", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsLessThan(BigDecimal value) {
            addCriterion("fixed_assets <", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fixed_assets <=", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsIn(List<BigDecimal> values) {
            addCriterion("fixed_assets in", values, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsNotIn(List<BigDecimal> values) {
            addCriterion("fixed_assets not in", values, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fixed_assets between", value1, value2, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fixed_assets not between", value1, value2, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresIsNull() {
            addCriterion("houses_structures is null");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresIsNotNull() {
            addCriterion("houses_structures is not null");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresEqualTo(BigDecimal value) {
            addCriterion("houses_structures =", value, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresNotEqualTo(BigDecimal value) {
            addCriterion("houses_structures <>", value, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresGreaterThan(BigDecimal value) {
            addCriterion("houses_structures >", value, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("houses_structures >=", value, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresLessThan(BigDecimal value) {
            addCriterion("houses_structures <", value, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresLessThanOrEqualTo(BigDecimal value) {
            addCriterion("houses_structures <=", value, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresIn(List<BigDecimal> values) {
            addCriterion("houses_structures in", values, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresNotIn(List<BigDecimal> values) {
            addCriterion("houses_structures not in", values, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("houses_structures between", value1, value2, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andHousesStructuresNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("houses_structures not between", value1, value2, "housesStructures");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentIsNull() {
            addCriterion("machinery_equipment is null");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentIsNotNull() {
            addCriterion("machinery_equipment is not null");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentEqualTo(BigDecimal value) {
            addCriterion("machinery_equipment =", value, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentNotEqualTo(BigDecimal value) {
            addCriterion("machinery_equipment <>", value, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentGreaterThan(BigDecimal value) {
            addCriterion("machinery_equipment >", value, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("machinery_equipment >=", value, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentLessThan(BigDecimal value) {
            addCriterion("machinery_equipment <", value, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentLessThanOrEqualTo(BigDecimal value) {
            addCriterion("machinery_equipment <=", value, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentIn(List<BigDecimal> values) {
            addCriterion("machinery_equipment in", values, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentNotIn(List<BigDecimal> values) {
            addCriterion("machinery_equipment not in", values, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("machinery_equipment between", value1, value2, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andMachineryEquipmentNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("machinery_equipment not between", value1, value2, "machineryEquipment");
            return (Criteria) this;
        }

        public Criteria andConveyanceIsNull() {
            addCriterion("conveyance is null");
            return (Criteria) this;
        }

        public Criteria andConveyanceIsNotNull() {
            addCriterion("conveyance is not null");
            return (Criteria) this;
        }

        public Criteria andConveyanceEqualTo(BigDecimal value) {
            addCriterion("conveyance =", value, "conveyance");
            return (Criteria) this;
        }

        public Criteria andConveyanceNotEqualTo(BigDecimal value) {
            addCriterion("conveyance <>", value, "conveyance");
            return (Criteria) this;
        }

        public Criteria andConveyanceGreaterThan(BigDecimal value) {
            addCriterion("conveyance >", value, "conveyance");
            return (Criteria) this;
        }

        public Criteria andConveyanceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("conveyance >=", value, "conveyance");
            return (Criteria) this;
        }

        public Criteria andConveyanceLessThan(BigDecimal value) {
            addCriterion("conveyance <", value, "conveyance");
            return (Criteria) this;
        }

        public Criteria andConveyanceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("conveyance <=", value, "conveyance");
            return (Criteria) this;
        }

        public Criteria andConveyanceIn(List<BigDecimal> values) {
            addCriterion("conveyance in", values, "conveyance");
            return (Criteria) this;
        }

        public Criteria andConveyanceNotIn(List<BigDecimal> values) {
            addCriterion("conveyance not in", values, "conveyance");
            return (Criteria) this;
        }

        public Criteria andConveyanceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("conveyance between", value1, value2, "conveyance");
            return (Criteria) this;
        }

        public Criteria andConveyanceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("conveyance not between", value1, value2, "conveyance");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseIsNull() {
            addCriterion("fixed_assets_under_financing_lease is null");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseIsNotNull() {
            addCriterion("fixed_assets_under_financing_lease is not null");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseEqualTo(BigDecimal value) {
            addCriterion("fixed_assets_under_financing_lease =", value, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseNotEqualTo(BigDecimal value) {
            addCriterion("fixed_assets_under_financing_lease <>", value, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseGreaterThan(BigDecimal value) {
            addCriterion("fixed_assets_under_financing_lease >", value, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fixed_assets_under_financing_lease >=", value, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseLessThan(BigDecimal value) {
            addCriterion("fixed_assets_under_financing_lease <", value, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fixed_assets_under_financing_lease <=", value, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseIn(List<BigDecimal> values) {
            addCriterion("fixed_assets_under_financing_lease in", values, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseNotIn(List<BigDecimal> values) {
            addCriterion("fixed_assets_under_financing_lease not in", values, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fixed_assets_under_financing_lease between", value1, value2, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsUnderFinancingLeaseNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fixed_assets_under_financing_lease not between", value1, value2, "fixedAssetsUnderFinancingLease");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectIsNull() {
            addCriterion("construction_project is null");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectIsNotNull() {
            addCriterion("construction_project is not null");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectEqualTo(BigDecimal value) {
            addCriterion("construction_project =", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectNotEqualTo(BigDecimal value) {
            addCriterion("construction_project <>", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectGreaterThan(BigDecimal value) {
            addCriterion("construction_project >", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("construction_project >=", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectLessThan(BigDecimal value) {
            addCriterion("construction_project <", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectLessThanOrEqualTo(BigDecimal value) {
            addCriterion("construction_project <=", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectIn(List<BigDecimal> values) {
            addCriterion("construction_project in", values, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectNotIn(List<BigDecimal> values) {
            addCriterion("construction_project not in", values, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("construction_project between", value1, value2, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("construction_project not between", value1, value2, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsIsNull() {
            addCriterion("cultural_assets is null");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsIsNotNull() {
            addCriterion("cultural_assets is not null");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsEqualTo(BigDecimal value) {
            addCriterion("cultural_assets =", value, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsNotEqualTo(BigDecimal value) {
            addCriterion("cultural_assets <>", value, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsGreaterThan(BigDecimal value) {
            addCriterion("cultural_assets >", value, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("cultural_assets >=", value, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsLessThan(BigDecimal value) {
            addCriterion("cultural_assets <", value, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("cultural_assets <=", value, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsIn(List<BigDecimal> values) {
            addCriterion("cultural_assets in", values, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsNotIn(List<BigDecimal> values) {
            addCriterion("cultural_assets not in", values, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cultural_assets between", value1, value2, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andCulturalAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cultural_assets not between", value1, value2, "culturalAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsIsNull() {
            addCriterion("intangible_assets is null");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsIsNotNull() {
            addCriterion("intangible_assets is not null");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsEqualTo(BigDecimal value) {
            addCriterion("intangible_assets =", value, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsNotEqualTo(BigDecimal value) {
            addCriterion("intangible_assets <>", value, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsGreaterThan(BigDecimal value) {
            addCriterion("intangible_assets >", value, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("intangible_assets >=", value, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsLessThan(BigDecimal value) {
            addCriterion("intangible_assets <", value, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("intangible_assets <=", value, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsIn(List<BigDecimal> values) {
            addCriterion("intangible_assets in", values, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsNotIn(List<BigDecimal> values) {
            addCriterion("intangible_assets not in", values, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("intangible_assets between", value1, value2, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andIntangibleAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("intangible_assets not between", value1, value2, "intangibleAssets");
            return (Criteria) this;
        }

        public Criteria andLandUseRightIsNull() {
            addCriterion("land_use_right is null");
            return (Criteria) this;
        }

        public Criteria andLandUseRightIsNotNull() {
            addCriterion("land_use_right is not null");
            return (Criteria) this;
        }

        public Criteria andLandUseRightEqualTo(BigDecimal value) {
            addCriterion("land_use_right =", value, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andLandUseRightNotEqualTo(BigDecimal value) {
            addCriterion("land_use_right <>", value, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andLandUseRightGreaterThan(BigDecimal value) {
            addCriterion("land_use_right >", value, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andLandUseRightGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("land_use_right >=", value, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andLandUseRightLessThan(BigDecimal value) {
            addCriterion("land_use_right <", value, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andLandUseRightLessThanOrEqualTo(BigDecimal value) {
            addCriterion("land_use_right <=", value, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andLandUseRightIn(List<BigDecimal> values) {
            addCriterion("land_use_right in", values, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andLandUseRightNotIn(List<BigDecimal> values) {
            addCriterion("land_use_right not in", values, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andLandUseRightBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("land_use_right between", value1, value2, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andLandUseRightNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("land_use_right not between", value1, value2, "landUseRight");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNull() {
            addCriterion("total_assets is null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNotNull() {
            addCriterion("total_assets is not null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsEqualTo(BigDecimal value) {
            addCriterion("total_assets =", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotEqualTo(BigDecimal value) {
            addCriterion("total_assets <>", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThan(BigDecimal value) {
            addCriterion("total_assets >", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_assets >=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThan(BigDecimal value) {
            addCriterion("total_assets <", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_assets <=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIn(List<BigDecimal> values) {
            addCriterion("total_assets in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotIn(List<BigDecimal> values) {
            addCriterion("total_assets not in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_assets between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_assets not between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalDebtIsNull() {
            addCriterion("total_debt is null");
            return (Criteria) this;
        }

        public Criteria andTotalDebtIsNotNull() {
            addCriterion("total_debt is not null");
            return (Criteria) this;
        }

        public Criteria andTotalDebtEqualTo(BigDecimal value) {
            addCriterion("total_debt =", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtNotEqualTo(BigDecimal value) {
            addCriterion("total_debt <>", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtGreaterThan(BigDecimal value) {
            addCriterion("total_debt >", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_debt >=", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtLessThan(BigDecimal value) {
            addCriterion("total_debt <", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_debt <=", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtIn(List<BigDecimal> values) {
            addCriterion("total_debt in", values, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtNotIn(List<BigDecimal> values) {
            addCriterion("total_debt not in", values, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_debt between", value1, value2, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_debt not between", value1, value2, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsIsNull() {
            addCriterion("total_net_assets is null");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsIsNotNull() {
            addCriterion("total_net_assets is not null");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsEqualTo(BigDecimal value) {
            addCriterion("total_net_assets =", value, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsNotEqualTo(BigDecimal value) {
            addCriterion("total_net_assets <>", value, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsGreaterThan(BigDecimal value) {
            addCriterion("total_net_assets >", value, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_net_assets >=", value, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsLessThan(BigDecimal value) {
            addCriterion("total_net_assets <", value, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_net_assets <=", value, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsIn(List<BigDecimal> values) {
            addCriterion("total_net_assets in", values, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsNotIn(List<BigDecimal> values) {
            addCriterion("total_net_assets not in", values, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_net_assets between", value1, value2, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalNetAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_net_assets not between", value1, value2, "totalNetAssets");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeIsNull() {
            addCriterion("total_income is null");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeIsNotNull() {
            addCriterion("total_income is not null");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeEqualTo(BigDecimal value) {
            addCriterion("total_income =", value, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeNotEqualTo(BigDecimal value) {
            addCriterion("total_income <>", value, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeGreaterThan(BigDecimal value) {
            addCriterion("total_income >", value, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_income >=", value, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeLessThan(BigDecimal value) {
            addCriterion("total_income <", value, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_income <=", value, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeIn(List<BigDecimal> values) {
            addCriterion("total_income in", values, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeNotIn(List<BigDecimal> values) {
            addCriterion("total_income not in", values, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_income between", value1, value2, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andTotalIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_income not between", value1, value2, "totalIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeIsNull() {
            addCriterion("donation_income is null");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeIsNotNull() {
            addCriterion("donation_income is not null");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeEqualTo(BigDecimal value) {
            addCriterion("donation_income =", value, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeNotEqualTo(BigDecimal value) {
            addCriterion("donation_income <>", value, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeGreaterThan(BigDecimal value) {
            addCriterion("donation_income >", value, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("donation_income >=", value, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeLessThan(BigDecimal value) {
            addCriterion("donation_income <", value, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("donation_income <=", value, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeIn(List<BigDecimal> values) {
            addCriterion("donation_income in", values, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeNotIn(List<BigDecimal> values) {
            addCriterion("donation_income not in", values, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("donation_income between", value1, value2, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andDonationIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("donation_income not between", value1, value2, "donationIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeIsNull() {
            addCriterion("fee_income is null");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeIsNotNull() {
            addCriterion("fee_income is not null");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeEqualTo(BigDecimal value) {
            addCriterion("fee_income =", value, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeNotEqualTo(BigDecimal value) {
            addCriterion("fee_income <>", value, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeGreaterThan(BigDecimal value) {
            addCriterion("fee_income >", value, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fee_income >=", value, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeLessThan(BigDecimal value) {
            addCriterion("fee_income <", value, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fee_income <=", value, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeIn(List<BigDecimal> values) {
            addCriterion("fee_income in", values, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeNotIn(List<BigDecimal> values) {
            addCriterion("fee_income not in", values, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee_income between", value1, value2, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andFeeIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee_income not between", value1, value2, "feeIncome");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureIsNull() {
            addCriterion("total_expenditure is null");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureIsNotNull() {
            addCriterion("total_expenditure is not null");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureEqualTo(BigDecimal value) {
            addCriterion("total_expenditure =", value, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureNotEqualTo(BigDecimal value) {
            addCriterion("total_expenditure <>", value, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureGreaterThan(BigDecimal value) {
            addCriterion("total_expenditure >", value, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_expenditure >=", value, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureLessThan(BigDecimal value) {
            addCriterion("total_expenditure <", value, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_expenditure <=", value, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureIn(List<BigDecimal> values) {
            addCriterion("total_expenditure in", values, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureNotIn(List<BigDecimal> values) {
            addCriterion("total_expenditure not in", values, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_expenditure between", value1, value2, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andTotalExpenditureNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_expenditure not between", value1, value2, "totalExpenditure");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostIsNull() {
            addCriterion("business_activity_cost is null");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostIsNotNull() {
            addCriterion("business_activity_cost is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostEqualTo(BigDecimal value) {
            addCriterion("business_activity_cost =", value, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostNotEqualTo(BigDecimal value) {
            addCriterion("business_activity_cost <>", value, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostGreaterThan(BigDecimal value) {
            addCriterion("business_activity_cost >", value, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("business_activity_cost >=", value, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostLessThan(BigDecimal value) {
            addCriterion("business_activity_cost <", value, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("business_activity_cost <=", value, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostIn(List<BigDecimal> values) {
            addCriterion("business_activity_cost in", values, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostNotIn(List<BigDecimal> values) {
            addCriterion("business_activity_cost not in", values, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("business_activity_cost between", value1, value2, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andBusinessActivityCostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("business_activity_cost not between", value1, value2, "businessActivityCost");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsIsNull() {
            addCriterion("activity_personnel_costs is null");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsIsNotNull() {
            addCriterion("activity_personnel_costs is not null");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsEqualTo(BigDecimal value) {
            addCriterion("activity_personnel_costs =", value, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsNotEqualTo(BigDecimal value) {
            addCriterion("activity_personnel_costs <>", value, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsGreaterThan(BigDecimal value) {
            addCriterion("activity_personnel_costs >", value, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_personnel_costs >=", value, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsLessThan(BigDecimal value) {
            addCriterion("activity_personnel_costs <", value, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_personnel_costs <=", value, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsIn(List<BigDecimal> values) {
            addCriterion("activity_personnel_costs in", values, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsNotIn(List<BigDecimal> values) {
            addCriterion("activity_personnel_costs not in", values, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_personnel_costs between", value1, value2, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityPersonnelCostsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_personnel_costs not between", value1, value2, "activityPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesIsNull() {
            addCriterion("activity_daily_expenses is null");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesIsNotNull() {
            addCriterion("activity_daily_expenses is not null");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesEqualTo(BigDecimal value) {
            addCriterion("activity_daily_expenses =", value, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesNotEqualTo(BigDecimal value) {
            addCriterion("activity_daily_expenses <>", value, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesGreaterThan(BigDecimal value) {
            addCriterion("activity_daily_expenses >", value, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_daily_expenses >=", value, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesLessThan(BigDecimal value) {
            addCriterion("activity_daily_expenses <", value, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_daily_expenses <=", value, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesIn(List<BigDecimal> values) {
            addCriterion("activity_daily_expenses in", values, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesNotIn(List<BigDecimal> values) {
            addCriterion("activity_daily_expenses not in", values, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_daily_expenses between", value1, value2, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityDailyExpensesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_daily_expenses not between", value1, value2, "activityDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationIsNull() {
            addCriterion("activity_fixed_assets_depreciation is null");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationIsNotNull() {
            addCriterion("activity_fixed_assets_depreciation is not null");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationEqualTo(BigDecimal value) {
            addCriterion("activity_fixed_assets_depreciation =", value, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationNotEqualTo(BigDecimal value) {
            addCriterion("activity_fixed_assets_depreciation <>", value, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationGreaterThan(BigDecimal value) {
            addCriterion("activity_fixed_assets_depreciation >", value, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_fixed_assets_depreciation >=", value, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationLessThan(BigDecimal value) {
            addCriterion("activity_fixed_assets_depreciation <", value, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationLessThanOrEqualTo(BigDecimal value) {
            addCriterion("activity_fixed_assets_depreciation <=", value, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationIn(List<BigDecimal> values) {
            addCriterion("activity_fixed_assets_depreciation in", values, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationNotIn(List<BigDecimal> values) {
            addCriterion("activity_fixed_assets_depreciation not in", values, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_fixed_assets_depreciation between", value1, value2, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andActivityFixedAssetsDepreciationNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activity_fixed_assets_depreciation not between", value1, value2, "activityFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andTaxIsNull() {
            addCriterion("tax is null");
            return (Criteria) this;
        }

        public Criteria andTaxIsNotNull() {
            addCriterion("tax is not null");
            return (Criteria) this;
        }

        public Criteria andTaxEqualTo(BigDecimal value) {
            addCriterion("tax =", value, "tax");
            return (Criteria) this;
        }

        public Criteria andTaxNotEqualTo(BigDecimal value) {
            addCriterion("tax <>", value, "tax");
            return (Criteria) this;
        }

        public Criteria andTaxGreaterThan(BigDecimal value) {
            addCriterion("tax >", value, "tax");
            return (Criteria) this;
        }

        public Criteria andTaxGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("tax >=", value, "tax");
            return (Criteria) this;
        }

        public Criteria andTaxLessThan(BigDecimal value) {
            addCriterion("tax <", value, "tax");
            return (Criteria) this;
        }

        public Criteria andTaxLessThanOrEqualTo(BigDecimal value) {
            addCriterion("tax <=", value, "tax");
            return (Criteria) this;
        }

        public Criteria andTaxIn(List<BigDecimal> values) {
            addCriterion("tax in", values, "tax");
            return (Criteria) this;
        }

        public Criteria andTaxNotIn(List<BigDecimal> values) {
            addCriterion("tax not in", values, "tax");
            return (Criteria) this;
        }

        public Criteria andTaxBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("tax between", value1, value2, "tax");
            return (Criteria) this;
        }

        public Criteria andTaxNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("tax not between", value1, value2, "tax");
            return (Criteria) this;
        }

        public Criteria andManagementCostIsNull() {
            addCriterion("management_cost is null");
            return (Criteria) this;
        }

        public Criteria andManagementCostIsNotNull() {
            addCriterion("management_cost is not null");
            return (Criteria) this;
        }

        public Criteria andManagementCostEqualTo(BigDecimal value) {
            addCriterion("management_cost =", value, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementCostNotEqualTo(BigDecimal value) {
            addCriterion("management_cost <>", value, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementCostGreaterThan(BigDecimal value) {
            addCriterion("management_cost >", value, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementCostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("management_cost >=", value, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementCostLessThan(BigDecimal value) {
            addCriterion("management_cost <", value, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementCostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("management_cost <=", value, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementCostIn(List<BigDecimal> values) {
            addCriterion("management_cost in", values, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementCostNotIn(List<BigDecimal> values) {
            addCriterion("management_cost not in", values, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementCostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_cost between", value1, value2, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementCostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_cost not between", value1, value2, "managementCost");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsIsNull() {
            addCriterion("management_personnel_costs is null");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsIsNotNull() {
            addCriterion("management_personnel_costs is not null");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsEqualTo(BigDecimal value) {
            addCriterion("management_personnel_costs =", value, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsNotEqualTo(BigDecimal value) {
            addCriterion("management_personnel_costs <>", value, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsGreaterThan(BigDecimal value) {
            addCriterion("management_personnel_costs >", value, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("management_personnel_costs >=", value, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsLessThan(BigDecimal value) {
            addCriterion("management_personnel_costs <", value, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("management_personnel_costs <=", value, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsIn(List<BigDecimal> values) {
            addCriterion("management_personnel_costs in", values, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsNotIn(List<BigDecimal> values) {
            addCriterion("management_personnel_costs not in", values, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_personnel_costs between", value1, value2, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementPersonnelCostsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_personnel_costs not between", value1, value2, "managementPersonnelCosts");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesIsNull() {
            addCriterion("management_daily_expenses is null");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesIsNotNull() {
            addCriterion("management_daily_expenses is not null");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesEqualTo(BigDecimal value) {
            addCriterion("management_daily_expenses =", value, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesNotEqualTo(BigDecimal value) {
            addCriterion("management_daily_expenses <>", value, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesGreaterThan(BigDecimal value) {
            addCriterion("management_daily_expenses >", value, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("management_daily_expenses >=", value, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesLessThan(BigDecimal value) {
            addCriterion("management_daily_expenses <", value, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("management_daily_expenses <=", value, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesIn(List<BigDecimal> values) {
            addCriterion("management_daily_expenses in", values, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesNotIn(List<BigDecimal> values) {
            addCriterion("management_daily_expenses not in", values, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_daily_expenses between", value1, value2, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementDailyExpensesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_daily_expenses not between", value1, value2, "managementDailyExpenses");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationIsNull() {
            addCriterion("management_fixed_assets_depreciation is null");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationIsNotNull() {
            addCriterion("management_fixed_assets_depreciation is not null");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationEqualTo(BigDecimal value) {
            addCriterion("management_fixed_assets_depreciation =", value, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationNotEqualTo(BigDecimal value) {
            addCriterion("management_fixed_assets_depreciation <>", value, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationGreaterThan(BigDecimal value) {
            addCriterion("management_fixed_assets_depreciation >", value, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("management_fixed_assets_depreciation >=", value, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationLessThan(BigDecimal value) {
            addCriterion("management_fixed_assets_depreciation <", value, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationLessThanOrEqualTo(BigDecimal value) {
            addCriterion("management_fixed_assets_depreciation <=", value, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationIn(List<BigDecimal> values) {
            addCriterion("management_fixed_assets_depreciation in", values, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationNotIn(List<BigDecimal> values) {
            addCriterion("management_fixed_assets_depreciation not in", values, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_fixed_assets_depreciation between", value1, value2, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementFixedAssetsDepreciationNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_fixed_assets_depreciation not between", value1, value2, "managementFixedAssetsDepreciation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationIsNull() {
            addCriterion("management_taxation is null");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationIsNotNull() {
            addCriterion("management_taxation is not null");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationEqualTo(BigDecimal value) {
            addCriterion("management_taxation =", value, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationNotEqualTo(BigDecimal value) {
            addCriterion("management_taxation <>", value, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationGreaterThan(BigDecimal value) {
            addCriterion("management_taxation >", value, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("management_taxation >=", value, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationLessThan(BigDecimal value) {
            addCriterion("management_taxation <", value, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationLessThanOrEqualTo(BigDecimal value) {
            addCriterion("management_taxation <=", value, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationIn(List<BigDecimal> values) {
            addCriterion("management_taxation in", values, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationNotIn(List<BigDecimal> values) {
            addCriterion("management_taxation not in", values, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_taxation between", value1, value2, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andManagementTaxationNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("management_taxation not between", value1, value2, "managementTaxation");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeIsNull() {
            addCriterion("net_asset_change is null");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeIsNotNull() {
            addCriterion("net_asset_change is not null");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeEqualTo(BigDecimal value) {
            addCriterion("net_asset_change =", value, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeNotEqualTo(BigDecimal value) {
            addCriterion("net_asset_change <>", value, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeGreaterThan(BigDecimal value) {
            addCriterion("net_asset_change >", value, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("net_asset_change >=", value, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeLessThan(BigDecimal value) {
            addCriterion("net_asset_change <", value, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("net_asset_change <=", value, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeIn(List<BigDecimal> values) {
            addCriterion("net_asset_change in", values, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeNotIn(List<BigDecimal> values) {
            addCriterion("net_asset_change not in", values, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("net_asset_change between", value1, value2, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andNetAssetChangeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("net_asset_change not between", value1, value2, "netAssetChange");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeIsNull() {
            addCriterion("supplement_social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeIsNotNull() {
            addCriterion("supplement_social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeEqualTo(String value) {
            addCriterion("supplement_social_credit_code =", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeNotEqualTo(String value) {
            addCriterion("supplement_social_credit_code <>", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeGreaterThan(String value) {
            addCriterion("supplement_social_credit_code >", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("supplement_social_credit_code >=", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeLessThan(String value) {
            addCriterion("supplement_social_credit_code <", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("supplement_social_credit_code <=", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeLike(String value) {
            addCriterion("supplement_social_credit_code like", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeNotLike(String value) {
            addCriterion("supplement_social_credit_code not like", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeIn(List<String> values) {
            addCriterion("supplement_social_credit_code in", values, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeNotIn(List<String> values) {
            addCriterion("supplement_social_credit_code not in", values, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("supplement_social_credit_code between", value1, value2, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("supplement_social_credit_code not between", value1, value2, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeIsNull() {
            addCriterion("supplement_organization_code is null");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeIsNotNull() {
            addCriterion("supplement_organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeEqualTo(String value) {
            addCriterion("supplement_organization_code =", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeNotEqualTo(String value) {
            addCriterion("supplement_organization_code <>", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeGreaterThan(String value) {
            addCriterion("supplement_organization_code >", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("supplement_organization_code >=", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeLessThan(String value) {
            addCriterion("supplement_organization_code <", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("supplement_organization_code <=", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeLike(String value) {
            addCriterion("supplement_organization_code like", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeNotLike(String value) {
            addCriterion("supplement_organization_code not like", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeIn(List<String> values) {
            addCriterion("supplement_organization_code in", values, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeNotIn(List<String> values) {
            addCriterion("supplement_organization_code not in", values, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeBetween(String value1, String value2) {
            addCriterion("supplement_organization_code between", value1, value2, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("supplement_organization_code not between", value1, value2, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameIsNull() {
            addCriterion("supplement_unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameIsNotNull() {
            addCriterion("supplement_unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameEqualTo(String value) {
            addCriterion("supplement_unit_detailed_name =", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameNotEqualTo(String value) {
            addCriterion("supplement_unit_detailed_name <>", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameGreaterThan(String value) {
            addCriterion("supplement_unit_detailed_name >", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("supplement_unit_detailed_name >=", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameLessThan(String value) {
            addCriterion("supplement_unit_detailed_name <", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("supplement_unit_detailed_name <=", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameLike(String value) {
            addCriterion("supplement_unit_detailed_name like", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameNotLike(String value) {
            addCriterion("supplement_unit_detailed_name not like", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameIn(List<String> values) {
            addCriterion("supplement_unit_detailed_name in", values, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameNotIn(List<String> values) {
            addCriterion("supplement_unit_detailed_name not in", values, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("supplement_unit_detailed_name between", value1, value2, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("supplement_unit_detailed_name not between", value1, value2, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIsNull() {
            addCriterion("unit_head is null");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIsNotNull() {
            addCriterion("unit_head is not null");
            return (Criteria) this;
        }

        public Criteria andUnitHeadEqualTo(String value) {
            addCriterion("unit_head =", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotEqualTo(String value) {
            addCriterion("unit_head <>", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadGreaterThan(String value) {
            addCriterion("unit_head >", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadGreaterThanOrEqualTo(String value) {
            addCriterion("unit_head >=", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLessThan(String value) {
            addCriterion("unit_head <", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLessThanOrEqualTo(String value) {
            addCriterion("unit_head <=", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLike(String value) {
            addCriterion("unit_head like", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotLike(String value) {
            addCriterion("unit_head not like", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIn(List<String> values) {
            addCriterion("unit_head in", values, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotIn(List<String> values) {
            addCriterion("unit_head not in", values, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadBetween(String value1, String value2) {
            addCriterion("unit_head between", value1, value2, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotBetween(String value1, String value2) {
            addCriterion("unit_head not between", value1, value2, "unitHead");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIsNull() {
            addCriterion("statistical_control_officer is null");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIsNotNull() {
            addCriterion("statistical_control_officer is not null");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerEqualTo(String value) {
            addCriterion("statistical_control_officer =", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotEqualTo(String value) {
            addCriterion("statistical_control_officer <>", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerGreaterThan(String value) {
            addCriterion("statistical_control_officer >", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerGreaterThanOrEqualTo(String value) {
            addCriterion("statistical_control_officer >=", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLessThan(String value) {
            addCriterion("statistical_control_officer <", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLessThanOrEqualTo(String value) {
            addCriterion("statistical_control_officer <=", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLike(String value) {
            addCriterion("statistical_control_officer like", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotLike(String value) {
            addCriterion("statistical_control_officer not like", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIn(List<String> values) {
            addCriterion("statistical_control_officer in", values, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotIn(List<String> values) {
            addCriterion("statistical_control_officer not in", values, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerBetween(String value1, String value2) {
            addCriterion("statistical_control_officer between", value1, value2, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotBetween(String value1, String value2) {
            addCriterion("statistical_control_officer not between", value1, value2, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNull() {
            addCriterion("fill_form_by is null");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNotNull() {
            addCriterion("fill_form_by is not null");
            return (Criteria) this;
        }

        public Criteria andFillFormByEqualTo(String value) {
            addCriterion("fill_form_by =", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotEqualTo(String value) {
            addCriterion("fill_form_by <>", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThan(String value) {
            addCriterion("fill_form_by >", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThanOrEqualTo(String value) {
            addCriterion("fill_form_by >=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThan(String value) {
            addCriterion("fill_form_by <", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThanOrEqualTo(String value) {
            addCriterion("fill_form_by <=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLike(String value) {
            addCriterion("fill_form_by like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotLike(String value) {
            addCriterion("fill_form_by not like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByIn(List<String> values) {
            addCriterion("fill_form_by in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotIn(List<String> values) {
            addCriterion("fill_form_by not in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByBetween(String value1, String value2) {
            addCriterion("fill_form_by between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotBetween(String value1, String value2) {
            addCriterion("fill_form_by not between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}