package net.yunxinyong.sign.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class CecThree {
    private Integer id;

    private Integer invId;

    private String socialCreditCode;

    private String organizationCode;

    private String unitDetailedName;

    private BigDecimal thisYearDepreciation;

    private BigDecimal fixedAssets;

    private BigDecimal constructionProject;

    private BigDecimal totalAssets;

    private BigDecimal totalDebt;

    private BigDecimal beginStock;

    private BigDecimal endTermStock;

    private BigDecimal businessIncome;

    private BigDecimal businessCost;

    private BigDecimal taxation;

    private BigDecimal investmentIncome;

    private BigDecimal operatingProfit;

    private BigDecimal totalRemuneration;

    private BigDecimal totalTax;

    private BigDecimal commodityPurchase;

    private BigDecimal commoditySales;

    private BigDecimal eCommoditySales;

    private BigDecimal retailSales;

    private BigDecimal eRetailSales;

    private BigDecimal finalCommodityStock;

    private BigDecimal serviceTurnover;

    private BigDecimal turnover;

    private BigDecimal eTurnover;

    private BigDecimal roomIncome;

    private BigDecimal mealIncome;

    private BigDecimal eMealIncome;

    private BigDecimal secondCommoditySales;

    private BigDecimal secondECommoditySales;

    private Integer isHaveEPlatform;

    private Integer ePlatformNumber;

    private String eEconomicActivity;

    private String otherEconomic;

    private String eBusinessActivities;

    private String otherBusiness;

    private String supplementSocialCreditCode;

    private String supplementOrganizationCode;

    private String supplementUnitDetailedName;

    private String unitHead;

    private String statisticalControlOfficer;

    private String fillFormBy;

    private String phone;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private Integer status;

    private Integer state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvId() {
        return invId;
    }

    public void setInvId(Integer invId) {
        this.invId = invId;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName == null ? null : unitDetailedName.trim();
    }

    public BigDecimal getThisYearDepreciation() {
        return thisYearDepreciation;
    }

    public void setThisYearDepreciation(BigDecimal thisYearDepreciation) {
        this.thisYearDepreciation = thisYearDepreciation;
    }

    public BigDecimal getFixedAssets() {
        return fixedAssets;
    }

    public void setFixedAssets(BigDecimal fixedAssets) {
        this.fixedAssets = fixedAssets;
    }

    public BigDecimal getConstructionProject() {
        return constructionProject;
    }

    public void setConstructionProject(BigDecimal constructionProject) {
        this.constructionProject = constructionProject;
    }

    public BigDecimal getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(BigDecimal totalAssets) {
        this.totalAssets = totalAssets;
    }

    public BigDecimal getTotalDebt() {
        return totalDebt;
    }

    public void setTotalDebt(BigDecimal totalDebt) {
        this.totalDebt = totalDebt;
    }

    public BigDecimal getBeginStock() {
        return beginStock;
    }

    public void setBeginStock(BigDecimal beginStock) {
        this.beginStock = beginStock;
    }

    public BigDecimal getEndTermStock() {
        return endTermStock;
    }

    public void setEndTermStock(BigDecimal endTermStock) {
        this.endTermStock = endTermStock;
    }

    public BigDecimal getBusinessIncome() {
        return businessIncome;
    }

    public void setBusinessIncome(BigDecimal businessIncome) {
        this.businessIncome = businessIncome;
    }

    public BigDecimal getBusinessCost() {
        return businessCost;
    }

    public void setBusinessCost(BigDecimal businessCost) {
        this.businessCost = businessCost;
    }

    public BigDecimal getTaxation() {
        return taxation;
    }

    public void setTaxation(BigDecimal taxation) {
        this.taxation = taxation;
    }

    public BigDecimal getInvestmentIncome() {
        return investmentIncome;
    }

    public void setInvestmentIncome(BigDecimal investmentIncome) {
        this.investmentIncome = investmentIncome;
    }

    public BigDecimal getOperatingProfit() {
        return operatingProfit;
    }

    public void setOperatingProfit(BigDecimal operatingProfit) {
        this.operatingProfit = operatingProfit;
    }

    public BigDecimal getTotalRemuneration() {
        return totalRemuneration;
    }

    public void setTotalRemuneration(BigDecimal totalRemuneration) {
        this.totalRemuneration = totalRemuneration;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public BigDecimal getCommodityPurchase() {
        return commodityPurchase;
    }

    public void setCommodityPurchase(BigDecimal commodityPurchase) {
        this.commodityPurchase = commodityPurchase;
    }

    public BigDecimal getCommoditySales() {
        return commoditySales;
    }

    public void setCommoditySales(BigDecimal commoditySales) {
        this.commoditySales = commoditySales;
    }

    public BigDecimal geteCommoditySales() {
        return eCommoditySales;
    }

    public void seteCommoditySales(BigDecimal eCommoditySales) {
        this.eCommoditySales = eCommoditySales;
    }

    public BigDecimal getRetailSales() {
        return retailSales;
    }

    public void setRetailSales(BigDecimal retailSales) {
        this.retailSales = retailSales;
    }

    public BigDecimal geteRetailSales() {
        return eRetailSales;
    }

    public void seteRetailSales(BigDecimal eRetailSales) {
        this.eRetailSales = eRetailSales;
    }

    public BigDecimal getFinalCommodityStock() {
        return finalCommodityStock;
    }

    public void setFinalCommodityStock(BigDecimal finalCommodityStock) {
        this.finalCommodityStock = finalCommodityStock;
    }

    public BigDecimal getServiceTurnover() {
        return serviceTurnover;
    }

    public void setServiceTurnover(BigDecimal serviceTurnover) {
        this.serviceTurnover = serviceTurnover;
    }

    public BigDecimal getTurnover() {
        return turnover;
    }

    public void setTurnover(BigDecimal turnover) {
        this.turnover = turnover;
    }

    public BigDecimal geteTurnover() {
        return eTurnover;
    }

    public void seteTurnover(BigDecimal eTurnover) {
        this.eTurnover = eTurnover;
    }

    public BigDecimal getRoomIncome() {
        return roomIncome;
    }

    public void setRoomIncome(BigDecimal roomIncome) {
        this.roomIncome = roomIncome;
    }

    public BigDecimal getMealIncome() {
        return mealIncome;
    }

    public void setMealIncome(BigDecimal mealIncome) {
        this.mealIncome = mealIncome;
    }

    public BigDecimal geteMealIncome() {
        return eMealIncome;
    }

    public void seteMealIncome(BigDecimal eMealIncome) {
        this.eMealIncome = eMealIncome;
    }

    public BigDecimal getSecondCommoditySales() {
        return secondCommoditySales;
    }

    public void setSecondCommoditySales(BigDecimal secondCommoditySales) {
        this.secondCommoditySales = secondCommoditySales;
    }

    public BigDecimal getSecondECommoditySales() {
        return secondECommoditySales;
    }

    public void setSecondECommoditySales(BigDecimal secondECommoditySales) {
        this.secondECommoditySales = secondECommoditySales;
    }

    public Integer getIsHaveEPlatform() {
        return isHaveEPlatform;
    }

    public void setIsHaveEPlatform(Integer isHaveEPlatform) {
        this.isHaveEPlatform = isHaveEPlatform;
    }

    public Integer getePlatformNumber() {
        return ePlatformNumber;
    }

    public void setePlatformNumber(Integer ePlatformNumber) {
        this.ePlatformNumber = ePlatformNumber;
    }

    public String geteEconomicActivity() {
        return eEconomicActivity;
    }

    public void seteEconomicActivity(String eEconomicActivity) {
        this.eEconomicActivity = eEconomicActivity == null ? null : eEconomicActivity.trim();
    }

    public String getOtherEconomic() {
        return otherEconomic;
    }

    public void setOtherEconomic(String otherEconomic) {
        this.otherEconomic = otherEconomic == null ? null : otherEconomic.trim();
    }

    public String geteBusinessActivities() {
        return eBusinessActivities;
    }

    public void seteBusinessActivities(String eBusinessActivities) {
        this.eBusinessActivities = eBusinessActivities == null ? null : eBusinessActivities.trim();
    }

    public String getOtherBusiness() {
        return otherBusiness;
    }

    public void setOtherBusiness(String otherBusiness) {
        this.otherBusiness = otherBusiness == null ? null : otherBusiness.trim();
    }

    public String getSupplementSocialCreditCode() {
        return supplementSocialCreditCode;
    }

    public void setSupplementSocialCreditCode(String supplementSocialCreditCode) {
        this.supplementSocialCreditCode = supplementSocialCreditCode == null ? null : supplementSocialCreditCode.trim();
    }

    public String getSupplementOrganizationCode() {
        return supplementOrganizationCode;
    }

    public void setSupplementOrganizationCode(String supplementOrganizationCode) {
        this.supplementOrganizationCode = supplementOrganizationCode == null ? null : supplementOrganizationCode.trim();
    }

    public String getSupplementUnitDetailedName() {
        return supplementUnitDetailedName;
    }

    public void setSupplementUnitDetailedName(String supplementUnitDetailedName) {
        this.supplementUnitDetailedName = supplementUnitDetailedName == null ? null : supplementUnitDetailedName.trim();
    }

    public String getUnitHead() {
        return unitHead;
    }

    public void setUnitHead(String unitHead) {
        this.unitHead = unitHead == null ? null : unitHead.trim();
    }

    public String getStatisticalControlOfficer() {
        return statisticalControlOfficer;
    }

    public void setStatisticalControlOfficer(String statisticalControlOfficer) {
        this.statisticalControlOfficer = statisticalControlOfficer == null ? null : statisticalControlOfficer.trim();
    }

    public String getFillFormBy() {
        return fillFormBy;
    }

    public void setFillFormBy(String fillFormBy) {
        this.fillFormBy = fillFormBy == null ? null : fillFormBy.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}