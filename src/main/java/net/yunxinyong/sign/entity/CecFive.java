package net.yunxinyong.sign.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class CecFive {
    private Integer id;

    private Integer invId;

    private String socialCreditCode;

    private String organizationCode;

    private String unitDetailedName;

    private BigDecimal beginStock;

    private BigDecimal totalCurrentAssets;

    private BigDecimal monetaryFunds;

    private BigDecimal cash;

    private BigDecimal stock;

    private BigDecimal securities;

    private BigDecimal longTermInvestment;

    private BigDecimal fixedAssets;

    private BigDecimal housesStructures;

    private BigDecimal machineryEquipment;

    private BigDecimal conveyance;

    private BigDecimal fixedAssetsUnderFinancingLease;

    private BigDecimal constructionProject;

    private BigDecimal intangibleAssets;

    private BigDecimal landUseRight;

    private BigDecimal publicInfrastructure;

    private BigDecimal publicInfrastructureDepreciation;

    private BigDecimal totalAssets;

    private BigDecimal totalDebt;

    private BigDecimal totalNetAssets;

    private BigDecimal totalIncome;

    private BigDecimal financialAppropriation;

    private BigDecimal causeIncome;

    private BigDecimal operatingIncome;

    private BigDecimal totalExpenditure;

    private BigDecimal welfareExpenditure;

    private BigDecimal serviceExpenditure;

    private BigDecimal laborServiceFee;

    private BigDecimal labourUnionExpenditure;

    private BigDecimal welfareFunds;

    private BigDecimal taxFee;

    private BigDecimal heatingFee;

    private BigDecimal travelAbroadFee;

    private BigDecimal subsidy;

    private BigDecimal retired;

    private BigDecimal medicalAid;

    private BigDecimal pension;

    private BigDecimal livingAllowance;

    private BigDecimal reliefFee;

    private BigDecimal schoolGrant;

    private BigDecimal operatingExpenses;

    private BigDecimal interestIncome;

    private BigDecimal interestExpenditure;

    private Integer averageEmployeesNumber;

    private String supplementSocialCreditCode;

    private String supplementOrganizationCode;

    private String supplementUnitDetailedName;

    private String unitHead;

    private String statisticalControlOfficer;

    private String fillFormBy;

    private String phone;

    private Integer state;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvId() {
        return invId;
    }

    public void setInvId(Integer invId) {
        this.invId = invId;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName == null ? null : unitDetailedName.trim();
    }

    public BigDecimal getBeginStock() {
        return beginStock;
    }

    public void setBeginStock(BigDecimal beginStock) {
        this.beginStock = beginStock;
    }

    public BigDecimal getTotalCurrentAssets() {
        return totalCurrentAssets;
    }

    public void setTotalCurrentAssets(BigDecimal totalCurrentAssets) {
        this.totalCurrentAssets = totalCurrentAssets;
    }

    public BigDecimal getMonetaryFunds() {
        return monetaryFunds;
    }

    public void setMonetaryFunds(BigDecimal monetaryFunds) {
        this.monetaryFunds = monetaryFunds;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getStock() {
        return stock;
    }

    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    public BigDecimal getSecurities() {
        return securities;
    }

    public void setSecurities(BigDecimal securities) {
        this.securities = securities;
    }

    public BigDecimal getLongTermInvestment() {
        return longTermInvestment;
    }

    public void setLongTermInvestment(BigDecimal longTermInvestment) {
        this.longTermInvestment = longTermInvestment;
    }

    public BigDecimal getFixedAssets() {
        return fixedAssets;
    }

    public void setFixedAssets(BigDecimal fixedAssets) {
        this.fixedAssets = fixedAssets;
    }

    public BigDecimal getHousesStructures() {
        return housesStructures;
    }

    public void setHousesStructures(BigDecimal housesStructures) {
        this.housesStructures = housesStructures;
    }

    public BigDecimal getMachineryEquipment() {
        return machineryEquipment;
    }

    public void setMachineryEquipment(BigDecimal machineryEquipment) {
        this.machineryEquipment = machineryEquipment;
    }

    public BigDecimal getConveyance() {
        return conveyance;
    }

    public void setConveyance(BigDecimal conveyance) {
        this.conveyance = conveyance;
    }

    public BigDecimal getFixedAssetsUnderFinancingLease() {
        return fixedAssetsUnderFinancingLease;
    }

    public void setFixedAssetsUnderFinancingLease(BigDecimal fixedAssetsUnderFinancingLease) {
        this.fixedAssetsUnderFinancingLease = fixedAssetsUnderFinancingLease;
    }

    public BigDecimal getConstructionProject() {
        return constructionProject;
    }

    public void setConstructionProject(BigDecimal constructionProject) {
        this.constructionProject = constructionProject;
    }

    public BigDecimal getIntangibleAssets() {
        return intangibleAssets;
    }

    public void setIntangibleAssets(BigDecimal intangibleAssets) {
        this.intangibleAssets = intangibleAssets;
    }

    public BigDecimal getLandUseRight() {
        return landUseRight;
    }

    public void setLandUseRight(BigDecimal landUseRight) {
        this.landUseRight = landUseRight;
    }

    public BigDecimal getPublicInfrastructure() {
        return publicInfrastructure;
    }

    public void setPublicInfrastructure(BigDecimal publicInfrastructure) {
        this.publicInfrastructure = publicInfrastructure;
    }

    public BigDecimal getPublicInfrastructureDepreciation() {
        return publicInfrastructureDepreciation;
    }

    public void setPublicInfrastructureDepreciation(BigDecimal publicInfrastructureDepreciation) {
        this.publicInfrastructureDepreciation = publicInfrastructureDepreciation;
    }

    public BigDecimal getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(BigDecimal totalAssets) {
        this.totalAssets = totalAssets;
    }

    public BigDecimal getTotalDebt() {
        return totalDebt;
    }

    public void setTotalDebt(BigDecimal totalDebt) {
        this.totalDebt = totalDebt;
    }

    public BigDecimal getTotalNetAssets() {
        return totalNetAssets;
    }

    public void setTotalNetAssets(BigDecimal totalNetAssets) {
        this.totalNetAssets = totalNetAssets;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public BigDecimal getFinancialAppropriation() {
        return financialAppropriation;
    }

    public void setFinancialAppropriation(BigDecimal financialAppropriation) {
        this.financialAppropriation = financialAppropriation;
    }

    public BigDecimal getCauseIncome() {
        return causeIncome;
    }

    public void setCauseIncome(BigDecimal causeIncome) {
        this.causeIncome = causeIncome;
    }

    public BigDecimal getOperatingIncome() {
        return operatingIncome;
    }

    public void setOperatingIncome(BigDecimal operatingIncome) {
        this.operatingIncome = operatingIncome;
    }

    public BigDecimal getTotalExpenditure() {
        return totalExpenditure;
    }

    public void setTotalExpenditure(BigDecimal totalExpenditure) {
        this.totalExpenditure = totalExpenditure;
    }

    public BigDecimal getWelfareExpenditure() {
        return welfareExpenditure;
    }

    public void setWelfareExpenditure(BigDecimal welfareExpenditure) {
        this.welfareExpenditure = welfareExpenditure;
    }

    public BigDecimal getServiceExpenditure() {
        return serviceExpenditure;
    }

    public void setServiceExpenditure(BigDecimal serviceExpenditure) {
        this.serviceExpenditure = serviceExpenditure;
    }

    public BigDecimal getLaborServiceFee() {
        return laborServiceFee;
    }

    public void setLaborServiceFee(BigDecimal laborServiceFee) {
        this.laborServiceFee = laborServiceFee;
    }

    public BigDecimal getLabourUnionExpenditure() {
        return labourUnionExpenditure;
    }

    public void setLabourUnionExpenditure(BigDecimal labourUnionExpenditure) {
        this.labourUnionExpenditure = labourUnionExpenditure;
    }

    public BigDecimal getWelfareFunds() {
        return welfareFunds;
    }

    public void setWelfareFunds(BigDecimal welfareFunds) {
        this.welfareFunds = welfareFunds;
    }

    public BigDecimal getTaxFee() {
        return taxFee;
    }

    public void setTaxFee(BigDecimal taxFee) {
        this.taxFee = taxFee;
    }

    public BigDecimal getHeatingFee() {
        return heatingFee;
    }

    public void setHeatingFee(BigDecimal heatingFee) {
        this.heatingFee = heatingFee;
    }

    public BigDecimal getTravelAbroadFee() {
        return travelAbroadFee;
    }

    public void setTravelAbroadFee(BigDecimal travelAbroadFee) {
        this.travelAbroadFee = travelAbroadFee;
    }

    public BigDecimal getSubsidy() {
        return subsidy;
    }

    public void setSubsidy(BigDecimal subsidy) {
        this.subsidy = subsidy;
    }

    public BigDecimal getRetired() {
        return retired;
    }

    public void setRetired(BigDecimal retired) {
        this.retired = retired;
    }

    public BigDecimal getMedicalAid() {
        return medicalAid;
    }

    public void setMedicalAid(BigDecimal medicalAid) {
        this.medicalAid = medicalAid;
    }

    public BigDecimal getPension() {
        return pension;
    }

    public void setPension(BigDecimal pension) {
        this.pension = pension;
    }

    public BigDecimal getLivingAllowance() {
        return livingAllowance;
    }

    public void setLivingAllowance(BigDecimal livingAllowance) {
        this.livingAllowance = livingAllowance;
    }

    public BigDecimal getReliefFee() {
        return reliefFee;
    }

    public void setReliefFee(BigDecimal reliefFee) {
        this.reliefFee = reliefFee;
    }

    public BigDecimal getSchoolGrant() {
        return schoolGrant;
    }

    public void setSchoolGrant(BigDecimal schoolGrant) {
        this.schoolGrant = schoolGrant;
    }

    public BigDecimal getOperatingExpenses() {
        return operatingExpenses;
    }

    public void setOperatingExpenses(BigDecimal operatingExpenses) {
        this.operatingExpenses = operatingExpenses;
    }

    public BigDecimal getInterestIncome() {
        return interestIncome;
    }

    public void setInterestIncome(BigDecimal interestIncome) {
        this.interestIncome = interestIncome;
    }

    public BigDecimal getInterestExpenditure() {
        return interestExpenditure;
    }

    public void setInterestExpenditure(BigDecimal interestExpenditure) {
        this.interestExpenditure = interestExpenditure;
    }

    public Integer getAverageEmployeesNumber() {
        return averageEmployeesNumber;
    }

    public void setAverageEmployeesNumber(Integer averageEmployeesNumber) {
        this.averageEmployeesNumber = averageEmployeesNumber;
    }

    public String getSupplementSocialCreditCode() {
        return supplementSocialCreditCode;
    }

    public void setSupplementSocialCreditCode(String supplementSocialCreditCode) {
        this.supplementSocialCreditCode = supplementSocialCreditCode == null ? null : supplementSocialCreditCode.trim();
    }

    public String getSupplementOrganizationCode() {
        return supplementOrganizationCode;
    }

    public void setSupplementOrganizationCode(String supplementOrganizationCode) {
        this.supplementOrganizationCode = supplementOrganizationCode == null ? null : supplementOrganizationCode.trim();
    }

    public String getSupplementUnitDetailedName() {
        return supplementUnitDetailedName;
    }

    public void setSupplementUnitDetailedName(String supplementUnitDetailedName) {
        this.supplementUnitDetailedName = supplementUnitDetailedName == null ? null : supplementUnitDetailedName.trim();
    }

    public String getUnitHead() {
        return unitHead;
    }

    public void setUnitHead(String unitHead) {
        this.unitHead = unitHead == null ? null : unitHead.trim();
    }

    public String getStatisticalControlOfficer() {
        return statisticalControlOfficer;
    }

    public void setStatisticalControlOfficer(String statisticalControlOfficer) {
        this.statisticalControlOfficer = statisticalControlOfficer == null ? null : statisticalControlOfficer.trim();
    }

    public String getFillFormBy() {
        return fillFormBy;
    }

    public void setFillFormBy(String fillFormBy) {
        this.fillFormBy = fillFormBy == null ? null : fillFormBy.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}