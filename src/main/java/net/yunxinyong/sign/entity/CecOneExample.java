package net.yunxinyong.sign.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CecOneExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CecOneExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNull() {
            addCriterion("inv_id is null");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNotNull() {
            addCriterion("inv_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvIdEqualTo(Integer value) {
            addCriterion("inv_id =", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotEqualTo(Integer value) {
            addCriterion("inv_id <>", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThan(Integer value) {
            addCriterion("inv_id >", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("inv_id >=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThan(Integer value) {
            addCriterion("inv_id <", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThanOrEqualTo(Integer value) {
            addCriterion("inv_id <=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdIn(List<Integer> values) {
            addCriterion("inv_id in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotIn(List<Integer> values) {
            addCriterion("inv_id not in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdBetween(Integer value1, Integer value2) {
            addCriterion("inv_id between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotBetween(Integer value1, Integer value2) {
            addCriterion("inv_id not between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNull() {
            addCriterion("social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNotNull() {
            addCriterion("social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeEqualTo(String value) {
            addCriterion("social_credit_code =", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotEqualTo(String value) {
            addCriterion("social_credit_code <>", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThan(String value) {
            addCriterion("social_credit_code >", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("social_credit_code >=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThan(String value) {
            addCriterion("social_credit_code <", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("social_credit_code <=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLike(String value) {
            addCriterion("social_credit_code like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotLike(String value) {
            addCriterion("social_credit_code not like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIn(List<String> values) {
            addCriterion("social_credit_code in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotIn(List<String> values) {
            addCriterion("social_credit_code not in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("social_credit_code between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("social_credit_code not between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNull() {
            addCriterion("organization_code is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNotNull() {
            addCriterion("organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeEqualTo(String value) {
            addCriterion("organization_code =", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotEqualTo(String value) {
            addCriterion("organization_code <>", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThan(String value) {
            addCriterion("organization_code >", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("organization_code >=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThan(String value) {
            addCriterion("organization_code <", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("organization_code <=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLike(String value) {
            addCriterion("organization_code like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotLike(String value) {
            addCriterion("organization_code not like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIn(List<String> values) {
            addCriterion("organization_code in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotIn(List<String> values) {
            addCriterion("organization_code not in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeBetween(String value1, String value2) {
            addCriterion("organization_code between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("organization_code not between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNull() {
            addCriterion("unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNotNull() {
            addCriterion("unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameEqualTo(String value) {
            addCriterion("unit_detailed_name =", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotEqualTo(String value) {
            addCriterion("unit_detailed_name <>", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThan(String value) {
            addCriterion("unit_detailed_name >", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name >=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThan(String value) {
            addCriterion("unit_detailed_name <", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name <=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLike(String value) {
            addCriterion("unit_detailed_name like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotLike(String value) {
            addCriterion("unit_detailed_name not like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIn(List<String> values) {
            addCriterion("unit_detailed_name in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotIn(List<String> values) {
            addCriterion("unit_detailed_name not in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("unit_detailed_name between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("unit_detailed_name not between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andAmountIsNull() {
            addCriterion("amount is null");
            return (Criteria) this;
        }

        public Criteria andAmountIsNotNull() {
            addCriterion("amount is not null");
            return (Criteria) this;
        }

        public Criteria andAmountEqualTo(Integer value) {
            addCriterion("amount =", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotEqualTo(Integer value) {
            addCriterion("amount <>", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThan(Integer value) {
            addCriterion("amount >", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThanOrEqualTo(Integer value) {
            addCriterion("amount >=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThan(Integer value) {
            addCriterion("amount <", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThanOrEqualTo(Integer value) {
            addCriterion("amount <=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountIn(List<Integer> values) {
            addCriterion("amount in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotIn(List<Integer> values) {
            addCriterion("amount not in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountBetween(Integer value1, Integer value2) {
            addCriterion("amount between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotBetween(Integer value1, Integer value2) {
            addCriterion("amount not between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeIsNull() {
            addCriterion("brances_type is null");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeIsNotNull() {
            addCriterion("brances_type is not null");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeEqualTo(Integer value) {
            addCriterion("brances_type =", value, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeNotEqualTo(Integer value) {
            addCriterion("brances_type <>", value, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeGreaterThan(Integer value) {
            addCriterion("brances_type >", value, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("brances_type >=", value, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeLessThan(Integer value) {
            addCriterion("brances_type <", value, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeLessThanOrEqualTo(Integer value) {
            addCriterion("brances_type <=", value, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeIn(List<Integer> values) {
            addCriterion("brances_type in", values, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeNotIn(List<Integer> values) {
            addCriterion("brances_type not in", values, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeBetween(Integer value1, Integer value2) {
            addCriterion("brances_type between", value1, value2, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("brances_type not between", value1, value2, "brancesType");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeIsNull() {
            addCriterion("brances_social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeIsNotNull() {
            addCriterion("brances_social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeEqualTo(String value) {
            addCriterion("brances_social_credit_code =", value, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeNotEqualTo(String value) {
            addCriterion("brances_social_credit_code <>", value, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeGreaterThan(String value) {
            addCriterion("brances_social_credit_code >", value, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("brances_social_credit_code >=", value, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeLessThan(String value) {
            addCriterion("brances_social_credit_code <", value, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("brances_social_credit_code <=", value, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeLike(String value) {
            addCriterion("brances_social_credit_code like", value, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeNotLike(String value) {
            addCriterion("brances_social_credit_code not like", value, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeIn(List<String> values) {
            addCriterion("brances_social_credit_code in", values, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeNotIn(List<String> values) {
            addCriterion("brances_social_credit_code not in", values, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("brances_social_credit_code between", value1, value2, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("brances_social_credit_code not between", value1, value2, "brancesSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeIsNull() {
            addCriterion("brances_organization_code is null");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeIsNotNull() {
            addCriterion("brances_organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeEqualTo(String value) {
            addCriterion("brances_organization_code =", value, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeNotEqualTo(String value) {
            addCriterion("brances_organization_code <>", value, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeGreaterThan(String value) {
            addCriterion("brances_organization_code >", value, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("brances_organization_code >=", value, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeLessThan(String value) {
            addCriterion("brances_organization_code <", value, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("brances_organization_code <=", value, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeLike(String value) {
            addCriterion("brances_organization_code like", value, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeNotLike(String value) {
            addCriterion("brances_organization_code not like", value, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeIn(List<String> values) {
            addCriterion("brances_organization_code in", values, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeNotIn(List<String> values) {
            addCriterion("brances_organization_code not in", values, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeBetween(String value1, String value2) {
            addCriterion("brances_organization_code between", value1, value2, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("brances_organization_code not between", value1, value2, "brancesOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameIsNull() {
            addCriterion("brances_unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameIsNotNull() {
            addCriterion("brances_unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameEqualTo(String value) {
            addCriterion("brances_unit_detailed_name =", value, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameNotEqualTo(String value) {
            addCriterion("brances_unit_detailed_name <>", value, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameGreaterThan(String value) {
            addCriterion("brances_unit_detailed_name >", value, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("brances_unit_detailed_name >=", value, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameLessThan(String value) {
            addCriterion("brances_unit_detailed_name <", value, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("brances_unit_detailed_name <=", value, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameLike(String value) {
            addCriterion("brances_unit_detailed_name like", value, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameNotLike(String value) {
            addCriterion("brances_unit_detailed_name not like", value, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameIn(List<String> values) {
            addCriterion("brances_unit_detailed_name in", values, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameNotIn(List<String> values) {
            addCriterion("brances_unit_detailed_name not in", values, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("brances_unit_detailed_name between", value1, value2, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("brances_unit_detailed_name not between", value1, value2, "brancesUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressIsNull() {
            addCriterion("brances_address is null");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressIsNotNull() {
            addCriterion("brances_address is not null");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressEqualTo(String value) {
            addCriterion("brances_address =", value, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressNotEqualTo(String value) {
            addCriterion("brances_address <>", value, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressGreaterThan(String value) {
            addCriterion("brances_address >", value, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressGreaterThanOrEqualTo(String value) {
            addCriterion("brances_address >=", value, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressLessThan(String value) {
            addCriterion("brances_address <", value, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressLessThanOrEqualTo(String value) {
            addCriterion("brances_address <=", value, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressLike(String value) {
            addCriterion("brances_address like", value, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressNotLike(String value) {
            addCriterion("brances_address not like", value, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressIn(List<String> values) {
            addCriterion("brances_address in", values, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressNotIn(List<String> values) {
            addCriterion("brances_address not in", values, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressBetween(String value1, String value2) {
            addCriterion("brances_address between", value1, value2, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesAddressNotBetween(String value1, String value2) {
            addCriterion("brances_address not between", value1, value2, "brancesAddress");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningIsNull() {
            addCriterion("brances_zoning is null");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningIsNotNull() {
            addCriterion("brances_zoning is not null");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningEqualTo(String value) {
            addCriterion("brances_zoning =", value, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningNotEqualTo(String value) {
            addCriterion("brances_zoning <>", value, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningGreaterThan(String value) {
            addCriterion("brances_zoning >", value, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningGreaterThanOrEqualTo(String value) {
            addCriterion("brances_zoning >=", value, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningLessThan(String value) {
            addCriterion("brances_zoning <", value, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningLessThanOrEqualTo(String value) {
            addCriterion("brances_zoning <=", value, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningLike(String value) {
            addCriterion("brances_zoning like", value, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningNotLike(String value) {
            addCriterion("brances_zoning not like", value, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningIn(List<String> values) {
            addCriterion("brances_zoning in", values, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningNotIn(List<String> values) {
            addCriterion("brances_zoning not in", values, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningBetween(String value1, String value2) {
            addCriterion("brances_zoning between", value1, value2, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesZoningNotBetween(String value1, String value2) {
            addCriterion("brances_zoning not between", value1, value2, "brancesZoning");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberIsNull() {
            addCriterion("brances_contact_number is null");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberIsNotNull() {
            addCriterion("brances_contact_number is not null");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberEqualTo(String value) {
            addCriterion("brances_contact_number =", value, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberNotEqualTo(String value) {
            addCriterion("brances_contact_number <>", value, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberGreaterThan(String value) {
            addCriterion("brances_contact_number >", value, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberGreaterThanOrEqualTo(String value) {
            addCriterion("brances_contact_number >=", value, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberLessThan(String value) {
            addCriterion("brances_contact_number <", value, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberLessThanOrEqualTo(String value) {
            addCriterion("brances_contact_number <=", value, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberLike(String value) {
            addCriterion("brances_contact_number like", value, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberNotLike(String value) {
            addCriterion("brances_contact_number not like", value, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberIn(List<String> values) {
            addCriterion("brances_contact_number in", values, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberNotIn(List<String> values) {
            addCriterion("brances_contact_number not in", values, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberBetween(String value1, String value2) {
            addCriterion("brances_contact_number between", value1, value2, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBrancesContactNumberNotBetween(String value1, String value2) {
            addCriterion("brances_contact_number not between", value1, value2, "brancesContactNumber");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseIsNull() {
            addCriterion("brance_main_bussinese is null");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseIsNotNull() {
            addCriterion("brance_main_bussinese is not null");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseEqualTo(String value) {
            addCriterion("brance_main_bussinese =", value, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseNotEqualTo(String value) {
            addCriterion("brance_main_bussinese <>", value, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseGreaterThan(String value) {
            addCriterion("brance_main_bussinese >", value, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseGreaterThanOrEqualTo(String value) {
            addCriterion("brance_main_bussinese >=", value, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseLessThan(String value) {
            addCriterion("brance_main_bussinese <", value, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseLessThanOrEqualTo(String value) {
            addCriterion("brance_main_bussinese <=", value, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseLike(String value) {
            addCriterion("brance_main_bussinese like", value, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseNotLike(String value) {
            addCriterion("brance_main_bussinese not like", value, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseIn(List<String> values) {
            addCriterion("brance_main_bussinese in", values, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseNotIn(List<String> values) {
            addCriterion("brance_main_bussinese not in", values, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseBetween(String value1, String value2) {
            addCriterion("brance_main_bussinese between", value1, value2, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceMainBussineseNotBetween(String value1, String value2) {
            addCriterion("brance_main_bussinese not between", value1, value2, "branceMainBussinese");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeIsNull() {
            addCriterion("brance_industry_code is null");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeIsNotNull() {
            addCriterion("brance_industry_code is not null");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeEqualTo(String value) {
            addCriterion("brance_industry_code =", value, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeNotEqualTo(String value) {
            addCriterion("brance_industry_code <>", value, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeGreaterThan(String value) {
            addCriterion("brance_industry_code >", value, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeGreaterThanOrEqualTo(String value) {
            addCriterion("brance_industry_code >=", value, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeLessThan(String value) {
            addCriterion("brance_industry_code <", value, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeLessThanOrEqualTo(String value) {
            addCriterion("brance_industry_code <=", value, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeLike(String value) {
            addCriterion("brance_industry_code like", value, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeNotLike(String value) {
            addCriterion("brance_industry_code not like", value, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeIn(List<String> values) {
            addCriterion("brance_industry_code in", values, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeNotIn(List<String> values) {
            addCriterion("brance_industry_code not in", values, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeBetween(String value1, String value2) {
            addCriterion("brance_industry_code between", value1, value2, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andBranceIndustryCodeNotBetween(String value1, String value2) {
            addCriterion("brance_industry_code not between", value1, value2, "branceIndustryCode");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberIsNull() {
            addCriterion("final_employees_number is null");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberIsNotNull() {
            addCriterion("final_employees_number is not null");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberEqualTo(String value) {
            addCriterion("final_employees_number =", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberNotEqualTo(String value) {
            addCriterion("final_employees_number <>", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberGreaterThan(String value) {
            addCriterion("final_employees_number >", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberGreaterThanOrEqualTo(String value) {
            addCriterion("final_employees_number >=", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberLessThan(String value) {
            addCriterion("final_employees_number <", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberLessThanOrEqualTo(String value) {
            addCriterion("final_employees_number <=", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberLike(String value) {
            addCriterion("final_employees_number like", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberNotLike(String value) {
            addCriterion("final_employees_number not like", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberIn(List<String> values) {
            addCriterion("final_employees_number in", values, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberNotIn(List<String> values) {
            addCriterion("final_employees_number not in", values, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberBetween(String value1, String value2) {
            addCriterion("final_employees_number between", value1, value2, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberNotBetween(String value1, String value2) {
            addCriterion("final_employees_number not between", value1, value2, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeIsNull() {
            addCriterion("bussinese_income is null");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeIsNotNull() {
            addCriterion("bussinese_income is not null");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeEqualTo(String value) {
            addCriterion("bussinese_income =", value, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeNotEqualTo(String value) {
            addCriterion("bussinese_income <>", value, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeGreaterThan(String value) {
            addCriterion("bussinese_income >", value, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeGreaterThanOrEqualTo(String value) {
            addCriterion("bussinese_income >=", value, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeLessThan(String value) {
            addCriterion("bussinese_income <", value, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeLessThanOrEqualTo(String value) {
            addCriterion("bussinese_income <=", value, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeLike(String value) {
            addCriterion("bussinese_income like", value, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeNotLike(String value) {
            addCriterion("bussinese_income not like", value, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeIn(List<String> values) {
            addCriterion("bussinese_income in", values, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeNotIn(List<String> values) {
            addCriterion("bussinese_income not in", values, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeBetween(String value1, String value2) {
            addCriterion("bussinese_income between", value1, value2, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andBussineseIncomeNotBetween(String value1, String value2) {
            addCriterion("bussinese_income not between", value1, value2, "bussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeIsNull() {
            addCriterion("non_bussinese_income is null");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeIsNotNull() {
            addCriterion("non_bussinese_income is not null");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeEqualTo(String value) {
            addCriterion("non_bussinese_income =", value, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeNotEqualTo(String value) {
            addCriterion("non_bussinese_income <>", value, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeGreaterThan(String value) {
            addCriterion("non_bussinese_income >", value, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeGreaterThanOrEqualTo(String value) {
            addCriterion("non_bussinese_income >=", value, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeLessThan(String value) {
            addCriterion("non_bussinese_income <", value, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeLessThanOrEqualTo(String value) {
            addCriterion("non_bussinese_income <=", value, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeLike(String value) {
            addCriterion("non_bussinese_income like", value, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeNotLike(String value) {
            addCriterion("non_bussinese_income not like", value, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeIn(List<String> values) {
            addCriterion("non_bussinese_income in", values, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeNotIn(List<String> values) {
            addCriterion("non_bussinese_income not in", values, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeBetween(String value1, String value2) {
            addCriterion("non_bussinese_income between", value1, value2, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andNonBussineseIncomeNotBetween(String value1, String value2) {
            addCriterion("non_bussinese_income not between", value1, value2, "nonBussineseIncome");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNull() {
            addCriterion("fill_form_by is null");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNotNull() {
            addCriterion("fill_form_by is not null");
            return (Criteria) this;
        }

        public Criteria andFillFormByEqualTo(String value) {
            addCriterion("fill_form_by =", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotEqualTo(String value) {
            addCriterion("fill_form_by <>", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThan(String value) {
            addCriterion("fill_form_by >", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThanOrEqualTo(String value) {
            addCriterion("fill_form_by >=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThan(String value) {
            addCriterion("fill_form_by <", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThanOrEqualTo(String value) {
            addCriterion("fill_form_by <=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLike(String value) {
            addCriterion("fill_form_by like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotLike(String value) {
            addCriterion("fill_form_by not like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByIn(List<String> values) {
            addCriterion("fill_form_by in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotIn(List<String> values) {
            addCriterion("fill_form_by not in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByBetween(String value1, String value2) {
            addCriterion("fill_form_by between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotBetween(String value1, String value2) {
            addCriterion("fill_form_by not between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andFormTypeIsNull() {
            addCriterion("form_type is null");
            return (Criteria) this;
        }

        public Criteria andFormTypeIsNotNull() {
            addCriterion("form_type is not null");
            return (Criteria) this;
        }

        public Criteria andFormTypeEqualTo(Integer value) {
            addCriterion("form_type =", value, "formType");
            return (Criteria) this;
        }

        public Criteria andFormTypeNotEqualTo(Integer value) {
            addCriterion("form_type <>", value, "formType");
            return (Criteria) this;
        }

        public Criteria andFormTypeGreaterThan(Integer value) {
            addCriterion("form_type >", value, "formType");
            return (Criteria) this;
        }

        public Criteria andFormTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("form_type >=", value, "formType");
            return (Criteria) this;
        }

        public Criteria andFormTypeLessThan(Integer value) {
            addCriterion("form_type <", value, "formType");
            return (Criteria) this;
        }

        public Criteria andFormTypeLessThanOrEqualTo(Integer value) {
            addCriterion("form_type <=", value, "formType");
            return (Criteria) this;
        }

        public Criteria andFormTypeIn(List<Integer> values) {
            addCriterion("form_type in", values, "formType");
            return (Criteria) this;
        }

        public Criteria andFormTypeNotIn(List<Integer> values) {
            addCriterion("form_type not in", values, "formType");
            return (Criteria) this;
        }

        public Criteria andFormTypeBetween(Integer value1, Integer value2) {
            addCriterion("form_type between", value1, value2, "formType");
            return (Criteria) this;
        }

        public Criteria andFormTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("form_type not between", value1, value2, "formType");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}