package net.yunxinyong.sign.entity;

public class StreetCommunity {
    private Integer id;

    private String areaCode;

    private String parentCode;

    private String areaName;

    private String areaLevel;

    private String isLastLevel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode == null ? null : parentCode.trim();
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName == null ? null : areaName.trim();
    }

    public String getAreaLevel() {
        return areaLevel;
    }

    public void setAreaLevel(String areaLevel) {
        this.areaLevel = areaLevel == null ? null : areaLevel.trim();
    }

    public String getIsLastLevel() {
        return isLastLevel;
    }

    public void setIsLastLevel(String isLastLevel) {
        this.isLastLevel = isLastLevel == null ? null : isLastLevel.trim();
    }
}