package net.yunxinyong.sign.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CecSevenExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CecSevenExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNull() {
            addCriterion("inv_id is null");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNotNull() {
            addCriterion("inv_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvIdEqualTo(Integer value) {
            addCriterion("inv_id =", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotEqualTo(Integer value) {
            addCriterion("inv_id <>", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThan(Integer value) {
            addCriterion("inv_id >", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("inv_id >=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThan(Integer value) {
            addCriterion("inv_id <", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThanOrEqualTo(Integer value) {
            addCriterion("inv_id <=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdIn(List<Integer> values) {
            addCriterion("inv_id in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotIn(List<Integer> values) {
            addCriterion("inv_id not in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdBetween(Integer value1, Integer value2) {
            addCriterion("inv_id between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotBetween(Integer value1, Integer value2) {
            addCriterion("inv_id not between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNull() {
            addCriterion("social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNotNull() {
            addCriterion("social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeEqualTo(String value) {
            addCriterion("social_credit_code =", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotEqualTo(String value) {
            addCriterion("social_credit_code <>", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThan(String value) {
            addCriterion("social_credit_code >", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("social_credit_code >=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThan(String value) {
            addCriterion("social_credit_code <", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("social_credit_code <=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLike(String value) {
            addCriterion("social_credit_code like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotLike(String value) {
            addCriterion("social_credit_code not like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIn(List<String> values) {
            addCriterion("social_credit_code in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotIn(List<String> values) {
            addCriterion("social_credit_code not in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("social_credit_code between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("social_credit_code not between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNull() {
            addCriterion("organization_code is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNotNull() {
            addCriterion("organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeEqualTo(String value) {
            addCriterion("organization_code =", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotEqualTo(String value) {
            addCriterion("organization_code <>", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThan(String value) {
            addCriterion("organization_code >", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("organization_code >=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThan(String value) {
            addCriterion("organization_code <", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("organization_code <=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLike(String value) {
            addCriterion("organization_code like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotLike(String value) {
            addCriterion("organization_code not like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIn(List<String> values) {
            addCriterion("organization_code in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotIn(List<String> values) {
            addCriterion("organization_code not in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeBetween(String value1, String value2) {
            addCriterion("organization_code between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("organization_code not between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNull() {
            addCriterion("unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNotNull() {
            addCriterion("unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameEqualTo(String value) {
            addCriterion("unit_detailed_name =", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotEqualTo(String value) {
            addCriterion("unit_detailed_name <>", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThan(String value) {
            addCriterion("unit_detailed_name >", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name >=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThan(String value) {
            addCriterion("unit_detailed_name <", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name <=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLike(String value) {
            addCriterion("unit_detailed_name like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotLike(String value) {
            addCriterion("unit_detailed_name not like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIn(List<String> values) {
            addCriterion("unit_detailed_name in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotIn(List<String> values) {
            addCriterion("unit_detailed_name not in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("unit_detailed_name between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("unit_detailed_name not between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyIsNull() {
            addCriterion("total_energy is null");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyIsNotNull() {
            addCriterion("total_energy is not null");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyEqualTo(BigDecimal value) {
            addCriterion("total_energy =", value, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyNotEqualTo(BigDecimal value) {
            addCriterion("total_energy <>", value, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyGreaterThan(BigDecimal value) {
            addCriterion("total_energy >", value, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_energy >=", value, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyLessThan(BigDecimal value) {
            addCriterion("total_energy <", value, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_energy <=", value, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyIn(List<BigDecimal> values) {
            addCriterion("total_energy in", values, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyNotIn(List<BigDecimal> values) {
            addCriterion("total_energy not in", values, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_energy between", value1, value2, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_energy not between", value1, value2, "totalEnergy");
            return (Criteria) this;
        }

        public Criteria andElectricIsNull() {
            addCriterion("electric is null");
            return (Criteria) this;
        }

        public Criteria andElectricIsNotNull() {
            addCriterion("electric is not null");
            return (Criteria) this;
        }

        public Criteria andElectricEqualTo(BigDecimal value) {
            addCriterion("electric =", value, "electric");
            return (Criteria) this;
        }

        public Criteria andElectricNotEqualTo(BigDecimal value) {
            addCriterion("electric <>", value, "electric");
            return (Criteria) this;
        }

        public Criteria andElectricGreaterThan(BigDecimal value) {
            addCriterion("electric >", value, "electric");
            return (Criteria) this;
        }

        public Criteria andElectricGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("electric >=", value, "electric");
            return (Criteria) this;
        }

        public Criteria andElectricLessThan(BigDecimal value) {
            addCriterion("electric <", value, "electric");
            return (Criteria) this;
        }

        public Criteria andElectricLessThanOrEqualTo(BigDecimal value) {
            addCriterion("electric <=", value, "electric");
            return (Criteria) this;
        }

        public Criteria andElectricIn(List<BigDecimal> values) {
            addCriterion("electric in", values, "electric");
            return (Criteria) this;
        }

        public Criteria andElectricNotIn(List<BigDecimal> values) {
            addCriterion("electric not in", values, "electric");
            return (Criteria) this;
        }

        public Criteria andElectricBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("electric between", value1, value2, "electric");
            return (Criteria) this;
        }

        public Criteria andElectricNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("electric not between", value1, value2, "electric");
            return (Criteria) this;
        }

        public Criteria andCoalIsNull() {
            addCriterion("coal is null");
            return (Criteria) this;
        }

        public Criteria andCoalIsNotNull() {
            addCriterion("coal is not null");
            return (Criteria) this;
        }

        public Criteria andCoalEqualTo(BigDecimal value) {
            addCriterion("coal =", value, "coal");
            return (Criteria) this;
        }

        public Criteria andCoalNotEqualTo(BigDecimal value) {
            addCriterion("coal <>", value, "coal");
            return (Criteria) this;
        }

        public Criteria andCoalGreaterThan(BigDecimal value) {
            addCriterion("coal >", value, "coal");
            return (Criteria) this;
        }

        public Criteria andCoalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coal >=", value, "coal");
            return (Criteria) this;
        }

        public Criteria andCoalLessThan(BigDecimal value) {
            addCriterion("coal <", value, "coal");
            return (Criteria) this;
        }

        public Criteria andCoalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coal <=", value, "coal");
            return (Criteria) this;
        }

        public Criteria andCoalIn(List<BigDecimal> values) {
            addCriterion("coal in", values, "coal");
            return (Criteria) this;
        }

        public Criteria andCoalNotIn(List<BigDecimal> values) {
            addCriterion("coal not in", values, "coal");
            return (Criteria) this;
        }

        public Criteria andCoalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coal between", value1, value2, "coal");
            return (Criteria) this;
        }

        public Criteria andCoalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coal not between", value1, value2, "coal");
            return (Criteria) this;
        }

        public Criteria andNaturalGasIsNull() {
            addCriterion("natural_gas is null");
            return (Criteria) this;
        }

        public Criteria andNaturalGasIsNotNull() {
            addCriterion("natural_gas is not null");
            return (Criteria) this;
        }

        public Criteria andNaturalGasEqualTo(BigDecimal value) {
            addCriterion("natural_gas =", value, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andNaturalGasNotEqualTo(BigDecimal value) {
            addCriterion("natural_gas <>", value, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andNaturalGasGreaterThan(BigDecimal value) {
            addCriterion("natural_gas >", value, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andNaturalGasGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("natural_gas >=", value, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andNaturalGasLessThan(BigDecimal value) {
            addCriterion("natural_gas <", value, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andNaturalGasLessThanOrEqualTo(BigDecimal value) {
            addCriterion("natural_gas <=", value, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andNaturalGasIn(List<BigDecimal> values) {
            addCriterion("natural_gas in", values, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andNaturalGasNotIn(List<BigDecimal> values) {
            addCriterion("natural_gas not in", values, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andNaturalGasBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("natural_gas between", value1, value2, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andNaturalGasNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("natural_gas not between", value1, value2, "naturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasIsNull() {
            addCriterion("liquified_natural_gas is null");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasIsNotNull() {
            addCriterion("liquified_natural_gas is not null");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasEqualTo(BigDecimal value) {
            addCriterion("liquified_natural_gas =", value, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasNotEqualTo(BigDecimal value) {
            addCriterion("liquified_natural_gas <>", value, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasGreaterThan(BigDecimal value) {
            addCriterion("liquified_natural_gas >", value, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("liquified_natural_gas >=", value, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasLessThan(BigDecimal value) {
            addCriterion("liquified_natural_gas <", value, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasLessThanOrEqualTo(BigDecimal value) {
            addCriterion("liquified_natural_gas <=", value, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasIn(List<BigDecimal> values) {
            addCriterion("liquified_natural_gas in", values, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasNotIn(List<BigDecimal> values) {
            addCriterion("liquified_natural_gas not in", values, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("liquified_natural_gas between", value1, value2, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("liquified_natural_gas not between", value1, value2, "liquifiedNaturalGas");
            return (Criteria) this;
        }

        public Criteria andGasolineIsNull() {
            addCriterion("gasoline is null");
            return (Criteria) this;
        }

        public Criteria andGasolineIsNotNull() {
            addCriterion("gasoline is not null");
            return (Criteria) this;
        }

        public Criteria andGasolineEqualTo(BigDecimal value) {
            addCriterion("gasoline =", value, "gasoline");
            return (Criteria) this;
        }

        public Criteria andGasolineNotEqualTo(BigDecimal value) {
            addCriterion("gasoline <>", value, "gasoline");
            return (Criteria) this;
        }

        public Criteria andGasolineGreaterThan(BigDecimal value) {
            addCriterion("gasoline >", value, "gasoline");
            return (Criteria) this;
        }

        public Criteria andGasolineGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("gasoline >=", value, "gasoline");
            return (Criteria) this;
        }

        public Criteria andGasolineLessThan(BigDecimal value) {
            addCriterion("gasoline <", value, "gasoline");
            return (Criteria) this;
        }

        public Criteria andGasolineLessThanOrEqualTo(BigDecimal value) {
            addCriterion("gasoline <=", value, "gasoline");
            return (Criteria) this;
        }

        public Criteria andGasolineIn(List<BigDecimal> values) {
            addCriterion("gasoline in", values, "gasoline");
            return (Criteria) this;
        }

        public Criteria andGasolineNotIn(List<BigDecimal> values) {
            addCriterion("gasoline not in", values, "gasoline");
            return (Criteria) this;
        }

        public Criteria andGasolineBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gasoline between", value1, value2, "gasoline");
            return (Criteria) this;
        }

        public Criteria andGasolineNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gasoline not between", value1, value2, "gasoline");
            return (Criteria) this;
        }

        public Criteria andDieselOilIsNull() {
            addCriterion("diesel_oil is null");
            return (Criteria) this;
        }

        public Criteria andDieselOilIsNotNull() {
            addCriterion("diesel_oil is not null");
            return (Criteria) this;
        }

        public Criteria andDieselOilEqualTo(BigDecimal value) {
            addCriterion("diesel_oil =", value, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andDieselOilNotEqualTo(BigDecimal value) {
            addCriterion("diesel_oil <>", value, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andDieselOilGreaterThan(BigDecimal value) {
            addCriterion("diesel_oil >", value, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andDieselOilGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("diesel_oil >=", value, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andDieselOilLessThan(BigDecimal value) {
            addCriterion("diesel_oil <", value, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andDieselOilLessThanOrEqualTo(BigDecimal value) {
            addCriterion("diesel_oil <=", value, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andDieselOilIn(List<BigDecimal> values) {
            addCriterion("diesel_oil in", values, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andDieselOilNotIn(List<BigDecimal> values) {
            addCriterion("diesel_oil not in", values, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andDieselOilBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("diesel_oil between", value1, value2, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andDieselOilNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("diesel_oil not between", value1, value2, "dieselOil");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatIsNull() {
            addCriterion("purchasing_heat is null");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatIsNotNull() {
            addCriterion("purchasing_heat is not null");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatEqualTo(BigDecimal value) {
            addCriterion("purchasing_heat =", value, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatNotEqualTo(BigDecimal value) {
            addCriterion("purchasing_heat <>", value, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatGreaterThan(BigDecimal value) {
            addCriterion("purchasing_heat >", value, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("purchasing_heat >=", value, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatLessThan(BigDecimal value) {
            addCriterion("purchasing_heat <", value, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatLessThanOrEqualTo(BigDecimal value) {
            addCriterion("purchasing_heat <=", value, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatIn(List<BigDecimal> values) {
            addCriterion("purchasing_heat in", values, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatNotIn(List<BigDecimal> values) {
            addCriterion("purchasing_heat not in", values, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchasing_heat between", value1, value2, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchasing_heat not between", value1, value2, "purchasingHeat");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyIsNull() {
            addCriterion("total_energy_money is null");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyIsNotNull() {
            addCriterion("total_energy_money is not null");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyEqualTo(BigDecimal value) {
            addCriterion("total_energy_money =", value, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyNotEqualTo(BigDecimal value) {
            addCriterion("total_energy_money <>", value, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyGreaterThan(BigDecimal value) {
            addCriterion("total_energy_money >", value, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_energy_money >=", value, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyLessThan(BigDecimal value) {
            addCriterion("total_energy_money <", value, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_energy_money <=", value, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyIn(List<BigDecimal> values) {
            addCriterion("total_energy_money in", values, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyNotIn(List<BigDecimal> values) {
            addCriterion("total_energy_money not in", values, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_energy_money between", value1, value2, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andTotalEnergyMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_energy_money not between", value1, value2, "totalEnergyMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyIsNull() {
            addCriterion("electric_money is null");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyIsNotNull() {
            addCriterion("electric_money is not null");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyEqualTo(BigDecimal value) {
            addCriterion("electric_money =", value, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyNotEqualTo(BigDecimal value) {
            addCriterion("electric_money <>", value, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyGreaterThan(BigDecimal value) {
            addCriterion("electric_money >", value, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("electric_money >=", value, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyLessThan(BigDecimal value) {
            addCriterion("electric_money <", value, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("electric_money <=", value, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyIn(List<BigDecimal> values) {
            addCriterion("electric_money in", values, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyNotIn(List<BigDecimal> values) {
            addCriterion("electric_money not in", values, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("electric_money between", value1, value2, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andElectricMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("electric_money not between", value1, value2, "electricMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyIsNull() {
            addCriterion("coal_money is null");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyIsNotNull() {
            addCriterion("coal_money is not null");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyEqualTo(BigDecimal value) {
            addCriterion("coal_money =", value, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyNotEqualTo(BigDecimal value) {
            addCriterion("coal_money <>", value, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyGreaterThan(BigDecimal value) {
            addCriterion("coal_money >", value, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coal_money >=", value, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyLessThan(BigDecimal value) {
            addCriterion("coal_money <", value, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coal_money <=", value, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyIn(List<BigDecimal> values) {
            addCriterion("coal_money in", values, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyNotIn(List<BigDecimal> values) {
            addCriterion("coal_money not in", values, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coal_money between", value1, value2, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andCoalMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coal_money not between", value1, value2, "coalMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyIsNull() {
            addCriterion("natural_gas_money is null");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyIsNotNull() {
            addCriterion("natural_gas_money is not null");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyEqualTo(BigDecimal value) {
            addCriterion("natural_gas_money =", value, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyNotEqualTo(BigDecimal value) {
            addCriterion("natural_gas_money <>", value, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyGreaterThan(BigDecimal value) {
            addCriterion("natural_gas_money >", value, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("natural_gas_money >=", value, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyLessThan(BigDecimal value) {
            addCriterion("natural_gas_money <", value, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("natural_gas_money <=", value, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyIn(List<BigDecimal> values) {
            addCriterion("natural_gas_money in", values, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyNotIn(List<BigDecimal> values) {
            addCriterion("natural_gas_money not in", values, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("natural_gas_money between", value1, value2, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andNaturalGasMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("natural_gas_money not between", value1, value2, "naturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyIsNull() {
            addCriterion("liquified_natural_gas_money is null");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyIsNotNull() {
            addCriterion("liquified_natural_gas_money is not null");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyEqualTo(BigDecimal value) {
            addCriterion("liquified_natural_gas_money =", value, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyNotEqualTo(BigDecimal value) {
            addCriterion("liquified_natural_gas_money <>", value, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyGreaterThan(BigDecimal value) {
            addCriterion("liquified_natural_gas_money >", value, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("liquified_natural_gas_money >=", value, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyLessThan(BigDecimal value) {
            addCriterion("liquified_natural_gas_money <", value, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("liquified_natural_gas_money <=", value, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyIn(List<BigDecimal> values) {
            addCriterion("liquified_natural_gas_money in", values, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyNotIn(List<BigDecimal> values) {
            addCriterion("liquified_natural_gas_money not in", values, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("liquified_natural_gas_money between", value1, value2, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andLiquifiedNaturalGasMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("liquified_natural_gas_money not between", value1, value2, "liquifiedNaturalGasMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyIsNull() {
            addCriterion("gasoline_money is null");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyIsNotNull() {
            addCriterion("gasoline_money is not null");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyEqualTo(BigDecimal value) {
            addCriterion("gasoline_money =", value, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyNotEqualTo(BigDecimal value) {
            addCriterion("gasoline_money <>", value, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyGreaterThan(BigDecimal value) {
            addCriterion("gasoline_money >", value, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("gasoline_money >=", value, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyLessThan(BigDecimal value) {
            addCriterion("gasoline_money <", value, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("gasoline_money <=", value, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyIn(List<BigDecimal> values) {
            addCriterion("gasoline_money in", values, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyNotIn(List<BigDecimal> values) {
            addCriterion("gasoline_money not in", values, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gasoline_money between", value1, value2, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andGasolineMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("gasoline_money not between", value1, value2, "gasolineMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyIsNull() {
            addCriterion("diesel_oil_money is null");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyIsNotNull() {
            addCriterion("diesel_oil_money is not null");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyEqualTo(BigDecimal value) {
            addCriterion("diesel_oil_money =", value, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyNotEqualTo(BigDecimal value) {
            addCriterion("diesel_oil_money <>", value, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyGreaterThan(BigDecimal value) {
            addCriterion("diesel_oil_money >", value, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("diesel_oil_money >=", value, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyLessThan(BigDecimal value) {
            addCriterion("diesel_oil_money <", value, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("diesel_oil_money <=", value, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyIn(List<BigDecimal> values) {
            addCriterion("diesel_oil_money in", values, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyNotIn(List<BigDecimal> values) {
            addCriterion("diesel_oil_money not in", values, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("diesel_oil_money between", value1, value2, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andDieselOilMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("diesel_oil_money not between", value1, value2, "dieselOilMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyIsNull() {
            addCriterion("purchasing_heat_money is null");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyIsNotNull() {
            addCriterion("purchasing_heat_money is not null");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyEqualTo(BigDecimal value) {
            addCriterion("purchasing_heat_money =", value, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyNotEqualTo(BigDecimal value) {
            addCriterion("purchasing_heat_money <>", value, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyGreaterThan(BigDecimal value) {
            addCriterion("purchasing_heat_money >", value, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("purchasing_heat_money >=", value, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyLessThan(BigDecimal value) {
            addCriterion("purchasing_heat_money <", value, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("purchasing_heat_money <=", value, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyIn(List<BigDecimal> values) {
            addCriterion("purchasing_heat_money in", values, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyNotIn(List<BigDecimal> values) {
            addCriterion("purchasing_heat_money not in", values, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchasing_heat_money between", value1, value2, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andPurchasingHeatMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchasing_heat_money not between", value1, value2, "purchasingHeatMoney");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIsNull() {
            addCriterion("unit_head is null");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIsNotNull() {
            addCriterion("unit_head is not null");
            return (Criteria) this;
        }

        public Criteria andUnitHeadEqualTo(String value) {
            addCriterion("unit_head =", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotEqualTo(String value) {
            addCriterion("unit_head <>", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadGreaterThan(String value) {
            addCriterion("unit_head >", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadGreaterThanOrEqualTo(String value) {
            addCriterion("unit_head >=", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLessThan(String value) {
            addCriterion("unit_head <", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLessThanOrEqualTo(String value) {
            addCriterion("unit_head <=", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLike(String value) {
            addCriterion("unit_head like", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotLike(String value) {
            addCriterion("unit_head not like", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIn(List<String> values) {
            addCriterion("unit_head in", values, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotIn(List<String> values) {
            addCriterion("unit_head not in", values, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadBetween(String value1, String value2) {
            addCriterion("unit_head between", value1, value2, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotBetween(String value1, String value2) {
            addCriterion("unit_head not between", value1, value2, "unitHead");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIsNull() {
            addCriterion("statistical_control_officer is null");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIsNotNull() {
            addCriterion("statistical_control_officer is not null");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerEqualTo(String value) {
            addCriterion("statistical_control_officer =", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotEqualTo(String value) {
            addCriterion("statistical_control_officer <>", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerGreaterThan(String value) {
            addCriterion("statistical_control_officer >", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerGreaterThanOrEqualTo(String value) {
            addCriterion("statistical_control_officer >=", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLessThan(String value) {
            addCriterion("statistical_control_officer <", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLessThanOrEqualTo(String value) {
            addCriterion("statistical_control_officer <=", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLike(String value) {
            addCriterion("statistical_control_officer like", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotLike(String value) {
            addCriterion("statistical_control_officer not like", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIn(List<String> values) {
            addCriterion("statistical_control_officer in", values, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotIn(List<String> values) {
            addCriterion("statistical_control_officer not in", values, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerBetween(String value1, String value2) {
            addCriterion("statistical_control_officer between", value1, value2, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotBetween(String value1, String value2) {
            addCriterion("statistical_control_officer not between", value1, value2, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNull() {
            addCriterion("fill_form_by is null");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNotNull() {
            addCriterion("fill_form_by is not null");
            return (Criteria) this;
        }

        public Criteria andFillFormByEqualTo(String value) {
            addCriterion("fill_form_by =", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotEqualTo(String value) {
            addCriterion("fill_form_by <>", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThan(String value) {
            addCriterion("fill_form_by >", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThanOrEqualTo(String value) {
            addCriterion("fill_form_by >=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThan(String value) {
            addCriterion("fill_form_by <", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThanOrEqualTo(String value) {
            addCriterion("fill_form_by <=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLike(String value) {
            addCriterion("fill_form_by like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotLike(String value) {
            addCriterion("fill_form_by not like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByIn(List<String> values) {
            addCriterion("fill_form_by in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotIn(List<String> values) {
            addCriterion("fill_form_by not in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByBetween(String value1, String value2) {
            addCriterion("fill_form_by between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotBetween(String value1, String value2) {
            addCriterion("fill_form_by not between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}