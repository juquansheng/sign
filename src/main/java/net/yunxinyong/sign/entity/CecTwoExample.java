package net.yunxinyong.sign.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CecTwoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CecTwoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNull() {
            addCriterion("inv_id is null");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNotNull() {
            addCriterion("inv_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvIdEqualTo(Integer value) {
            addCriterion("inv_id =", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotEqualTo(Integer value) {
            addCriterion("inv_id <>", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThan(Integer value) {
            addCriterion("inv_id >", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("inv_id >=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThan(Integer value) {
            addCriterion("inv_id <", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThanOrEqualTo(Integer value) {
            addCriterion("inv_id <=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdIn(List<Integer> values) {
            addCriterion("inv_id in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotIn(List<Integer> values) {
            addCriterion("inv_id not in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdBetween(Integer value1, Integer value2) {
            addCriterion("inv_id between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotBetween(Integer value1, Integer value2) {
            addCriterion("inv_id not between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNull() {
            addCriterion("organization_code is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNotNull() {
            addCriterion("organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeEqualTo(String value) {
            addCriterion("organization_code =", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotEqualTo(String value) {
            addCriterion("organization_code <>", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThan(String value) {
            addCriterion("organization_code >", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("organization_code >=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThan(String value) {
            addCriterion("organization_code <", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("organization_code <=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLike(String value) {
            addCriterion("organization_code like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotLike(String value) {
            addCriterion("organization_code not like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIn(List<String> values) {
            addCriterion("organization_code in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotIn(List<String> values) {
            addCriterion("organization_code not in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeBetween(String value1, String value2) {
            addCriterion("organization_code between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("organization_code not between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNull() {
            addCriterion("social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNotNull() {
            addCriterion("social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeEqualTo(String value) {
            addCriterion("social_credit_code =", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotEqualTo(String value) {
            addCriterion("social_credit_code <>", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThan(String value) {
            addCriterion("social_credit_code >", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("social_credit_code >=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThan(String value) {
            addCriterion("social_credit_code <", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("social_credit_code <=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLike(String value) {
            addCriterion("social_credit_code like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotLike(String value) {
            addCriterion("social_credit_code not like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIn(List<String> values) {
            addCriterion("social_credit_code in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotIn(List<String> values) {
            addCriterion("social_credit_code not in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("social_credit_code between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("social_credit_code not between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNull() {
            addCriterion("unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNotNull() {
            addCriterion("unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameEqualTo(String value) {
            addCriterion("unit_detailed_name =", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotEqualTo(String value) {
            addCriterion("unit_detailed_name <>", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThan(String value) {
            addCriterion("unit_detailed_name >", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name >=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThan(String value) {
            addCriterion("unit_detailed_name <", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name <=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLike(String value) {
            addCriterion("unit_detailed_name like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotLike(String value) {
            addCriterion("unit_detailed_name not like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIn(List<String> values) {
            addCriterion("unit_detailed_name in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotIn(List<String> values) {
            addCriterion("unit_detailed_name not in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("unit_detailed_name between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("unit_detailed_name not between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberIsNull() {
            addCriterion("final_employees_number is null");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberIsNotNull() {
            addCriterion("final_employees_number is not null");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberEqualTo(Integer value) {
            addCriterion("final_employees_number =", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberNotEqualTo(Integer value) {
            addCriterion("final_employees_number <>", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberGreaterThan(Integer value) {
            addCriterion("final_employees_number >", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("final_employees_number >=", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberLessThan(Integer value) {
            addCriterion("final_employees_number <", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberLessThanOrEqualTo(Integer value) {
            addCriterion("final_employees_number <=", value, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberIn(List<Integer> values) {
            addCriterion("final_employees_number in", values, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberNotIn(List<Integer> values) {
            addCriterion("final_employees_number not in", values, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberBetween(Integer value1, Integer value2) {
            addCriterion("final_employees_number between", value1, value2, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFinalEmployeesNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("final_employees_number not between", value1, value2, "finalEmployeesNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberIsNull() {
            addCriterion("female_number is null");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberIsNotNull() {
            addCriterion("female_number is not null");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberEqualTo(Integer value) {
            addCriterion("female_number =", value, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberNotEqualTo(Integer value) {
            addCriterion("female_number <>", value, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberGreaterThan(Integer value) {
            addCriterion("female_number >", value, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("female_number >=", value, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberLessThan(Integer value) {
            addCriterion("female_number <", value, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberLessThanOrEqualTo(Integer value) {
            addCriterion("female_number <=", value, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberIn(List<Integer> values) {
            addCriterion("female_number in", values, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberNotIn(List<Integer> values) {
            addCriterion("female_number not in", values, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberBetween(Integer value1, Integer value2) {
            addCriterion("female_number between", value1, value2, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andFemaleNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("female_number not between", value1, value2, "femaleNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberIsNull() {
            addCriterion("postgraduate_number is null");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberIsNotNull() {
            addCriterion("postgraduate_number is not null");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberEqualTo(Integer value) {
            addCriterion("postgraduate_number =", value, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberNotEqualTo(Integer value) {
            addCriterion("postgraduate_number <>", value, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberGreaterThan(Integer value) {
            addCriterion("postgraduate_number >", value, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("postgraduate_number >=", value, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberLessThan(Integer value) {
            addCriterion("postgraduate_number <", value, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberLessThanOrEqualTo(Integer value) {
            addCriterion("postgraduate_number <=", value, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberIn(List<Integer> values) {
            addCriterion("postgraduate_number in", values, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberNotIn(List<Integer> values) {
            addCriterion("postgraduate_number not in", values, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberBetween(Integer value1, Integer value2) {
            addCriterion("postgraduate_number between", value1, value2, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andPostgraduateNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("postgraduate_number not between", value1, value2, "postgraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberIsNull() {
            addCriterion("undergraduate_number is null");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberIsNotNull() {
            addCriterion("undergraduate_number is not null");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberEqualTo(Integer value) {
            addCriterion("undergraduate_number =", value, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberNotEqualTo(Integer value) {
            addCriterion("undergraduate_number <>", value, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberGreaterThan(Integer value) {
            addCriterion("undergraduate_number >", value, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("undergraduate_number >=", value, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberLessThan(Integer value) {
            addCriterion("undergraduate_number <", value, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberLessThanOrEqualTo(Integer value) {
            addCriterion("undergraduate_number <=", value, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberIn(List<Integer> values) {
            addCriterion("undergraduate_number in", values, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberNotIn(List<Integer> values) {
            addCriterion("undergraduate_number not in", values, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberBetween(Integer value1, Integer value2) {
            addCriterion("undergraduate_number between", value1, value2, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andUndergraduateNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("undergraduate_number not between", value1, value2, "undergraduateNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberIsNull() {
            addCriterion("college_number is null");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberIsNotNull() {
            addCriterion("college_number is not null");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberEqualTo(Integer value) {
            addCriterion("college_number =", value, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberNotEqualTo(Integer value) {
            addCriterion("college_number <>", value, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberGreaterThan(Integer value) {
            addCriterion("college_number >", value, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("college_number >=", value, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberLessThan(Integer value) {
            addCriterion("college_number <", value, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberLessThanOrEqualTo(Integer value) {
            addCriterion("college_number <=", value, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberIn(List<Integer> values) {
            addCriterion("college_number in", values, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberNotIn(List<Integer> values) {
            addCriterion("college_number not in", values, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberBetween(Integer value1, Integer value2) {
            addCriterion("college_number between", value1, value2, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andCollegeNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("college_number not between", value1, value2, "collegeNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberIsNull() {
            addCriterion("skilled_workers_number is null");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberIsNotNull() {
            addCriterion("skilled_workers_number is not null");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberEqualTo(Integer value) {
            addCriterion("skilled_workers_number =", value, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberNotEqualTo(Integer value) {
            addCriterion("skilled_workers_number <>", value, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberGreaterThan(Integer value) {
            addCriterion("skilled_workers_number >", value, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("skilled_workers_number >=", value, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberLessThan(Integer value) {
            addCriterion("skilled_workers_number <", value, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberLessThanOrEqualTo(Integer value) {
            addCriterion("skilled_workers_number <=", value, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberIn(List<Integer> values) {
            addCriterion("skilled_workers_number in", values, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberNotIn(List<Integer> values) {
            addCriterion("skilled_workers_number not in", values, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberBetween(Integer value1, Integer value2) {
            addCriterion("skilled_workers_number between", value1, value2, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledWorkersNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("skilled_workers_number not between", value1, value2, "skilledWorkersNumber");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneIsNull() {
            addCriterion("skilled_level_one is null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneIsNotNull() {
            addCriterion("skilled_level_one is not null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneEqualTo(Integer value) {
            addCriterion("skilled_level_one =", value, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneNotEqualTo(Integer value) {
            addCriterion("skilled_level_one <>", value, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneGreaterThan(Integer value) {
            addCriterion("skilled_level_one >", value, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneGreaterThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_one >=", value, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneLessThan(Integer value) {
            addCriterion("skilled_level_one <", value, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneLessThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_one <=", value, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneIn(List<Integer> values) {
            addCriterion("skilled_level_one in", values, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneNotIn(List<Integer> values) {
            addCriterion("skilled_level_one not in", values, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_one between", value1, value2, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelOneNotBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_one not between", value1, value2, "skilledLevelOne");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoIsNull() {
            addCriterion("skilled_level_two is null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoIsNotNull() {
            addCriterion("skilled_level_two is not null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoEqualTo(Integer value) {
            addCriterion("skilled_level_two =", value, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoNotEqualTo(Integer value) {
            addCriterion("skilled_level_two <>", value, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoGreaterThan(Integer value) {
            addCriterion("skilled_level_two >", value, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoGreaterThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_two >=", value, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoLessThan(Integer value) {
            addCriterion("skilled_level_two <", value, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoLessThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_two <=", value, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoIn(List<Integer> values) {
            addCriterion("skilled_level_two in", values, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoNotIn(List<Integer> values) {
            addCriterion("skilled_level_two not in", values, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_two between", value1, value2, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelTwoNotBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_two not between", value1, value2, "skilledLevelTwo");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeIsNull() {
            addCriterion("skilled_level_three is null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeIsNotNull() {
            addCriterion("skilled_level_three is not null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeEqualTo(Integer value) {
            addCriterion("skilled_level_three =", value, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeNotEqualTo(Integer value) {
            addCriterion("skilled_level_three <>", value, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeGreaterThan(Integer value) {
            addCriterion("skilled_level_three >", value, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeGreaterThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_three >=", value, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeLessThan(Integer value) {
            addCriterion("skilled_level_three <", value, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeLessThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_three <=", value, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeIn(List<Integer> values) {
            addCriterion("skilled_level_three in", values, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeNotIn(List<Integer> values) {
            addCriterion("skilled_level_three not in", values, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_three between", value1, value2, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelThreeNotBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_three not between", value1, value2, "skilledLevelThree");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourIsNull() {
            addCriterion("skilled_level_four is null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourIsNotNull() {
            addCriterion("skilled_level_four is not null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourEqualTo(Integer value) {
            addCriterion("skilled_level_four =", value, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourNotEqualTo(Integer value) {
            addCriterion("skilled_level_four <>", value, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourGreaterThan(Integer value) {
            addCriterion("skilled_level_four >", value, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourGreaterThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_four >=", value, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourLessThan(Integer value) {
            addCriterion("skilled_level_four <", value, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourLessThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_four <=", value, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourIn(List<Integer> values) {
            addCriterion("skilled_level_four in", values, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourNotIn(List<Integer> values) {
            addCriterion("skilled_level_four not in", values, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_four between", value1, value2, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFourNotBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_four not between", value1, value2, "skilledLevelFour");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveIsNull() {
            addCriterion("skilled_level_five is null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveIsNotNull() {
            addCriterion("skilled_level_five is not null");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveEqualTo(Integer value) {
            addCriterion("skilled_level_five =", value, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveNotEqualTo(Integer value) {
            addCriterion("skilled_level_five <>", value, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveGreaterThan(Integer value) {
            addCriterion("skilled_level_five >", value, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveGreaterThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_five >=", value, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveLessThan(Integer value) {
            addCriterion("skilled_level_five <", value, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveLessThanOrEqualTo(Integer value) {
            addCriterion("skilled_level_five <=", value, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveIn(List<Integer> values) {
            addCriterion("skilled_level_five in", values, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveNotIn(List<Integer> values) {
            addCriterion("skilled_level_five not in", values, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_five between", value1, value2, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andSkilledLevelFiveNotBetween(Integer value1, Integer value2) {
            addCriterion("skilled_level_five not between", value1, value2, "skilledLevelFive");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceIsNull() {
            addCriterion("work_outside_province is null");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceIsNotNull() {
            addCriterion("work_outside_province is not null");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceEqualTo(Integer value) {
            addCriterion("work_outside_province =", value, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceNotEqualTo(Integer value) {
            addCriterion("work_outside_province <>", value, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceGreaterThan(Integer value) {
            addCriterion("work_outside_province >", value, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceGreaterThanOrEqualTo(Integer value) {
            addCriterion("work_outside_province >=", value, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceLessThan(Integer value) {
            addCriterion("work_outside_province <", value, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceLessThanOrEqualTo(Integer value) {
            addCriterion("work_outside_province <=", value, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceIn(List<Integer> values) {
            addCriterion("work_outside_province in", values, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceNotIn(List<Integer> values) {
            addCriterion("work_outside_province not in", values, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceBetween(Integer value1, Integer value2) {
            addCriterion("work_outside_province between", value1, value2, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkOutsideProvinceNotBetween(Integer value1, Integer value2) {
            addCriterion("work_outside_province not between", value1, value2, "workOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceIsNull() {
            addCriterion("domicile_outside_province is null");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceIsNotNull() {
            addCriterion("domicile_outside_province is not null");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceEqualTo(Integer value) {
            addCriterion("domicile_outside_province =", value, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceNotEqualTo(Integer value) {
            addCriterion("domicile_outside_province <>", value, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceGreaterThan(Integer value) {
            addCriterion("domicile_outside_province >", value, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceGreaterThanOrEqualTo(Integer value) {
            addCriterion("domicile_outside_province >=", value, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceLessThan(Integer value) {
            addCriterion("domicile_outside_province <", value, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceLessThanOrEqualTo(Integer value) {
            addCriterion("domicile_outside_province <=", value, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceIn(List<Integer> values) {
            addCriterion("domicile_outside_province in", values, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceNotIn(List<Integer> values) {
            addCriterion("domicile_outside_province not in", values, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceBetween(Integer value1, Integer value2) {
            addCriterion("domicile_outside_province between", value1, value2, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andDomicileOutsideProvinceNotBetween(Integer value1, Integer value2) {
            addCriterion("domicile_outside_province not between", value1, value2, "domicileOutsideProvince");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideIsNull() {
            addCriterion("work_and_domicile_outside is null");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideIsNotNull() {
            addCriterion("work_and_domicile_outside is not null");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideEqualTo(Integer value) {
            addCriterion("work_and_domicile_outside =", value, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideNotEqualTo(Integer value) {
            addCriterion("work_and_domicile_outside <>", value, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideGreaterThan(Integer value) {
            addCriterion("work_and_domicile_outside >", value, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideGreaterThanOrEqualTo(Integer value) {
            addCriterion("work_and_domicile_outside >=", value, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideLessThan(Integer value) {
            addCriterion("work_and_domicile_outside <", value, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideLessThanOrEqualTo(Integer value) {
            addCriterion("work_and_domicile_outside <=", value, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideIn(List<Integer> values) {
            addCriterion("work_and_domicile_outside in", values, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideNotIn(List<Integer> values) {
            addCriterion("work_and_domicile_outside not in", values, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideBetween(Integer value1, Integer value2) {
            addCriterion("work_and_domicile_outside between", value1, value2, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andWorkAndDomicileOutsideNotBetween(Integer value1, Integer value2) {
            addCriterion("work_and_domicile_outside not between", value1, value2, "workAndDomicileOutside");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNull() {
            addCriterion("fill_form_by is null");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNotNull() {
            addCriterion("fill_form_by is not null");
            return (Criteria) this;
        }

        public Criteria andFillFormByEqualTo(String value) {
            addCriterion("fill_form_by =", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotEqualTo(String value) {
            addCriterion("fill_form_by <>", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThan(String value) {
            addCriterion("fill_form_by >", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThanOrEqualTo(String value) {
            addCriterion("fill_form_by >=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThan(String value) {
            addCriterion("fill_form_by <", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThanOrEqualTo(String value) {
            addCriterion("fill_form_by <=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLike(String value) {
            addCriterion("fill_form_by like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotLike(String value) {
            addCriterion("fill_form_by not like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByIn(List<String> values) {
            addCriterion("fill_form_by in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotIn(List<String> values) {
            addCriterion("fill_form_by not in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByBetween(String value1, String value2) {
            addCriterion("fill_form_by between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotBetween(String value1, String value2) {
            addCriterion("fill_form_by not between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}