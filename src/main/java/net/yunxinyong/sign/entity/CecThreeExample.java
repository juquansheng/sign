package net.yunxinyong.sign.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CecThreeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CecThreeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNull() {
            addCriterion("inv_id is null");
            return (Criteria) this;
        }

        public Criteria andInvIdIsNotNull() {
            addCriterion("inv_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvIdEqualTo(Integer value) {
            addCriterion("inv_id =", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotEqualTo(Integer value) {
            addCriterion("inv_id <>", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThan(Integer value) {
            addCriterion("inv_id >", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("inv_id >=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThan(Integer value) {
            addCriterion("inv_id <", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdLessThanOrEqualTo(Integer value) {
            addCriterion("inv_id <=", value, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdIn(List<Integer> values) {
            addCriterion("inv_id in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotIn(List<Integer> values) {
            addCriterion("inv_id not in", values, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdBetween(Integer value1, Integer value2) {
            addCriterion("inv_id between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andInvIdNotBetween(Integer value1, Integer value2) {
            addCriterion("inv_id not between", value1, value2, "invId");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNull() {
            addCriterion("social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIsNotNull() {
            addCriterion("social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeEqualTo(String value) {
            addCriterion("social_credit_code =", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotEqualTo(String value) {
            addCriterion("social_credit_code <>", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThan(String value) {
            addCriterion("social_credit_code >", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("social_credit_code >=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThan(String value) {
            addCriterion("social_credit_code <", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("social_credit_code <=", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeLike(String value) {
            addCriterion("social_credit_code like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotLike(String value) {
            addCriterion("social_credit_code not like", value, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeIn(List<String> values) {
            addCriterion("social_credit_code in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotIn(List<String> values) {
            addCriterion("social_credit_code not in", values, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("social_credit_code between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("social_credit_code not between", value1, value2, "socialCreditCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNull() {
            addCriterion("organization_code is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIsNotNull() {
            addCriterion("organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeEqualTo(String value) {
            addCriterion("organization_code =", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotEqualTo(String value) {
            addCriterion("organization_code <>", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThan(String value) {
            addCriterion("organization_code >", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("organization_code >=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThan(String value) {
            addCriterion("organization_code <", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("organization_code <=", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeLike(String value) {
            addCriterion("organization_code like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotLike(String value) {
            addCriterion("organization_code not like", value, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeIn(List<String> values) {
            addCriterion("organization_code in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotIn(List<String> values) {
            addCriterion("organization_code not in", values, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeBetween(String value1, String value2) {
            addCriterion("organization_code between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("organization_code not between", value1, value2, "organizationCode");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNull() {
            addCriterion("unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIsNotNull() {
            addCriterion("unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameEqualTo(String value) {
            addCriterion("unit_detailed_name =", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotEqualTo(String value) {
            addCriterion("unit_detailed_name <>", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThan(String value) {
            addCriterion("unit_detailed_name >", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name >=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThan(String value) {
            addCriterion("unit_detailed_name <", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("unit_detailed_name <=", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameLike(String value) {
            addCriterion("unit_detailed_name like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotLike(String value) {
            addCriterion("unit_detailed_name not like", value, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameIn(List<String> values) {
            addCriterion("unit_detailed_name in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotIn(List<String> values) {
            addCriterion("unit_detailed_name not in", values, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("unit_detailed_name between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("unit_detailed_name not between", value1, value2, "unitDetailedName");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationIsNull() {
            addCriterion("this_year_depreciation is null");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationIsNotNull() {
            addCriterion("this_year_depreciation is not null");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationEqualTo(BigDecimal value) {
            addCriterion("this_year_depreciation =", value, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationNotEqualTo(BigDecimal value) {
            addCriterion("this_year_depreciation <>", value, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationGreaterThan(BigDecimal value) {
            addCriterion("this_year_depreciation >", value, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("this_year_depreciation >=", value, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationLessThan(BigDecimal value) {
            addCriterion("this_year_depreciation <", value, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationLessThanOrEqualTo(BigDecimal value) {
            addCriterion("this_year_depreciation <=", value, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationIn(List<BigDecimal> values) {
            addCriterion("this_year_depreciation in", values, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationNotIn(List<BigDecimal> values) {
            addCriterion("this_year_depreciation not in", values, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("this_year_depreciation between", value1, value2, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andThisYearDepreciationNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("this_year_depreciation not between", value1, value2, "thisYearDepreciation");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsIsNull() {
            addCriterion("fixed_assets is null");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsIsNotNull() {
            addCriterion("fixed_assets is not null");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsEqualTo(BigDecimal value) {
            addCriterion("fixed_assets =", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsNotEqualTo(BigDecimal value) {
            addCriterion("fixed_assets <>", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsGreaterThan(BigDecimal value) {
            addCriterion("fixed_assets >", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fixed_assets >=", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsLessThan(BigDecimal value) {
            addCriterion("fixed_assets <", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fixed_assets <=", value, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsIn(List<BigDecimal> values) {
            addCriterion("fixed_assets in", values, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsNotIn(List<BigDecimal> values) {
            addCriterion("fixed_assets not in", values, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fixed_assets between", value1, value2, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andFixedAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fixed_assets not between", value1, value2, "fixedAssets");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectIsNull() {
            addCriterion("construction_project is null");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectIsNotNull() {
            addCriterion("construction_project is not null");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectEqualTo(BigDecimal value) {
            addCriterion("construction_project =", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectNotEqualTo(BigDecimal value) {
            addCriterion("construction_project <>", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectGreaterThan(BigDecimal value) {
            addCriterion("construction_project >", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("construction_project >=", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectLessThan(BigDecimal value) {
            addCriterion("construction_project <", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectLessThanOrEqualTo(BigDecimal value) {
            addCriterion("construction_project <=", value, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectIn(List<BigDecimal> values) {
            addCriterion("construction_project in", values, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectNotIn(List<BigDecimal> values) {
            addCriterion("construction_project not in", values, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("construction_project between", value1, value2, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andConstructionProjectNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("construction_project not between", value1, value2, "constructionProject");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNull() {
            addCriterion("total_assets is null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNotNull() {
            addCriterion("total_assets is not null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsEqualTo(BigDecimal value) {
            addCriterion("total_assets =", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotEqualTo(BigDecimal value) {
            addCriterion("total_assets <>", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThan(BigDecimal value) {
            addCriterion("total_assets >", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_assets >=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThan(BigDecimal value) {
            addCriterion("total_assets <", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_assets <=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIn(List<BigDecimal> values) {
            addCriterion("total_assets in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotIn(List<BigDecimal> values) {
            addCriterion("total_assets not in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_assets between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_assets not between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalDebtIsNull() {
            addCriterion("total_debt is null");
            return (Criteria) this;
        }

        public Criteria andTotalDebtIsNotNull() {
            addCriterion("total_debt is not null");
            return (Criteria) this;
        }

        public Criteria andTotalDebtEqualTo(BigDecimal value) {
            addCriterion("total_debt =", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtNotEqualTo(BigDecimal value) {
            addCriterion("total_debt <>", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtGreaterThan(BigDecimal value) {
            addCriterion("total_debt >", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_debt >=", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtLessThan(BigDecimal value) {
            addCriterion("total_debt <", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_debt <=", value, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtIn(List<BigDecimal> values) {
            addCriterion("total_debt in", values, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtNotIn(List<BigDecimal> values) {
            addCriterion("total_debt not in", values, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_debt between", value1, value2, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andTotalDebtNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_debt not between", value1, value2, "totalDebt");
            return (Criteria) this;
        }

        public Criteria andBeginStockIsNull() {
            addCriterion("begin_stock is null");
            return (Criteria) this;
        }

        public Criteria andBeginStockIsNotNull() {
            addCriterion("begin_stock is not null");
            return (Criteria) this;
        }

        public Criteria andBeginStockEqualTo(BigDecimal value) {
            addCriterion("begin_stock =", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockNotEqualTo(BigDecimal value) {
            addCriterion("begin_stock <>", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockGreaterThan(BigDecimal value) {
            addCriterion("begin_stock >", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("begin_stock >=", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockLessThan(BigDecimal value) {
            addCriterion("begin_stock <", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockLessThanOrEqualTo(BigDecimal value) {
            addCriterion("begin_stock <=", value, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockIn(List<BigDecimal> values) {
            addCriterion("begin_stock in", values, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockNotIn(List<BigDecimal> values) {
            addCriterion("begin_stock not in", values, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("begin_stock between", value1, value2, "beginStock");
            return (Criteria) this;
        }

        public Criteria andBeginStockNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("begin_stock not between", value1, value2, "beginStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockIsNull() {
            addCriterion("end_term_stock is null");
            return (Criteria) this;
        }

        public Criteria andEndTermStockIsNotNull() {
            addCriterion("end_term_stock is not null");
            return (Criteria) this;
        }

        public Criteria andEndTermStockEqualTo(BigDecimal value) {
            addCriterion("end_term_stock =", value, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockNotEqualTo(BigDecimal value) {
            addCriterion("end_term_stock <>", value, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockGreaterThan(BigDecimal value) {
            addCriterion("end_term_stock >", value, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("end_term_stock >=", value, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockLessThan(BigDecimal value) {
            addCriterion("end_term_stock <", value, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockLessThanOrEqualTo(BigDecimal value) {
            addCriterion("end_term_stock <=", value, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockIn(List<BigDecimal> values) {
            addCriterion("end_term_stock in", values, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockNotIn(List<BigDecimal> values) {
            addCriterion("end_term_stock not in", values, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("end_term_stock between", value1, value2, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andEndTermStockNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("end_term_stock not between", value1, value2, "endTermStock");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeIsNull() {
            addCriterion("business_income is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeIsNotNull() {
            addCriterion("business_income is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeEqualTo(BigDecimal value) {
            addCriterion("business_income =", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeNotEqualTo(BigDecimal value) {
            addCriterion("business_income <>", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeGreaterThan(BigDecimal value) {
            addCriterion("business_income >", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("business_income >=", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLessThan(BigDecimal value) {
            addCriterion("business_income <", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("business_income <=", value, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeIn(List<BigDecimal> values) {
            addCriterion("business_income in", values, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeNotIn(List<BigDecimal> values) {
            addCriterion("business_income not in", values, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("business_income between", value1, value2, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("business_income not between", value1, value2, "businessIncome");
            return (Criteria) this;
        }

        public Criteria andBusinessCostIsNull() {
            addCriterion("business_cost is null");
            return (Criteria) this;
        }

        public Criteria andBusinessCostIsNotNull() {
            addCriterion("business_cost is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessCostEqualTo(BigDecimal value) {
            addCriterion("business_cost =", value, "businessCost");
            return (Criteria) this;
        }

        public Criteria andBusinessCostNotEqualTo(BigDecimal value) {
            addCriterion("business_cost <>", value, "businessCost");
            return (Criteria) this;
        }

        public Criteria andBusinessCostGreaterThan(BigDecimal value) {
            addCriterion("business_cost >", value, "businessCost");
            return (Criteria) this;
        }

        public Criteria andBusinessCostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("business_cost >=", value, "businessCost");
            return (Criteria) this;
        }

        public Criteria andBusinessCostLessThan(BigDecimal value) {
            addCriterion("business_cost <", value, "businessCost");
            return (Criteria) this;
        }

        public Criteria andBusinessCostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("business_cost <=", value, "businessCost");
            return (Criteria) this;
        }

        public Criteria andBusinessCostIn(List<BigDecimal> values) {
            addCriterion("business_cost in", values, "businessCost");
            return (Criteria) this;
        }

        public Criteria andBusinessCostNotIn(List<BigDecimal> values) {
            addCriterion("business_cost not in", values, "businessCost");
            return (Criteria) this;
        }

        public Criteria andBusinessCostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("business_cost between", value1, value2, "businessCost");
            return (Criteria) this;
        }

        public Criteria andBusinessCostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("business_cost not between", value1, value2, "businessCost");
            return (Criteria) this;
        }

        public Criteria andTaxationIsNull() {
            addCriterion("taxation is null");
            return (Criteria) this;
        }

        public Criteria andTaxationIsNotNull() {
            addCriterion("taxation is not null");
            return (Criteria) this;
        }

        public Criteria andTaxationEqualTo(BigDecimal value) {
            addCriterion("taxation =", value, "taxation");
            return (Criteria) this;
        }

        public Criteria andTaxationNotEqualTo(BigDecimal value) {
            addCriterion("taxation <>", value, "taxation");
            return (Criteria) this;
        }

        public Criteria andTaxationGreaterThan(BigDecimal value) {
            addCriterion("taxation >", value, "taxation");
            return (Criteria) this;
        }

        public Criteria andTaxationGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("taxation >=", value, "taxation");
            return (Criteria) this;
        }

        public Criteria andTaxationLessThan(BigDecimal value) {
            addCriterion("taxation <", value, "taxation");
            return (Criteria) this;
        }

        public Criteria andTaxationLessThanOrEqualTo(BigDecimal value) {
            addCriterion("taxation <=", value, "taxation");
            return (Criteria) this;
        }

        public Criteria andTaxationIn(List<BigDecimal> values) {
            addCriterion("taxation in", values, "taxation");
            return (Criteria) this;
        }

        public Criteria andTaxationNotIn(List<BigDecimal> values) {
            addCriterion("taxation not in", values, "taxation");
            return (Criteria) this;
        }

        public Criteria andTaxationBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taxation between", value1, value2, "taxation");
            return (Criteria) this;
        }

        public Criteria andTaxationNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("taxation not between", value1, value2, "taxation");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeIsNull() {
            addCriterion("investment_income is null");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeIsNotNull() {
            addCriterion("investment_income is not null");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeEqualTo(BigDecimal value) {
            addCriterion("investment_income =", value, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeNotEqualTo(BigDecimal value) {
            addCriterion("investment_income <>", value, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeGreaterThan(BigDecimal value) {
            addCriterion("investment_income >", value, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("investment_income >=", value, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeLessThan(BigDecimal value) {
            addCriterion("investment_income <", value, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("investment_income <=", value, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeIn(List<BigDecimal> values) {
            addCriterion("investment_income in", values, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeNotIn(List<BigDecimal> values) {
            addCriterion("investment_income not in", values, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("investment_income between", value1, value2, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andInvestmentIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("investment_income not between", value1, value2, "investmentIncome");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitIsNull() {
            addCriterion("operating_profit is null");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitIsNotNull() {
            addCriterion("operating_profit is not null");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitEqualTo(BigDecimal value) {
            addCriterion("operating_profit =", value, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitNotEqualTo(BigDecimal value) {
            addCriterion("operating_profit <>", value, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitGreaterThan(BigDecimal value) {
            addCriterion("operating_profit >", value, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("operating_profit >=", value, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitLessThan(BigDecimal value) {
            addCriterion("operating_profit <", value, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitLessThanOrEqualTo(BigDecimal value) {
            addCriterion("operating_profit <=", value, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitIn(List<BigDecimal> values) {
            addCriterion("operating_profit in", values, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitNotIn(List<BigDecimal> values) {
            addCriterion("operating_profit not in", values, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("operating_profit between", value1, value2, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andOperatingProfitNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("operating_profit not between", value1, value2, "operatingProfit");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationIsNull() {
            addCriterion("total_remuneration is null");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationIsNotNull() {
            addCriterion("total_remuneration is not null");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationEqualTo(BigDecimal value) {
            addCriterion("total_remuneration =", value, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationNotEqualTo(BigDecimal value) {
            addCriterion("total_remuneration <>", value, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationGreaterThan(BigDecimal value) {
            addCriterion("total_remuneration >", value, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_remuneration >=", value, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationLessThan(BigDecimal value) {
            addCriterion("total_remuneration <", value, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_remuneration <=", value, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationIn(List<BigDecimal> values) {
            addCriterion("total_remuneration in", values, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationNotIn(List<BigDecimal> values) {
            addCriterion("total_remuneration not in", values, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_remuneration between", value1, value2, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalRemunerationNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_remuneration not between", value1, value2, "totalRemuneration");
            return (Criteria) this;
        }

        public Criteria andTotalTaxIsNull() {
            addCriterion("total_tax is null");
            return (Criteria) this;
        }

        public Criteria andTotalTaxIsNotNull() {
            addCriterion("total_tax is not null");
            return (Criteria) this;
        }

        public Criteria andTotalTaxEqualTo(BigDecimal value) {
            addCriterion("total_tax =", value, "totalTax");
            return (Criteria) this;
        }

        public Criteria andTotalTaxNotEqualTo(BigDecimal value) {
            addCriterion("total_tax <>", value, "totalTax");
            return (Criteria) this;
        }

        public Criteria andTotalTaxGreaterThan(BigDecimal value) {
            addCriterion("total_tax >", value, "totalTax");
            return (Criteria) this;
        }

        public Criteria andTotalTaxGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_tax >=", value, "totalTax");
            return (Criteria) this;
        }

        public Criteria andTotalTaxLessThan(BigDecimal value) {
            addCriterion("total_tax <", value, "totalTax");
            return (Criteria) this;
        }

        public Criteria andTotalTaxLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_tax <=", value, "totalTax");
            return (Criteria) this;
        }

        public Criteria andTotalTaxIn(List<BigDecimal> values) {
            addCriterion("total_tax in", values, "totalTax");
            return (Criteria) this;
        }

        public Criteria andTotalTaxNotIn(List<BigDecimal> values) {
            addCriterion("total_tax not in", values, "totalTax");
            return (Criteria) this;
        }

        public Criteria andTotalTaxBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_tax between", value1, value2, "totalTax");
            return (Criteria) this;
        }

        public Criteria andTotalTaxNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_tax not between", value1, value2, "totalTax");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseIsNull() {
            addCriterion("commodity_purchase is null");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseIsNotNull() {
            addCriterion("commodity_purchase is not null");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseEqualTo(BigDecimal value) {
            addCriterion("commodity_purchase =", value, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseNotEqualTo(BigDecimal value) {
            addCriterion("commodity_purchase <>", value, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseGreaterThan(BigDecimal value) {
            addCriterion("commodity_purchase >", value, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("commodity_purchase >=", value, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseLessThan(BigDecimal value) {
            addCriterion("commodity_purchase <", value, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseLessThanOrEqualTo(BigDecimal value) {
            addCriterion("commodity_purchase <=", value, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseIn(List<BigDecimal> values) {
            addCriterion("commodity_purchase in", values, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseNotIn(List<BigDecimal> values) {
            addCriterion("commodity_purchase not in", values, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commodity_purchase between", value1, value2, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommodityPurchaseNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commodity_purchase not between", value1, value2, "commodityPurchase");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesIsNull() {
            addCriterion("commodity_sales is null");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesIsNotNull() {
            addCriterion("commodity_sales is not null");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesEqualTo(BigDecimal value) {
            addCriterion("commodity_sales =", value, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNotEqualTo(BigDecimal value) {
            addCriterion("commodity_sales <>", value, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesGreaterThan(BigDecimal value) {
            addCriterion("commodity_sales >", value, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("commodity_sales >=", value, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesLessThan(BigDecimal value) {
            addCriterion("commodity_sales <", value, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("commodity_sales <=", value, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesIn(List<BigDecimal> values) {
            addCriterion("commodity_sales in", values, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNotIn(List<BigDecimal> values) {
            addCriterion("commodity_sales not in", values, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commodity_sales between", value1, value2, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commodity_sales not between", value1, value2, "commoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesIsNull() {
            addCriterion("e_commodity_sales is null");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesIsNotNull() {
            addCriterion("e_commodity_sales is not null");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesEqualTo(BigDecimal value) {
            addCriterion("e_commodity_sales =", value, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesNotEqualTo(BigDecimal value) {
            addCriterion("e_commodity_sales <>", value, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesGreaterThan(BigDecimal value) {
            addCriterion("e_commodity_sales >", value, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("e_commodity_sales >=", value, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesLessThan(BigDecimal value) {
            addCriterion("e_commodity_sales <", value, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("e_commodity_sales <=", value, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesIn(List<BigDecimal> values) {
            addCriterion("e_commodity_sales in", values, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesNotIn(List<BigDecimal> values) {
            addCriterion("e_commodity_sales not in", values, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("e_commodity_sales between", value1, value2, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andECommoditySalesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("e_commodity_sales not between", value1, value2, "eCommoditySales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesIsNull() {
            addCriterion("retail_sales is null");
            return (Criteria) this;
        }

        public Criteria andRetailSalesIsNotNull() {
            addCriterion("retail_sales is not null");
            return (Criteria) this;
        }

        public Criteria andRetailSalesEqualTo(BigDecimal value) {
            addCriterion("retail_sales =", value, "retailSales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesNotEqualTo(BigDecimal value) {
            addCriterion("retail_sales <>", value, "retailSales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesGreaterThan(BigDecimal value) {
            addCriterion("retail_sales >", value, "retailSales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("retail_sales >=", value, "retailSales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesLessThan(BigDecimal value) {
            addCriterion("retail_sales <", value, "retailSales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("retail_sales <=", value, "retailSales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesIn(List<BigDecimal> values) {
            addCriterion("retail_sales in", values, "retailSales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesNotIn(List<BigDecimal> values) {
            addCriterion("retail_sales not in", values, "retailSales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("retail_sales between", value1, value2, "retailSales");
            return (Criteria) this;
        }

        public Criteria andRetailSalesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("retail_sales not between", value1, value2, "retailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesIsNull() {
            addCriterion("e_retail_sales is null");
            return (Criteria) this;
        }

        public Criteria andERetailSalesIsNotNull() {
            addCriterion("e_retail_sales is not null");
            return (Criteria) this;
        }

        public Criteria andERetailSalesEqualTo(BigDecimal value) {
            addCriterion("e_retail_sales =", value, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesNotEqualTo(BigDecimal value) {
            addCriterion("e_retail_sales <>", value, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesGreaterThan(BigDecimal value) {
            addCriterion("e_retail_sales >", value, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("e_retail_sales >=", value, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesLessThan(BigDecimal value) {
            addCriterion("e_retail_sales <", value, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("e_retail_sales <=", value, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesIn(List<BigDecimal> values) {
            addCriterion("e_retail_sales in", values, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesNotIn(List<BigDecimal> values) {
            addCriterion("e_retail_sales not in", values, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("e_retail_sales between", value1, value2, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andERetailSalesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("e_retail_sales not between", value1, value2, "eRetailSales");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockIsNull() {
            addCriterion("final_commodity_stock is null");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockIsNotNull() {
            addCriterion("final_commodity_stock is not null");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockEqualTo(BigDecimal value) {
            addCriterion("final_commodity_stock =", value, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockNotEqualTo(BigDecimal value) {
            addCriterion("final_commodity_stock <>", value, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockGreaterThan(BigDecimal value) {
            addCriterion("final_commodity_stock >", value, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("final_commodity_stock >=", value, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockLessThan(BigDecimal value) {
            addCriterion("final_commodity_stock <", value, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockLessThanOrEqualTo(BigDecimal value) {
            addCriterion("final_commodity_stock <=", value, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockIn(List<BigDecimal> values) {
            addCriterion("final_commodity_stock in", values, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockNotIn(List<BigDecimal> values) {
            addCriterion("final_commodity_stock not in", values, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("final_commodity_stock between", value1, value2, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andFinalCommodityStockNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("final_commodity_stock not between", value1, value2, "finalCommodityStock");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverIsNull() {
            addCriterion("service_turnover is null");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverIsNotNull() {
            addCriterion("service_turnover is not null");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverEqualTo(BigDecimal value) {
            addCriterion("service_turnover =", value, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverNotEqualTo(BigDecimal value) {
            addCriterion("service_turnover <>", value, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverGreaterThan(BigDecimal value) {
            addCriterion("service_turnover >", value, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("service_turnover >=", value, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverLessThan(BigDecimal value) {
            addCriterion("service_turnover <", value, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverLessThanOrEqualTo(BigDecimal value) {
            addCriterion("service_turnover <=", value, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverIn(List<BigDecimal> values) {
            addCriterion("service_turnover in", values, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverNotIn(List<BigDecimal> values) {
            addCriterion("service_turnover not in", values, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("service_turnover between", value1, value2, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andServiceTurnoverNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("service_turnover not between", value1, value2, "serviceTurnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverIsNull() {
            addCriterion("turnover is null");
            return (Criteria) this;
        }

        public Criteria andTurnoverIsNotNull() {
            addCriterion("turnover is not null");
            return (Criteria) this;
        }

        public Criteria andTurnoverEqualTo(BigDecimal value) {
            addCriterion("turnover =", value, "turnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverNotEqualTo(BigDecimal value) {
            addCriterion("turnover <>", value, "turnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverGreaterThan(BigDecimal value) {
            addCriterion("turnover >", value, "turnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("turnover >=", value, "turnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverLessThan(BigDecimal value) {
            addCriterion("turnover <", value, "turnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverLessThanOrEqualTo(BigDecimal value) {
            addCriterion("turnover <=", value, "turnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverIn(List<BigDecimal> values) {
            addCriterion("turnover in", values, "turnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverNotIn(List<BigDecimal> values) {
            addCriterion("turnover not in", values, "turnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("turnover between", value1, value2, "turnover");
            return (Criteria) this;
        }

        public Criteria andTurnoverNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("turnover not between", value1, value2, "turnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverIsNull() {
            addCriterion("e_turnover is null");
            return (Criteria) this;
        }

        public Criteria andETurnoverIsNotNull() {
            addCriterion("e_turnover is not null");
            return (Criteria) this;
        }

        public Criteria andETurnoverEqualTo(BigDecimal value) {
            addCriterion("e_turnover =", value, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverNotEqualTo(BigDecimal value) {
            addCriterion("e_turnover <>", value, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverGreaterThan(BigDecimal value) {
            addCriterion("e_turnover >", value, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("e_turnover >=", value, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverLessThan(BigDecimal value) {
            addCriterion("e_turnover <", value, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverLessThanOrEqualTo(BigDecimal value) {
            addCriterion("e_turnover <=", value, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverIn(List<BigDecimal> values) {
            addCriterion("e_turnover in", values, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverNotIn(List<BigDecimal> values) {
            addCriterion("e_turnover not in", values, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("e_turnover between", value1, value2, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andETurnoverNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("e_turnover not between", value1, value2, "eTurnover");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeIsNull() {
            addCriterion("room_income is null");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeIsNotNull() {
            addCriterion("room_income is not null");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeEqualTo(BigDecimal value) {
            addCriterion("room_income =", value, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeNotEqualTo(BigDecimal value) {
            addCriterion("room_income <>", value, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeGreaterThan(BigDecimal value) {
            addCriterion("room_income >", value, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("room_income >=", value, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeLessThan(BigDecimal value) {
            addCriterion("room_income <", value, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("room_income <=", value, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeIn(List<BigDecimal> values) {
            addCriterion("room_income in", values, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeNotIn(List<BigDecimal> values) {
            addCriterion("room_income not in", values, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("room_income between", value1, value2, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andRoomIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("room_income not between", value1, value2, "roomIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeIsNull() {
            addCriterion("meal_income is null");
            return (Criteria) this;
        }

        public Criteria andMealIncomeIsNotNull() {
            addCriterion("meal_income is not null");
            return (Criteria) this;
        }

        public Criteria andMealIncomeEqualTo(BigDecimal value) {
            addCriterion("meal_income =", value, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeNotEqualTo(BigDecimal value) {
            addCriterion("meal_income <>", value, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeGreaterThan(BigDecimal value) {
            addCriterion("meal_income >", value, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("meal_income >=", value, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeLessThan(BigDecimal value) {
            addCriterion("meal_income <", value, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("meal_income <=", value, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeIn(List<BigDecimal> values) {
            addCriterion("meal_income in", values, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeNotIn(List<BigDecimal> values) {
            addCriterion("meal_income not in", values, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("meal_income between", value1, value2, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andMealIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("meal_income not between", value1, value2, "mealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeIsNull() {
            addCriterion("e_meal_income is null");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeIsNotNull() {
            addCriterion("e_meal_income is not null");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeEqualTo(BigDecimal value) {
            addCriterion("e_meal_income =", value, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeNotEqualTo(BigDecimal value) {
            addCriterion("e_meal_income <>", value, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeGreaterThan(BigDecimal value) {
            addCriterion("e_meal_income >", value, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("e_meal_income >=", value, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeLessThan(BigDecimal value) {
            addCriterion("e_meal_income <", value, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("e_meal_income <=", value, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeIn(List<BigDecimal> values) {
            addCriterion("e_meal_income in", values, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeNotIn(List<BigDecimal> values) {
            addCriterion("e_meal_income not in", values, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("e_meal_income between", value1, value2, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andEMealIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("e_meal_income not between", value1, value2, "eMealIncome");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesIsNull() {
            addCriterion("second_commodity_sales is null");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesIsNotNull() {
            addCriterion("second_commodity_sales is not null");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesEqualTo(BigDecimal value) {
            addCriterion("second_commodity_sales =", value, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesNotEqualTo(BigDecimal value) {
            addCriterion("second_commodity_sales <>", value, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesGreaterThan(BigDecimal value) {
            addCriterion("second_commodity_sales >", value, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("second_commodity_sales >=", value, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesLessThan(BigDecimal value) {
            addCriterion("second_commodity_sales <", value, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("second_commodity_sales <=", value, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesIn(List<BigDecimal> values) {
            addCriterion("second_commodity_sales in", values, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesNotIn(List<BigDecimal> values) {
            addCriterion("second_commodity_sales not in", values, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("second_commodity_sales between", value1, value2, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondCommoditySalesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("second_commodity_sales not between", value1, value2, "secondCommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesIsNull() {
            addCriterion("second_e_commodity_sales is null");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesIsNotNull() {
            addCriterion("second_e_commodity_sales is not null");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesEqualTo(BigDecimal value) {
            addCriterion("second_e_commodity_sales =", value, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesNotEqualTo(BigDecimal value) {
            addCriterion("second_e_commodity_sales <>", value, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesGreaterThan(BigDecimal value) {
            addCriterion("second_e_commodity_sales >", value, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("second_e_commodity_sales >=", value, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesLessThan(BigDecimal value) {
            addCriterion("second_e_commodity_sales <", value, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("second_e_commodity_sales <=", value, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesIn(List<BigDecimal> values) {
            addCriterion("second_e_commodity_sales in", values, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesNotIn(List<BigDecimal> values) {
            addCriterion("second_e_commodity_sales not in", values, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("second_e_commodity_sales between", value1, value2, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andSecondECommoditySalesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("second_e_commodity_sales not between", value1, value2, "secondECommoditySales");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformIsNull() {
            addCriterion("is_have_e_platform is null");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformIsNotNull() {
            addCriterion("is_have_e_platform is not null");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformEqualTo(Integer value) {
            addCriterion("is_have_e_platform =", value, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformNotEqualTo(Integer value) {
            addCriterion("is_have_e_platform <>", value, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformGreaterThan(Integer value) {
            addCriterion("is_have_e_platform >", value, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_have_e_platform >=", value, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformLessThan(Integer value) {
            addCriterion("is_have_e_platform <", value, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformLessThanOrEqualTo(Integer value) {
            addCriterion("is_have_e_platform <=", value, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformIn(List<Integer> values) {
            addCriterion("is_have_e_platform in", values, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformNotIn(List<Integer> values) {
            addCriterion("is_have_e_platform not in", values, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformBetween(Integer value1, Integer value2) {
            addCriterion("is_have_e_platform between", value1, value2, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andIsHaveEPlatformNotBetween(Integer value1, Integer value2) {
            addCriterion("is_have_e_platform not between", value1, value2, "isHaveEPlatform");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberIsNull() {
            addCriterion("e_platform_number is null");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberIsNotNull() {
            addCriterion("e_platform_number is not null");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberEqualTo(Integer value) {
            addCriterion("e_platform_number =", value, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberNotEqualTo(Integer value) {
            addCriterion("e_platform_number <>", value, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberGreaterThan(Integer value) {
            addCriterion("e_platform_number >", value, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("e_platform_number >=", value, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberLessThan(Integer value) {
            addCriterion("e_platform_number <", value, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberLessThanOrEqualTo(Integer value) {
            addCriterion("e_platform_number <=", value, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberIn(List<Integer> values) {
            addCriterion("e_platform_number in", values, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberNotIn(List<Integer> values) {
            addCriterion("e_platform_number not in", values, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberBetween(Integer value1, Integer value2) {
            addCriterion("e_platform_number between", value1, value2, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEPlatformNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("e_platform_number not between", value1, value2, "ePlatformNumber");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityIsNull() {
            addCriterion("e_economic_activity is null");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityIsNotNull() {
            addCriterion("e_economic_activity is not null");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityEqualTo(String value) {
            addCriterion("e_economic_activity =", value, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityNotEqualTo(String value) {
            addCriterion("e_economic_activity <>", value, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityGreaterThan(String value) {
            addCriterion("e_economic_activity >", value, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityGreaterThanOrEqualTo(String value) {
            addCriterion("e_economic_activity >=", value, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityLessThan(String value) {
            addCriterion("e_economic_activity <", value, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityLessThanOrEqualTo(String value) {
            addCriterion("e_economic_activity <=", value, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityLike(String value) {
            addCriterion("e_economic_activity like", value, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityNotLike(String value) {
            addCriterion("e_economic_activity not like", value, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityIn(List<String> values) {
            addCriterion("e_economic_activity in", values, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityNotIn(List<String> values) {
            addCriterion("e_economic_activity not in", values, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityBetween(String value1, String value2) {
            addCriterion("e_economic_activity between", value1, value2, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andEEconomicActivityNotBetween(String value1, String value2) {
            addCriterion("e_economic_activity not between", value1, value2, "eEconomicActivity");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicIsNull() {
            addCriterion("other_economic is null");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicIsNotNull() {
            addCriterion("other_economic is not null");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicEqualTo(String value) {
            addCriterion("other_economic =", value, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicNotEqualTo(String value) {
            addCriterion("other_economic <>", value, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicGreaterThan(String value) {
            addCriterion("other_economic >", value, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicGreaterThanOrEqualTo(String value) {
            addCriterion("other_economic >=", value, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicLessThan(String value) {
            addCriterion("other_economic <", value, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicLessThanOrEqualTo(String value) {
            addCriterion("other_economic <=", value, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicLike(String value) {
            addCriterion("other_economic like", value, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicNotLike(String value) {
            addCriterion("other_economic not like", value, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicIn(List<String> values) {
            addCriterion("other_economic in", values, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicNotIn(List<String> values) {
            addCriterion("other_economic not in", values, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicBetween(String value1, String value2) {
            addCriterion("other_economic between", value1, value2, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andOtherEconomicNotBetween(String value1, String value2) {
            addCriterion("other_economic not between", value1, value2, "otherEconomic");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesIsNull() {
            addCriterion("e_business_activities is null");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesIsNotNull() {
            addCriterion("e_business_activities is not null");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesEqualTo(String value) {
            addCriterion("e_business_activities =", value, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesNotEqualTo(String value) {
            addCriterion("e_business_activities <>", value, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesGreaterThan(String value) {
            addCriterion("e_business_activities >", value, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesGreaterThanOrEqualTo(String value) {
            addCriterion("e_business_activities >=", value, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesLessThan(String value) {
            addCriterion("e_business_activities <", value, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesLessThanOrEqualTo(String value) {
            addCriterion("e_business_activities <=", value, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesLike(String value) {
            addCriterion("e_business_activities like", value, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesNotLike(String value) {
            addCriterion("e_business_activities not like", value, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesIn(List<String> values) {
            addCriterion("e_business_activities in", values, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesNotIn(List<String> values) {
            addCriterion("e_business_activities not in", values, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesBetween(String value1, String value2) {
            addCriterion("e_business_activities between", value1, value2, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andEBusinessActivitiesNotBetween(String value1, String value2) {
            addCriterion("e_business_activities not between", value1, value2, "eBusinessActivities");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessIsNull() {
            addCriterion("other_business is null");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessIsNotNull() {
            addCriterion("other_business is not null");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessEqualTo(String value) {
            addCriterion("other_business =", value, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessNotEqualTo(String value) {
            addCriterion("other_business <>", value, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessGreaterThan(String value) {
            addCriterion("other_business >", value, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessGreaterThanOrEqualTo(String value) {
            addCriterion("other_business >=", value, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessLessThan(String value) {
            addCriterion("other_business <", value, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessLessThanOrEqualTo(String value) {
            addCriterion("other_business <=", value, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessLike(String value) {
            addCriterion("other_business like", value, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessNotLike(String value) {
            addCriterion("other_business not like", value, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessIn(List<String> values) {
            addCriterion("other_business in", values, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessNotIn(List<String> values) {
            addCriterion("other_business not in", values, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessBetween(String value1, String value2) {
            addCriterion("other_business between", value1, value2, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andOtherBusinessNotBetween(String value1, String value2) {
            addCriterion("other_business not between", value1, value2, "otherBusiness");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeIsNull() {
            addCriterion("supplement_social_credit_code is null");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeIsNotNull() {
            addCriterion("supplement_social_credit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeEqualTo(String value) {
            addCriterion("supplement_social_credit_code =", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeNotEqualTo(String value) {
            addCriterion("supplement_social_credit_code <>", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeGreaterThan(String value) {
            addCriterion("supplement_social_credit_code >", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeGreaterThanOrEqualTo(String value) {
            addCriterion("supplement_social_credit_code >=", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeLessThan(String value) {
            addCriterion("supplement_social_credit_code <", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeLessThanOrEqualTo(String value) {
            addCriterion("supplement_social_credit_code <=", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeLike(String value) {
            addCriterion("supplement_social_credit_code like", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeNotLike(String value) {
            addCriterion("supplement_social_credit_code not like", value, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeIn(List<String> values) {
            addCriterion("supplement_social_credit_code in", values, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeNotIn(List<String> values) {
            addCriterion("supplement_social_credit_code not in", values, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeBetween(String value1, String value2) {
            addCriterion("supplement_social_credit_code between", value1, value2, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementSocialCreditCodeNotBetween(String value1, String value2) {
            addCriterion("supplement_social_credit_code not between", value1, value2, "supplementSocialCreditCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeIsNull() {
            addCriterion("supplement_organization_code is null");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeIsNotNull() {
            addCriterion("supplement_organization_code is not null");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeEqualTo(String value) {
            addCriterion("supplement_organization_code =", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeNotEqualTo(String value) {
            addCriterion("supplement_organization_code <>", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeGreaterThan(String value) {
            addCriterion("supplement_organization_code >", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("supplement_organization_code >=", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeLessThan(String value) {
            addCriterion("supplement_organization_code <", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeLessThanOrEqualTo(String value) {
            addCriterion("supplement_organization_code <=", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeLike(String value) {
            addCriterion("supplement_organization_code like", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeNotLike(String value) {
            addCriterion("supplement_organization_code not like", value, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeIn(List<String> values) {
            addCriterion("supplement_organization_code in", values, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeNotIn(List<String> values) {
            addCriterion("supplement_organization_code not in", values, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeBetween(String value1, String value2) {
            addCriterion("supplement_organization_code between", value1, value2, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementOrganizationCodeNotBetween(String value1, String value2) {
            addCriterion("supplement_organization_code not between", value1, value2, "supplementOrganizationCode");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameIsNull() {
            addCriterion("supplement_unit_detailed_name is null");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameIsNotNull() {
            addCriterion("supplement_unit_detailed_name is not null");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameEqualTo(String value) {
            addCriterion("supplement_unit_detailed_name =", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameNotEqualTo(String value) {
            addCriterion("supplement_unit_detailed_name <>", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameGreaterThan(String value) {
            addCriterion("supplement_unit_detailed_name >", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameGreaterThanOrEqualTo(String value) {
            addCriterion("supplement_unit_detailed_name >=", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameLessThan(String value) {
            addCriterion("supplement_unit_detailed_name <", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameLessThanOrEqualTo(String value) {
            addCriterion("supplement_unit_detailed_name <=", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameLike(String value) {
            addCriterion("supplement_unit_detailed_name like", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameNotLike(String value) {
            addCriterion("supplement_unit_detailed_name not like", value, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameIn(List<String> values) {
            addCriterion("supplement_unit_detailed_name in", values, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameNotIn(List<String> values) {
            addCriterion("supplement_unit_detailed_name not in", values, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameBetween(String value1, String value2) {
            addCriterion("supplement_unit_detailed_name between", value1, value2, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andSupplementUnitDetailedNameNotBetween(String value1, String value2) {
            addCriterion("supplement_unit_detailed_name not between", value1, value2, "supplementUnitDetailedName");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIsNull() {
            addCriterion("unit_head is null");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIsNotNull() {
            addCriterion("unit_head is not null");
            return (Criteria) this;
        }

        public Criteria andUnitHeadEqualTo(String value) {
            addCriterion("unit_head =", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotEqualTo(String value) {
            addCriterion("unit_head <>", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadGreaterThan(String value) {
            addCriterion("unit_head >", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadGreaterThanOrEqualTo(String value) {
            addCriterion("unit_head >=", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLessThan(String value) {
            addCriterion("unit_head <", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLessThanOrEqualTo(String value) {
            addCriterion("unit_head <=", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadLike(String value) {
            addCriterion("unit_head like", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotLike(String value) {
            addCriterion("unit_head not like", value, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadIn(List<String> values) {
            addCriterion("unit_head in", values, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotIn(List<String> values) {
            addCriterion("unit_head not in", values, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadBetween(String value1, String value2) {
            addCriterion("unit_head between", value1, value2, "unitHead");
            return (Criteria) this;
        }

        public Criteria andUnitHeadNotBetween(String value1, String value2) {
            addCriterion("unit_head not between", value1, value2, "unitHead");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIsNull() {
            addCriterion("statistical_control_officer is null");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIsNotNull() {
            addCriterion("statistical_control_officer is not null");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerEqualTo(String value) {
            addCriterion("statistical_control_officer =", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotEqualTo(String value) {
            addCriterion("statistical_control_officer <>", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerGreaterThan(String value) {
            addCriterion("statistical_control_officer >", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerGreaterThanOrEqualTo(String value) {
            addCriterion("statistical_control_officer >=", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLessThan(String value) {
            addCriterion("statistical_control_officer <", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLessThanOrEqualTo(String value) {
            addCriterion("statistical_control_officer <=", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerLike(String value) {
            addCriterion("statistical_control_officer like", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotLike(String value) {
            addCriterion("statistical_control_officer not like", value, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerIn(List<String> values) {
            addCriterion("statistical_control_officer in", values, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotIn(List<String> values) {
            addCriterion("statistical_control_officer not in", values, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerBetween(String value1, String value2) {
            addCriterion("statistical_control_officer between", value1, value2, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andStatisticalControlOfficerNotBetween(String value1, String value2) {
            addCriterion("statistical_control_officer not between", value1, value2, "statisticalControlOfficer");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNull() {
            addCriterion("fill_form_by is null");
            return (Criteria) this;
        }

        public Criteria andFillFormByIsNotNull() {
            addCriterion("fill_form_by is not null");
            return (Criteria) this;
        }

        public Criteria andFillFormByEqualTo(String value) {
            addCriterion("fill_form_by =", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotEqualTo(String value) {
            addCriterion("fill_form_by <>", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThan(String value) {
            addCriterion("fill_form_by >", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByGreaterThanOrEqualTo(String value) {
            addCriterion("fill_form_by >=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThan(String value) {
            addCriterion("fill_form_by <", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLessThanOrEqualTo(String value) {
            addCriterion("fill_form_by <=", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByLike(String value) {
            addCriterion("fill_form_by like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotLike(String value) {
            addCriterion("fill_form_by not like", value, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByIn(List<String> values) {
            addCriterion("fill_form_by in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotIn(List<String> values) {
            addCriterion("fill_form_by not in", values, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByBetween(String value1, String value2) {
            addCriterion("fill_form_by between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andFillFormByNotBetween(String value1, String value2) {
            addCriterion("fill_form_by not between", value1, value2, "fillFormBy");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}