package net.yunxinyong.sign.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

public class CecSix {
    private Integer id;

    private Integer invId;

    private String socialCreditCode;

    private String organizationCode;

    private String unitDetailedName;

    private BigDecimal beginStock;

    private BigDecimal totalCurrentAssets;

    private BigDecimal monetaryFunds;

    private BigDecimal cash;

    private BigDecimal stock;

    private BigDecimal securities;

    private BigDecimal longTermInvestment;

    private BigDecimal fixedAssets;

    private BigDecimal housesStructures;

    private BigDecimal machineryEquipment;

    private BigDecimal conveyance;

    private BigDecimal fixedAssetsUnderFinancingLease;

    private BigDecimal constructionProject;

    private BigDecimal culturalAssets;

    private BigDecimal intangibleAssets;

    private BigDecimal landUseRight;

    private BigDecimal totalAssets;

    private BigDecimal totalDebt;

    private BigDecimal totalNetAssets;

    private BigDecimal totalIncome;

    private BigDecimal donationIncome;

    private BigDecimal feeIncome;

    private BigDecimal totalExpenditure;

    private BigDecimal businessActivityCost;

    private BigDecimal activityPersonnelCosts;

    private BigDecimal activityDailyExpenses;

    private BigDecimal activityFixedAssetsDepreciation;

    private BigDecimal tax;

    private BigDecimal managementCost;

    private BigDecimal managementPersonnelCosts;

    private BigDecimal managementDailyExpenses;

    private BigDecimal managementFixedAssetsDepreciation;

    private BigDecimal managementTaxation;

    private BigDecimal netAssetChange;

    private String supplementSocialCreditCode;

    private String supplementOrganizationCode;

    private String supplementUnitDetailedName;

    private String unitHead;

    private String statisticalControlOfficer;

    private String fillFormBy;

    private String phone;

    private Integer state;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvId() {
        return invId;
    }

    public void setInvId(Integer invId) {
        this.invId = invId;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName == null ? null : unitDetailedName.trim();
    }

    public BigDecimal getBeginStock() {
        return beginStock;
    }

    public void setBeginStock(BigDecimal beginStock) {
        this.beginStock = beginStock;
    }

    public BigDecimal getTotalCurrentAssets() {
        return totalCurrentAssets;
    }

    public void setTotalCurrentAssets(BigDecimal totalCurrentAssets) {
        this.totalCurrentAssets = totalCurrentAssets;
    }

    public BigDecimal getMonetaryFunds() {
        return monetaryFunds;
    }

    public void setMonetaryFunds(BigDecimal monetaryFunds) {
        this.monetaryFunds = monetaryFunds;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getStock() {
        return stock;
    }

    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    public BigDecimal getSecurities() {
        return securities;
    }

    public void setSecurities(BigDecimal securities) {
        this.securities = securities;
    }

    public BigDecimal getLongTermInvestment() {
        return longTermInvestment;
    }

    public void setLongTermInvestment(BigDecimal longTermInvestment) {
        this.longTermInvestment = longTermInvestment;
    }

    public BigDecimal getFixedAssets() {
        return fixedAssets;
    }

    public void setFixedAssets(BigDecimal fixedAssets) {
        this.fixedAssets = fixedAssets;
    }

    public BigDecimal getHousesStructures() {
        return housesStructures;
    }

    public void setHousesStructures(BigDecimal housesStructures) {
        this.housesStructures = housesStructures;
    }

    public BigDecimal getMachineryEquipment() {
        return machineryEquipment;
    }

    public void setMachineryEquipment(BigDecimal machineryEquipment) {
        this.machineryEquipment = machineryEquipment;
    }

    public BigDecimal getConveyance() {
        return conveyance;
    }

    public void setConveyance(BigDecimal conveyance) {
        this.conveyance = conveyance;
    }

    public BigDecimal getFixedAssetsUnderFinancingLease() {
        return fixedAssetsUnderFinancingLease;
    }

    public void setFixedAssetsUnderFinancingLease(BigDecimal fixedAssetsUnderFinancingLease) {
        this.fixedAssetsUnderFinancingLease = fixedAssetsUnderFinancingLease;
    }

    public BigDecimal getConstructionProject() {
        return constructionProject;
    }

    public void setConstructionProject(BigDecimal constructionProject) {
        this.constructionProject = constructionProject;
    }

    public BigDecimal getCulturalAssets() {
        return culturalAssets;
    }

    public void setCulturalAssets(BigDecimal culturalAssets) {
        this.culturalAssets = culturalAssets;
    }

    public BigDecimal getIntangibleAssets() {
        return intangibleAssets;
    }

    public void setIntangibleAssets(BigDecimal intangibleAssets) {
        this.intangibleAssets = intangibleAssets;
    }

    public BigDecimal getLandUseRight() {
        return landUseRight;
    }

    public void setLandUseRight(BigDecimal landUseRight) {
        this.landUseRight = landUseRight;
    }

    public BigDecimal getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(BigDecimal totalAssets) {
        this.totalAssets = totalAssets;
    }

    public BigDecimal getTotalDebt() {
        return totalDebt;
    }

    public void setTotalDebt(BigDecimal totalDebt) {
        this.totalDebt = totalDebt;
    }

    public BigDecimal getTotalNetAssets() {
        return totalNetAssets;
    }

    public void setTotalNetAssets(BigDecimal totalNetAssets) {
        this.totalNetAssets = totalNetAssets;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public BigDecimal getDonationIncome() {
        return donationIncome;
    }

    public void setDonationIncome(BigDecimal donationIncome) {
        this.donationIncome = donationIncome;
    }

    public BigDecimal getFeeIncome() {
        return feeIncome;
    }

    public void setFeeIncome(BigDecimal feeIncome) {
        this.feeIncome = feeIncome;
    }

    public BigDecimal getTotalExpenditure() {
        return totalExpenditure;
    }

    public void setTotalExpenditure(BigDecimal totalExpenditure) {
        this.totalExpenditure = totalExpenditure;
    }

    public BigDecimal getBusinessActivityCost() {
        return businessActivityCost;
    }

    public void setBusinessActivityCost(BigDecimal businessActivityCost) {
        this.businessActivityCost = businessActivityCost;
    }

    public BigDecimal getActivityPersonnelCosts() {
        return activityPersonnelCosts;
    }

    public void setActivityPersonnelCosts(BigDecimal activityPersonnelCosts) {
        this.activityPersonnelCosts = activityPersonnelCosts;
    }

    public BigDecimal getActivityDailyExpenses() {
        return activityDailyExpenses;
    }

    public void setActivityDailyExpenses(BigDecimal activityDailyExpenses) {
        this.activityDailyExpenses = activityDailyExpenses;
    }

    public BigDecimal getActivityFixedAssetsDepreciation() {
        return activityFixedAssetsDepreciation;
    }

    public void setActivityFixedAssetsDepreciation(BigDecimal activityFixedAssetsDepreciation) {
        this.activityFixedAssetsDepreciation = activityFixedAssetsDepreciation;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getManagementCost() {
        return managementCost;
    }

    public void setManagementCost(BigDecimal managementCost) {
        this.managementCost = managementCost;
    }

    public BigDecimal getManagementPersonnelCosts() {
        return managementPersonnelCosts;
    }

    public void setManagementPersonnelCosts(BigDecimal managementPersonnelCosts) {
        this.managementPersonnelCosts = managementPersonnelCosts;
    }

    public BigDecimal getManagementDailyExpenses() {
        return managementDailyExpenses;
    }

    public void setManagementDailyExpenses(BigDecimal managementDailyExpenses) {
        this.managementDailyExpenses = managementDailyExpenses;
    }

    public BigDecimal getManagementFixedAssetsDepreciation() {
        return managementFixedAssetsDepreciation;
    }

    public void setManagementFixedAssetsDepreciation(BigDecimal managementFixedAssetsDepreciation) {
        this.managementFixedAssetsDepreciation = managementFixedAssetsDepreciation;
    }

    public BigDecimal getManagementTaxation() {
        return managementTaxation;
    }

    public void setManagementTaxation(BigDecimal managementTaxation) {
        this.managementTaxation = managementTaxation;
    }

    public BigDecimal getNetAssetChange() {
        return netAssetChange;
    }

    public void setNetAssetChange(BigDecimal netAssetChange) {
        this.netAssetChange = netAssetChange;
    }

    public String getSupplementSocialCreditCode() {
        return supplementSocialCreditCode;
    }

    public void setSupplementSocialCreditCode(String supplementSocialCreditCode) {
        this.supplementSocialCreditCode = supplementSocialCreditCode == null ? null : supplementSocialCreditCode.trim();
    }

    public String getSupplementOrganizationCode() {
        return supplementOrganizationCode;
    }

    public void setSupplementOrganizationCode(String supplementOrganizationCode) {
        this.supplementOrganizationCode = supplementOrganizationCode == null ? null : supplementOrganizationCode.trim();
    }

    public String getSupplementUnitDetailedName() {
        return supplementUnitDetailedName;
    }

    public void setSupplementUnitDetailedName(String supplementUnitDetailedName) {
        this.supplementUnitDetailedName = supplementUnitDetailedName == null ? null : supplementUnitDetailedName.trim();
    }

    public String getUnitHead() {
        return unitHead;
    }

    public void setUnitHead(String unitHead) {
        this.unitHead = unitHead == null ? null : unitHead.trim();
    }

    public String getStatisticalControlOfficer() {
        return statisticalControlOfficer;
    }

    public void setStatisticalControlOfficer(String statisticalControlOfficer) {
        this.statisticalControlOfficer = statisticalControlOfficer == null ? null : statisticalControlOfficer.trim();
    }

    public String getFillFormBy() {
        return fillFormBy;
    }

    public void setFillFormBy(String fillFormBy) {
        this.fillFormBy = fillFormBy == null ? null : fillFormBy.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}