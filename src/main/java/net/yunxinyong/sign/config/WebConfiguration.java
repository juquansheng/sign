package net.yunxinyong.sign.config;




import net.yunxinyong.sign.filter.RestFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 解决跨域请求 配置请求拦截器
 */
@Configuration
public class WebConfiguration {

    /*@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedHeaders("*")
                        .allowedMethods("*")
                        .allowedOrigins("*").allowCredentials(true);
            }
        };
    }*/

    @Bean
    public FilterRegistrationBean testFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new RestFilter());
        registration.addUrlPatterns("/*");
        registration.setName("restFilter");
        registration.setOrder(1);
        return registration;
    }
}