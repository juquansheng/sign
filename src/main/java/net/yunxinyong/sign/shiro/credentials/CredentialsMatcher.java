package net.yunxinyong.sign.shiro.credentials;



import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.service.AdminService;
import net.yunxinyong.sign.utils.PasswordUtil;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

import javax.annotation.Resource;

/**
 * Shiro-密码凭证匹配器（验证密码有效性）
 */
public class CredentialsMatcher extends SimpleCredentialsMatcher {

    @Resource
    private AdminService loginAdminService;

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        UsernamePasswordToken utoken = (UsernamePasswordToken) token;
        //获得用户输入的密码:(可以采用加盐(salt)的方式去检验)
        String inPassword = new String(utoken.getPassword());
        Object principals = info.getPrincipals();
        long l = Long.parseLong(principals.toString());
        SignAdmin admin = loginAdminService.getAdminById(Integer.parseInt(principals.toString()));
        //获得数据库中的密码
        String dbPassword = (String) info.getCredentials();
        try {
            inPassword = PasswordUtil.encrypt(inPassword, admin.getHash());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        //进行密码的比对
        return this.equals(inPassword, dbPassword);
    }
}
