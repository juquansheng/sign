package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.CecFive;

public interface CecFiveSerivce {
    CecFive select(int invid);

    void update(CecFive cecFice, Integer adminId);

    void insert(CecFive cecFice, Integer adminId);
}
