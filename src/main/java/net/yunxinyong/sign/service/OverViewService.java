package net.yunxinyong.sign.service;

import net.yunxinyong.sign.vo.StatusStatisticsVo;

import java.util.List;

public interface OverViewService {

    StatusStatisticsVo getOverView(int adminId, String street);

    /**
     * 根据传入的状态id和 所在街道查询  该街道内valueId 状态企业的id 列表
     * @param valueId
     * @param street
     * @return
     */
    List<Long> getRegisterSatus(Integer valueId,String street);

}
