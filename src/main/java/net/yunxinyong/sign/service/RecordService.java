package net.yunxinyong.sign.service;


import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.vo.SignRecordReqVo;

/** 操作记录相关模块
 *@describe
 *@author  ttbf
 *@date  2018/10/29
 */
public interface RecordService {
    /**
     * 添加操作记录
     * @param adminId 操作人id
     * @param invId 企业id
     * @param userId 被操作人id/状态id
     * @param tpye 操作类型
     * @return
     */
    boolean insert(Integer adminId, Integer invId,Integer userId, Integer tpye);

    /**
     * 获取操作记录
     * @param signRecordReqVo
     * @param adminId 操作人id
     * @param page
     * @param rows
     * @return
     */
    PageBean getList(SignRecordReqVo signRecordReqVo,Integer adminId, int page, int rows);
}
