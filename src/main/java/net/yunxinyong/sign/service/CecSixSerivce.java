package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.CecSix;

public interface CecSixSerivce {
    CecSix select(int invid);

    void update(CecSix cecSix, Integer adminId);

    void insert(CecSix cecSix, Integer adminId);
}
