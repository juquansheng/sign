package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.EcnomicInventory;
import net.yunxinyong.sign.entity.Image;
import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.vo.EcnomicReqVo;
import net.yunxinyong.sign.vo.InventoryVo;

import java.util.Map;

public interface EcnomicService {
    /**
     * 获取企业列表数据
     * @return
     */
    PageBean getList(EcnomicReqVo ecnomicReqVo, int page, int rows, Integer adminId);

    /**
     * 添加企业审核
     * @param invId 企业id
     * @param adminId 操作人id
     * @param status 审核结果
     * @return
     */
     EcnomicInventory examine(Integer invId,Integer adminId,Integer status);

    /**
     * 获取企业信息
     * @param invId
     * @return
     */
    EcnomicInventory get(Long invId);

    /**
     * 获取企业审核图片
     * @param invId
     * @param adminId
     * @return
     */
    Image getExamineImage(Integer invId,Integer adminId);
    void updateType(Integer invId);

    /**
     * 查询一共有多少家企业，有多少家企业已经认证
     * @param street 街道名字
     * @return  返回值是一个map，把查询出来的信息放在map中
     */
    Map getCount(String street);


    void insert(EcnomicInventory ecnomicInventory,Integer adminId);
}
