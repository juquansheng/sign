package net.yunxinyong.sign.service;


import net.yunxinyong.sign.entity.Street;
import net.yunxinyong.sign.entity.StreetCommunity;

import java.util.List;


/**
 *@describe 查询街道相关
 * @date 18/11/14
 * @author  李跃
 *
 */
public interface StreetService {

  List<Street> getAllStreet();

  /**
   * 查询社区
   * @param street 街道名称
   * @return
   */
  List<StreetCommunity> getCoummunity(String street);
}
