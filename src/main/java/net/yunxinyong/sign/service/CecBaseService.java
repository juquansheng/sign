package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.CecBase;

public interface CecBaseService {
    CecBase select(int invid);

    void update(CecBase cecBase, Integer adminId);

    void insert(CecBase cecBase, Integer adminId);
}
