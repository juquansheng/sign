package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.CecTwo;

public interface CecTwoService {

    CecTwo select(int invid);

    void update(CecTwo cecTwo, Integer adminId);

    void insert(CecTwo cecTwo, Integer adminId);
}
