package net.yunxinyong.sign.service;


import net.yunxinyong.sign.entity.AppointmentTime;
import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.vo.AppointmentTimeVo;

/**
 *@describe 预约上门
 *@author  ttbf
 *@date  2018/9/28
 */
public interface AppointmentTimeService {

    /**
     * 添加预约时间
     * @param appointmentTime
     * @return
     */
    AppointmentTime insertOrUpdate(AppointmentTime appointmentTime);

    /**
     * 获取
     * @param invId
     * @return
     */
    AppointmentTime get(Integer invId);

    PageBean getList(AppointmentTimeVo appointmentTimeVo);


}
