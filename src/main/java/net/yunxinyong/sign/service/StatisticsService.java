package net.yunxinyong.sign.service;

import net.yunxinyong.sign.newEntity.RequestRecent30;
import net.yunxinyong.sign.newEntity.ResultRecent30;
import net.yunxinyong.sign.utils.PageBean;

import java.util.List;

public interface StatisticsService {

    /**
     * 新增企业数量
     * @param area
     * @return
     */
    Integer getAmountByAdd(String area);

    /**
     * 根据状态值id获取数量
     * @param valueId
     * @param area
     * @return
     */
    Integer getAmountByStatus(Integer valueId,String area);

    /**
     * 根据状态值获取最近三十天每日数量
     * @param requestRecent30
     * @return
     */
    List<ResultRecent30> getList(RequestRecent30 requestRecent30);

    /**
     * 获取最近三十天每日新增数量
     * @param requestRecent30
     * @return
     */
    List<ResultRecent30> getAddList(RequestRecent30 requestRecent30);

    /**
     * 已通知未注册
     * @param requestRecent30
     * @return
     */
    Integer getNotRegister(RequestRecent30 requestRecent30);

    /**
     * 已注册未提交
     * @param requestRecent30
     * @return
     */
    Integer getNotCheck(RequestRecent30 requestRecent30);

    /**
     * 已审核未上传
     * @param area
     * @return
     */
    Integer getNotUpload(String area);

    /**
     * 已上传未录入
     * @param area
     * @return
     */
    Integer getNotSign(String area);





    /**
     * 未联系上（未注册，未填写）
     * @param area
     * @param page
     * @param rows
     * @return
     */
    PageBean notContact(String area, int page, int rows,Integer isAssign);

    /**
     * 已注册未填写
     * @param area
     * @param page
     * @param rows
     * @return
     */
    PageBean notWrite(String area, int page, int rows,Integer isAssign);

    /**
     * 已填写未未下载
     * @param area
     * @param page
     * @param rows
     * @return
     */
    PageBean notDownload(String area, int page, int rows,Integer isAssign);

    /**
     * 已完成未上传文件
     * @param area
     * @param page
     * @param rows
     * @return
     */
    PageBean notUpload(String area, int page, int rows,Integer isAssign);



    /**
     * 未联系上（未注册，未填写）
     * @param area
     * @return
     */
    List<Long> notContactList(String area,Integer isAssign);

    /**
     * 已注册未填写
     * @param area
     * @return
     */
    List<Long> notWriteList(String area,Integer isAssign);

    /**
     * 已填写未未下载
     * @param area
     * @return
     */
    List<Long> notDownloadList(String area,Integer isAssign);

    /**
     * 已完成未上传文件
     * @param area
     * @return
     */
    List<Long> notUploadList(String area,Integer isAssign);

}
