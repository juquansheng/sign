package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.Status;

import java.util.List;

/**
 *@describe 状态值管理
 *@author  ttbf
 *@date  2018/10/30
 */
public interface StatusService {

    /**
     * 添加状态
     * @param status
     * @return
     */
    boolean insert(Status status,Integer id);

    /**
     * 修改状态
     * @param status
     * @return
     */
    boolean update(Status status,Integer id);

    List<Status> getList(Integer parentId,Integer id);
}
