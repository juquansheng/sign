package net.yunxinyong.sign.service;



import java.io.OutputStream;
import java.util.List;


public interface ExcelService {

    void test(OutputStream outputStream);

    /**
     * 下载已经注册的企业（没有时间限制）
     * @return
     */
    void listEnterprise(OutputStream outputStream);



}
