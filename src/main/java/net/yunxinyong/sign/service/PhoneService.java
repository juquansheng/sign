package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.Phone;
import net.yunxinyong.sign.utils.PageBean;

/**
 *@describe 企业电话相关
 *@author  ttbf
 *@date  2018/11/12
 */
public interface PhoneService {

    Phone getPhone(Integer id);
    //修改
    Phone update(Phone phone,Integer adminId);
    //添加
    Phone insert(Phone phone,Integer adminId);
    //获取电话
    PageBean getList(Phone phone,Integer adminId,int page, int rows);
}
