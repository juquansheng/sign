package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.utils.PageBean;

/**
 *@describe 用户登录相关模块
 *@author  ttbf
 *@date  2018/10/29
 */
public interface AdminService {

    /**
     * 根据用户名获取普查员信息
     * @param phone
     * @return
     */
    SignAdmin getAdminByPhone(String phone);

    /**
     * 根据用户id获取普查员信息
     * @param id
     * @return
     */
    SignAdmin getAdminById(Integer id);

    PageBean getList(SignAdmin signAdmin,Integer id,int page, int rows);

    void update(SignAdmin signAdmin) throws Exception;
}
