package net.yunxinyong.sign.service;

import net.yunxinyong.sign.entity.RegisterStatus;
import net.yunxinyong.sign.entity.Status;
import net.yunxinyong.sign.vo.RegisterStatusVo;

import java.util.List;

/**
 *@describe  企业状态管理相关
 *@author  ttbf
 *@date  2018/10/30
 */
public interface RegisterStatusService {
    /**
     * 添加企业状态
     * @param registerStatus
     * @return
     */
    boolean insert(RegisterStatus registerStatus,Integer id);

    /**
     * 修改企业状态
     * @param registerStatus
     * @return
     */
    boolean update(RegisterStatus registerStatus,Integer id);

    /**
     * 根据企业id获取企业状态信息
     * @param invId 企业id
     * @return
     */
    List<RegisterStatusVo> getRegisterStatusList(Integer invId);

    /**
     * 更新企业上传文件状态
     * @param invId
     */
    void updateUploadStatus(Integer invId);

    /**
     * 更新企业是否能下载状态
     * @param invId
     */
    void updadeDownloadStatus(Integer invId);
    /**
     * 修改全部表格状态为不能修改 -1
     * @param invId
     */
    void  notChange(Integer invId);


}
