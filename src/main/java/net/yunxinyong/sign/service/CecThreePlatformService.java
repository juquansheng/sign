package net.yunxinyong.sign.service;



import net.yunxinyong.sign.entity.CecThreePlatform;

import java.util.List;

public interface CecThreePlatformService {
    /**
     * 添加,修改企业信息
     * @param
     * @return
     */
    void insertOrUpdate(List<CecThreePlatform> cecThreePlatformList);
    /**
     * 查询企业信息
     * @return
     */
    List<CecThreePlatform> get(Integer threeId);

    void insert(CecThreePlatform cecThreePlatform, Integer threeId);

    void update(CecThreePlatform cecThreePlatform);

    /**
     * 逻辑删除，
     * @param id 是分支表的id
     */
    void delete(Integer id);

}
