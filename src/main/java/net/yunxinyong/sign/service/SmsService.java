package net.yunxinyong.sign.service;

public interface SmsService {

    /**
     * 阿里云短息服务
     * @param mobile
     * @param code
     */
    void sendMessageByAliyun(String mobile, String code);

    /**
     * 发送审核结果
     * @param mobile
     */
    void sendExamine(String mobile,Integer type);

}
