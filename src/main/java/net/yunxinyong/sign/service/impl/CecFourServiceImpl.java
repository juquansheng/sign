package net.yunxinyong.sign.service.impl;

import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;
import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecFour;
import net.yunxinyong.sign.entity.CecFourExample;
import net.yunxinyong.sign.mapper.CecFourMapper;
import net.yunxinyong.sign.mapper.EcnomicInventoryMapper;
import net.yunxinyong.sign.service.CecFourService;
import net.yunxinyong.sign.service.RecordService;
import net.yunxinyong.sign.utils.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Log4j
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = CECException.class)
public class CecFourServiceImpl implements CecFourService {
    @Autowired
    private CecFourMapper cecFourMapper;
    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;
    @Autowired
    private RecordService recordService;
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = CECException.class)
    public void insertOrUpdate(List<CecFour> cecFourList) {
        //传入的id列表
        List<Integer> inputList = Lists.newArrayList();
        //查询的列表
        CecFourExample cecFourExample = new CecFourExample();
        cecFourExample.createCriteria().andStatusEqualTo(0).andInvIdEqualTo(cecFourList.get(0).getInvId());
        List<CecFour> cecFourSourceList = cecFourMapper.selectByExample(cecFourExample);
        Date date = new Date();
        //循环参数
        for (CecFour cecFour : cecFourList) {
            cecFour.setStatus(0);
            cecFour.setUpdateTime(date);
            if (cecFour.getFormType() == 1) {
                if (cecFour.getState() == null) {
                    cecFour.setState(0);
                }
            } else {
                cecFour.setState(0);
            }
            //如果id为空且添加表
            if (cecFour.getId() == null) {
                CecFour main = getMain(cecFour.getInvId());
                if (main != null && cecFour.getFormType() == 1) {
                    if (main.getState() != null && main.getState() == -1) {
                        throw new CECException(501,"企业状态为不可修改");
                    }
                    cecFour.setId(main.getId());
                    log.info("添加id为" + main.getId() + "数据");
                    int insert = cecFourMapper.updateByPrimaryKey(cecFour);
                    if (insert <= 0) {
                        log.error("添加id为" + cecFour.getUnitDetailedName() + "数据失败");
                        throw new CECException(501, "添加id为" + cecFour.getUnitDetailedName() + "数据失败");
                    }
                } else {
                    cecFour.setCreateTime(date);
                    int insert = cecFourMapper.insert(cecFour);
                    if (insert <= 0) {
                        log.error("添加id为" + cecFour.getUnitDetailedName() + "数据失败");
                        throw new CECException(501, "添加id为" + cecFour.getUnitDetailedName() + "数据失败");
                    }
                }

            } else {
                inputList.add(cecFour.getId());
                int update = cecFourMapper.updateByPrimaryKey(cecFour);
                if (update <= 0) {
                    log.error("更新名称为" + cecFour.getUnitDetailedName() + "数据失败");
                    throw new CECException(501, "更新名称为" + cecFour.getUnitDetailedName() + "数据失败");
                }
            }
        }
        //如果为空则不进行删除操作
        if (!ListUtils.isEmpty(cecFourSourceList)) {
            List<Integer> sourceList = Lists.newArrayList();
            for (CecFour cecFour : cecFourSourceList) {
                if (cecFour.getFormType() != 1) {
                    sourceList.add(cecFour.getId());
                }
            }
            sourceList.removeAll(inputList);
            for (Integer integer : sourceList) {
                int delete = cecFourMapper.deleteByPrimaryKey(integer);
                if (delete <= 0) {
                    log.error("integer+删除失败");
                    throw new CECException(501, "integer+删除失败");
                }
            }
        }
    }

    @Override
    public List<CecFour> get(Integer invId) {
        CecFourExample cecFourExample = new CecFourExample();
        cecFourExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        return cecFourMapper.selectByExample(cecFourExample);
    }

    private CecFour getMain(Integer invId) {
        //根据711base表的baseId 查询数据库中的数据 ,查询的时候要设置状态不是已删除的状态
        CecFourExample cecFourExample = new CecFourExample();
        cecFourExample.createCriteria().andInvIdEqualTo(invId).andStatusEqualTo(0);
        List<CecFour> cecFourList = cecFourMapper.selectByExample(cecFourExample);
        if (!ListUtils.isEmpty(cecFourList)) {
            for (CecFour cecFour : cecFourList) {
                if (cecFour.getFormType() == 1) {
                    return cecFour;
                }
            }
        }

        return null;

    }
}
