package net.yunxinyong.sign.service.impl;


import lombok.extern.log4j.Log4j2;
import net.yunxinyong.sign.entity.*;
import net.yunxinyong.sign.mapper.EcnomicInventoryMapper;
import net.yunxinyong.sign.mapper.NewMapper;
import net.yunxinyong.sign.mapper.RegisterStatusMapper;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.newEntity.RequestRecent30;
import net.yunxinyong.sign.service.OverViewService;
import net.yunxinyong.sign.vo.StatusStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
@Log4j2
public class OverViewServiceImpl implements OverViewService {
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;
    @Autowired
    private RegisterStatusMapper registerStatusMapper;
    @Autowired
    private NewMapper newMapper;

    @Override
    public StatusStatisticsVo getOverView(int adminId, String street) {
        long now = System.currentTimeMillis() / 1000l;
        long daySecond = 60 * 60 * 24;
        long dayTime = now - (now + 8 * 3600) % daySecond;
        Date date = new Date(Long.valueOf(dayTime + "000"));
        if (street == null) {
            SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
            if (signAdmin != null) {
                street = signAdmin.getArea();
            }
        }

        StatusStatisticsVo statusStatisticsVo = new StatusStatisticsVo();
        statusStatisticsVo.setTotal(getReferenceAddress02(street));
        statusStatisticsVo.setNewUnit(getNewUnit(street,date));
        statusStatisticsVo.setInformed(getInformed(street,date));
        statusStatisticsVo.setRegistered(getRegistered(street,date));
        statusStatisticsVo.setCompleted(getCompleted(street,date));
        statusStatisticsVo.setEntered(getEntered(street,date));
        statusStatisticsVo.setUploaded(getUploaded(street,date));
        return statusStatisticsVo;
    }

    /**
     * 根据传入的状态id和所在的街道查询， 在这种状态下 这个街道 企业的 id列表
     * @param valueId
     * @param street
     * @return
     */
    @Override
    public List<Long> getRegisterSatus(Integer valueId, String street) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setValueId(valueId);
        requestRecent30.setArea(street);
        return newMapper.getStatus(requestRecent30);
    }

    //获取街道所有的公司数量
    public Integer getReferenceAddress02(String area) {
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andReferenceAddress02EqualTo(area).andStatusEqualTo(1);
        return ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);
    }
    //当天添加企业
    public int getNewUnit(String area,Date date) {

        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andCreatetimeGreaterThan(date).andUiteStatusEqualTo(1).andReferenceAddress02EqualTo(area);
        int count = ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);
        log.info("今日新增企业:" + count + "个");
        return count;
    }

    //已通知
    public int getInformed(String area,Date date) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setStartTime(date);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(8);
        return newMapper.selectToday(requestRecent30);
    }

    //已注册
    public int getRegistered(String area,Date date) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setStartTime(date);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(23);
        return newMapper.selectToday(requestRecent30);
    }

    //已完成
    public int getCompleted(String area,Date date) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setStartTime(date);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(15);
        return newMapper.selectToday(requestRecent30);
    }

    //已录入
    public int getEntered(String area,Date date) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setStartTime(date);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(19);
        return newMapper.selectToday(requestRecent30);
    }

    //已上传
    public int getUploaded(String area,Date date) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setStartTime(date);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(17);
        return newMapper.selectToday(requestRecent30);
    }

}
