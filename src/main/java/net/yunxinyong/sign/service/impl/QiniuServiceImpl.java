package net.yunxinyong.sign.service.impl;


import com.google.common.collect.Lists;
import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.Image;
import net.yunxinyong.sign.entity.ImageExample;
import net.yunxinyong.sign.mapper.ImageMapper;
import net.yunxinyong.sign.service.QiniuService;
import net.yunxinyong.sign.service.RecordService;
import net.yunxinyong.sign.service.RegisterStatusService;
import net.yunxinyong.sign.utils.ListUtils;
import net.yunxinyong.sign.utils.QiniuUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;

@Service
public class QiniuServiceImpl implements QiniuService {

    @Autowired
    private ImageMapper imageMapper;
    @Autowired
    private RegisterStatusService registerStatusService;
    @Autowired
    private RecordService recordService;


    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = CECException.class)
    public void insert(List<Image> imageList,Integer invId,String name) {
        //先删除企业所有类型图片
        ImageExample imageExample = new ImageExample();
        imageExample.createCriteria().andInvIdEqualTo(invId).andStateEqualTo(0).andNameEqualTo(name);
        List<Image> images = imageMapper.selectByExample(imageExample);
        if (!ListUtils.isEmpty(images)){
            for (Image image:images){
                image.setState(-1);
                imageMapper.updateByPrimaryKeySelective(image);
            }
        }
        for (Image image:imageList){
            image.setType(2);
            image.setInvId(invId);
            image.setName(name);
            Date date = new Date();
            image.setUpdateTime(date);
            image.setCreateTime(date);
            int insertSelective = imageMapper.insertSelective(image);
            if (insertSelective <= 0){
                throw new CECException(501,"图片添加失败");
            }
        }
    }

    @Override
    public List<String> getUrl(Integer invId) {
        ImageExample imageExample = new ImageExample();
        imageExample.createCriteria().andNameEqualTo("all").andInvIdEqualTo(invId).andStateEqualTo(0);
        List<Image> imageList = imageMapper.selectByExample(imageExample);
        if (ListUtils.isEmpty(imageList)){
            throw new CECException(501,"未获取到图片信息");
        }
        List<String> stringList = Lists.newArrayList();
        for (Image image:imageList){
            stringList.add(QiniuUtils.getUrl(image.getUrl()));
        }
        return stringList;
    }

    @Override
    public List<String> getExamineUrl(Integer invId) {
        //这里添加操作记录
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        recordService.insert(adminId,invId,null,13);

        ImageExample imageExample = new ImageExample();
        imageExample.createCriteria().andTypeEqualTo(1).andInvIdEqualTo(invId).andStateEqualTo(0);
        List<Image> imageList = imageMapper.selectByExample(imageExample);
        if (ListUtils.isEmpty(imageList)){
            throw new CECException(501,"未获取到图片信息");
        }
        List<String> stringList = Lists.newArrayList();
        for (Image image:imageList){
            stringList.add(QiniuUtils.getUrl(image.getUrl()));
        }
        return stringList;
    }

    /**
     * * type = 1 为添加企业数据批量下载 =2 登记数据批量下载
     * @param idList
     * @param type
     * @param adminId
     * @return
     */
    @Override
    public String mkzip(List<Integer> idList,Integer type, Integer adminId) throws IOException {
        List<String> urlList = Lists.newArrayList();
        if (ListUtils.isEmpty(idList)){
            throw new CECException(501,"请选择企业");
        }
        for (Integer integer:idList){
            ImageExample imageExample = new ImageExample();
            imageExample.createCriteria().andStateEqualTo(0).andTypeEqualTo(type).andInvIdEqualTo(integer);
            List<Image> imageList = imageMapper.selectByExample(imageExample);
            if (!ListUtils.isEmpty(imageList)){

                //这里添加操作记录
                if (type == 1){
                    recordService.insert(adminId, integer, null, 14);
                }else if (type == 2){
                    recordService.insert(adminId, integer, null, 15);
                }

                for (Image image:imageList){
                    urlList.add(image.getUrl());
                }
            }
        }
        if (ListUtils.isEmpty(urlList)){
            throw new CECException(501,"未获取到文件");
        }
        Date date = new Date();
        String mkzip = QiniuUtils.mkzip(urlList, type, adminId + ":" + date.getTime());
        Image image = new Image();
        //解密过后的地址，数据库中保存的是不能直接访问的，用作记录文件名而已
        image.setUrl(mkzip);
        image.setUpdateTime(new Date());
        image.setCreateTime(new Date());
        image.setInvId(adminId);
        image.setName("zip");
        image.setState(0);
        image.setType(5);
        imageMapper.insertSelective(image);
        return mkzip;
    }
}
