package net.yunxinyong.sign.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.log4j.Log4j;
import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.*;
import net.yunxinyong.sign.mapper.*;
import net.yunxinyong.sign.newEntity.RequestRecent30;
import net.yunxinyong.sign.service.*;
import net.yunxinyong.sign.utils.ListUtils;
import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.utils.QiniuUtils;
import net.yunxinyong.sign.vo.EcnomicReqVo;
import net.yunxinyong.sign.vo.InventoryVo;
import net.yunxinyong.sign.vo.RegisterStatusVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 *@describe 查询企业数据
 *@author  ttbf
 *@date  2018/10/31
 */
@Service
@Log4j
public class EcnomicServiceImpl implements EcnomicService {
    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;
    @Autowired
    private RecordService recordService;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private ImageMapper imageMapper;
    @Autowired
    private CecBaseMapper cecBaseMapper;
    @Autowired
    private CecUserMapper cecUserMapper;
    @Autowired
    private SmsService smsService;
    @Autowired
    private RegisterStatusMapper registerStatusMapper;
    @Autowired
    private NewMapper newMapper;
    @Autowired
    private OverViewService overViewService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private RegisterStatusService registerStatusService;
    @Override
    public PageBean getList(EcnomicReqVo ecnomicReqVo, int page, int rows, Integer adminId) {

        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        EcnomicInventoryExample.Criteria criteria = ecnomicInventoryExample.createCriteria();
        //判断查询人角色
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin == null){
            throw new CECException(501,"管理员身份有误");
        }
        String area = null;
        if (signAdmin.getRole().equals("管理员")){
            area = signAdmin.getArea();
            criteria.andReferenceAddress02EqualTo(area);
        }else if (signAdmin.getRole().equals("普查员")){
            area = signAdmin.getArea();
            criteria.andAdminIdEqualTo(adminId);
        }else if (signAdmin.getRole().equals("root")){
            area = ecnomicReqVo.getReferenceAddress02();
        }
        //添加搜索条件
        //企业名称     这里的企业名字可以为""?
        if (ecnomicReqVo.getUnitDetailedName() != null && !"".equals(ecnomicReqVo.getUnitDetailedName())){
            criteria.andUnitDetailedNameLike("%"+ecnomicReqVo.getUnitDetailedName()+"%");
        }
        if (ecnomicReqVo.getReferenceAddress02() != null && !ecnomicReqVo.getReferenceAddress02().equals("")){
            criteria.andReferenceAddress02EqualTo(ecnomicReqVo.getReferenceAddress02());
        }
        //审核企业状态
        if (ecnomicReqVo.getUiteStatus() != null){
            criteria.andUiteStatusEqualTo(ecnomicReqVo.getUiteStatus());
        }
        //企业分配任务状态
         if (ecnomicReqVo.getIsAssign() != null){
            if (ecnomicReqVo.getIsAssign() == 1){
                if (ecnomicReqVo.getAdminId() != null){
                    criteria.andAdminIdEqualTo(ecnomicReqVo.getAdminId());
                }else {
                    criteria.andAdminIdGreaterThan(0);
                }
            }else if (ecnomicReqVo.getIsAssign() == 0){
                criteria.andAdminIdEqualTo(0);
            }
        }
        //注册地址
        if(ecnomicReqVo.getReferenceAddress() != null && !ecnomicReqVo.getReferenceAddress().equals("")){
                criteria.andReferenceAddressLike("%"+ecnomicReqVo.getReferenceAddress()+"%");
        }
        //省
        if (ecnomicReqVo.getUnitLocationProvience() != null && !ecnomicReqVo.getUnitLocationProvience().equals("")){
            criteria.andUnitLocationProvienceEqualTo(ecnomicReqVo.getUnitLocationProvience());
        }
        //市
        if (ecnomicReqVo.getUnitLocationCity() != null && !ecnomicReqVo.getUnitLocationCity().equals("")){
            criteria.andUnitLocationCityEqualTo(ecnomicReqVo.getUnitLocationCity());
        }
        //区
        if (ecnomicReqVo.getUnitLocationDistrict() != null && !ecnomicReqVo.getUnitLocationDistrict().equals("")){
            criteria.andUnitLocationDistrictEqualTo(ecnomicReqVo.getUnitLocationDistrict());
        }
        //社区community
        if (ecnomicReqVo.getCommunity() !=null && !ecnomicReqVo.getCommunity().equals("")){
            criteria.andCommunityEqualTo(ecnomicReqVo.getCommunity());
        }
        //普查员id
        if (ecnomicReqVo.getAdminId() != null){
            criteria.andAdminIdEqualTo(ecnomicReqVo.getAdminId());
        }
        //通知、提交、注册、上传   0 代表未完成 1代表已完成 null 代表查询全部，所以就直接不走查询条件了
        if (ecnomicReqVo.getInform() != null || ecnomicReqVo.getRegister() != null || ecnomicReqVo.getSubmit() != null || ecnomicReqVo.getUploaded() != null){
            List<Long> idListByStreet ;
            //如若是超级管理员 当前的街道名字是ReferenceAddress02中的。。
            if ("root".equals(signAdmin.getRole())){
                if (ecnomicReqVo.getReferenceAddress02() == null || ecnomicReqVo.getReferenceAddress02().equals("")){
                    throw new CECException(501,"请选择街道");
                }
                criteria.andReferenceAddress02EqualTo(ecnomicReqVo.getReferenceAddress02());
                //先去数据库查出底册企业的id列表
                idListByStreet = newMapper.selectIdListByStreet(ecnomicReqVo.getReferenceAddress02());
            }else {
                //如果不是超级管理员，街道是在signAdmin.getArea()中存在
                //先去数据库查出底册企业的id列表
                idListByStreet = newMapper.selectIdListByStreet(signAdmin.getArea());
            }
            //通知
            if (ecnomicReqVo.getInform() != null){
                //这是查出的id列表 ,已通知在数据库状态表对应的id是8 ，未通知在数据对应的id是 9
                idListByStreet = dealStatus(idListByStreet, ecnomicReqVo.getInform(), area, 8);
            }
            //注册23
            if (ecnomicReqVo.getRegister() != null){
                idListByStreet = dealStatus(idListByStreet, ecnomicReqVo.getRegister(), area, 23);
            }
            //上传
            if (ecnomicReqVo.getUploaded() != null){
                idListByStreet = dealStatus(idListByStreet,ecnomicReqVo.getUploaded(),area,17);
            }
            //提交
            if (ecnomicReqVo.getSubmit() != null){
                //已提交对应的数据库status表中15项 填表情况，已完成。15
                idListByStreet = dealStatus(idListByStreet,ecnomicReqVo.getSubmit(),area,15);
            }
            //汇总查询条件
            if (!ListUtils.isEmpty(idListByStreet)){
                criteria.andIdIn(idListByStreet);
            }else {
                //如果查询范围的集合是null或者size=0 就直接返回一个pageBean
                return new PageBean();
            }
        }
        if (page != 0 && rows != 0){
            PageHelper.startPage(page, rows);
        }
        List<EcnomicInventory> ecnomicInventoryList = ecnomicInventoryMapper.selectByExample(ecnomicInventoryExample);
        //把查出来的集合转化为返回值为vo类的集合
        ArrayList<EcnomicReqVo> ecnomicReqVoList = new ArrayList<>();
        // 这个实现类有 查询企业， 根据条件查询企业 ，多个controller调用，所以，对所有的判断条件进行 判断
        //随着企业数目的增加，这个给vo类赋值的速度会减慢
        for (EcnomicInventory ecnomicInventory : ecnomicInventoryList){
            EcnomicReqVo ecnomicReqVo1 = new EcnomicReqVo();
            //区状态表，查询idListByStreet 中包含的企业的状态。
            List<RegisterStatusVo> registerStatusList = registerStatusService.getRegisterStatusList(ecnomicInventory.getId().intValue());
            BeanUtils.copyProperties(ecnomicInventory,ecnomicReqVo1);
            for (RegisterStatusVo registerStatusVo : registerStatusList){
                Integer valueId = registerStatusVo.getValueId();
                switch (valueId){
                    //通知
                    case 8: ecnomicReqVo1.setInform(1);
                        break;
                    case 9: ecnomicReqVo1.setInform(0);
                        break;
                    //在这里联系不上也算是未通知
                    case 10: ecnomicReqVo1.setInform(0);
                        break;
                    //注册
                    case 22: ecnomicReqVo1.setRegister(0);
                        break;
                    case 23: ecnomicReqVo1.setRegister(1);
                        break;
                        //提交（ 在这里状态表中的未填写和填写中都是未提交）
                    case 15: ecnomicReqVo1.setSubmit(1);
                        break;
                    case 14: ecnomicReqVo1.setSubmit(0);
                        break;
                    case 13: ecnomicReqVo1.setSubmit(0);
                        break;
                        //上传
                    case 16: ecnomicReqVo1.setUploaded(0);
                        break;
                    case 17: ecnomicReqVo1.setUploaded(1);
                        break;
                        default:break;
                }
            }
            //是否已分配
            if (ecnomicInventory.getAdminId() == 0){
                ecnomicReqVo1.setIsAssign(0);
            }else {
                ecnomicReqVo1.setIsAssign(1);
                SignAdmin adminById = adminService.getAdminById(ecnomicInventory.getAdminId());
                if (adminById != null){
                    ecnomicReqVo1.setCunsusTakerName(adminById.getPhone());
                }
            }
            ecnomicReqVoList.add(ecnomicReqVo1);
        }
        PageBean<EcnomicReqVo> pageBean = new PageBean<>();
        PageInfo<EcnomicInventory> pageInfo = new PageInfo<>(ecnomicInventoryList);
        pageBean.setTotal(pageInfo.getTotal());
        pageBean.setTotalPages(pageInfo.getPages());
        pageBean.setPageNumber(pageInfo.getPageNum());
        pageBean.setPageSize(pageInfo.getSize());
        pageBean.setPageDatas(ecnomicReqVoList);
        return pageBean;
    }

    /**
     * 这个方法是上面查询企业，  处理 通知，提交，上传，注册  用到的。
     * @param idListByStreet 这个街道所有企业id的集合
     * @param ecnomicReqVoStatus 查询条件的状态值，0代表未操作 ，1 代表已操作，
     * @param area 所在的街道
     * @param valueId  状态值（数据库装填表中 完成状态的id）
     */
    public List dealStatus(List idListByStreet,Integer ecnomicReqVoStatus,String area,Integer valueId){
        //这是查出的id列表 ,已通知在数据库状态表对应的id是8 ，未通知在数据对应的id是 9
        List<Long> registerSatusList = overViewService.getRegisterSatus(valueId, area);
        if (!ListUtils.isEmpty(registerSatusList)){
            if (ecnomicReqVoStatus == 1){
                //求出底册企业和registerSatusList的交集
                idListByStreet.retainAll(registerSatusList);
            }else if (ecnomicReqVoStatus == 0){
                //未注册的企业为 该街道查出的所有企业，减去已注册的企业
                idListByStreet.removeAll(registerSatusList);
            }
        }else {
            if (ecnomicReqVoStatus == 1){
                //求出底册企业和registerSatusList的交集,因为registerSatusList是null或者size为0，所以这里新建一个集合赋值给idListByStreet
                ArrayList<Long> newList = new ArrayList<>();
                idListByStreet = newList;
            }
        }
        return idListByStreet;
    }

    @Override
    public EcnomicInventory examine(Integer invId, Integer adminId, Integer status) {
        EcnomicInventory ecnomicInventory = new EcnomicInventory();
        ecnomicInventory.setUiteStatus(status);
        ecnomicInventory.setId((long)invId);
        ecnomicInventory.setUpdatetime(new Date());
        int update = ecnomicInventoryMapper.updateByPrimaryKeySelective(ecnomicInventory);

        if (update <= 0){
            throw  new CECException(501,"审核操作失败");
        }
        EcnomicInventory ecnomicInventory1 = ecnomicInventoryMapper.selectByPrimaryKey((long)invId);
        //发送审核短信
        if (ecnomicInventory1.getUserId() != null){
            CecUser cecUser = cecUserMapper.selectByPrimaryKey(ecnomicInventory1.getUserId());
            if (status == 1){
                smsService.sendExamine(cecUser.getPhone(),1);
            }else if(status == -1){
                smsService.sendExamine(cecUser.getPhone(),-1);
            }
        }

        //添加操作记录
        if (status == 1){
            recordService.insert(adminId,invId,null,8);
        }else if(status == -1){
            recordService.insert(adminId,invId,null,9);
        }
        return ecnomicInventory;
    }

    @Override
    public EcnomicInventory get(Long invId) {
        EcnomicInventory inventory = ecnomicInventoryMapper.selectByPrimaryKey(invId);
        if (inventory == null){
            throw new CECException(501,"未查到企业信息");
        }
        return inventory;
    }

    @Override
    public Image getExamineImage(Integer invId, Integer adminId) {
        EcnomicInventory ecnomicInventory = ecnomicInventoryMapper.selectByPrimaryKey((long) invId);
        Integer inventoryAdminId = ecnomicInventory.getAdminId();
        String referenceAddress02 = ecnomicInventory.getReferenceAddress02();
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        String role = signAdmin.getRole();
        String area = signAdmin.getArea();
        if (role.equals("管理员")){
            if (!area.equals(referenceAddress02)){
                throw new CECException(501,"没有操作权限");
            }
        }else if (role.equals("普查员")){
            if (!inventoryAdminId.equals(adminId)){
                throw new CECException(501,"没有操作权限");
            }
        }
        ImageExample imageExample = new ImageExample();
        imageExample.createCriteria().andTypeEqualTo(1).andInvIdEqualTo(invId);
        List<Image> imageList = imageMapper.selectByExample(imageExample);
        if (ListUtils.isEmpty(imageList)){
            throw new CECException(501,"未查到图片信息");
        }
        Image image = imageList.get(0);
        image.setUrl(QiniuUtils.getUrl(image.getUrl()));
        return image;
    }

    /*@Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = CECException.class)
    public void updateType(Integer invId) {
        CecBaseExample cecBaseExample = new CecBaseExample();
        cecBaseExample.createCriteria().andStatusEqualTo(0).andInvIdEqualTo(invId);
        List<CecBase> cecBaseList = cecBaseMapper.selectByExample(cecBaseExample);
        //查到base表则更新
        if (!ListUtils.isEmpty(cecBaseList)) {
            CecBase cecBase = cecBaseList.get(0);
            boolean one = false;
            boolean two = false;
            boolean three = false;
            boolean four = false;
            boolean five = false;
            boolean six = false;
            boolean seven = false;
            //110 == 1
            if (cecBase.getUnitType() != null && cecBase.getUnitType() == 1) {
                two = true;
                four = true;
                seven = true;
                //214==1
                if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 1) {
                    one = true;
                }
                //209==1
                if (cecBase.getAccountingStandardCategory() != null && cecBase.getAccountingStandardCategory() == 1) {
                    three = true;
                }
                //211=55,56,90
                if (cecBase.getMechanismType() != null &&
                        (cecBase.getMechanismType() == 55 || cecBase.getMechanismType() == 56 || cecBase.getMechanismType() == 90)) {
                    three = true;
                }
                //211=30,53,54
                if (cecBase.getMechanismType() != null &&
                        (cecBase.getMechanismType() == 30 || cecBase.getMechanismType() == 53 || cecBase.getMechanismType() == 54)) {
                    five = true;
                }
                //209 ==2 and 211 ==20
                if (cecBase.getAccountingStandardCategory() != null && cecBase.getAccountingStandardCategory() == 2) {
                    five = true;
                }
                //209!=1 and 211==51,52,90
                if (cecBase.getAccountingStandardCategory() != null && cecBase.getAccountingStandardCategory() != 1 &&
                        cecBase.getMechanismType() != null && (cecBase.getMechanismType() == 51 || cecBase.getMechanismType() == 52
                        || cecBase.getMechanismType() == 90)) {
                    six = true;
                }
            }
            StringBuilder type = new StringBuilder();
            if (one) {
                type.append("1,");
            }
            if (two) {
                type.append("2,");
            }
            if (three) {
                type.append("3,");
            }
            if (four) {
                type.append("4,");
            }
            if (five) {
                type.append("5,");
            }
            if (six) {
                type.append("6,");
            }
            if (seven) {
                type.append("7,");
            }
            EcnomicInventory ecnomicInventory = new EcnomicInventory();
            ecnomicInventory.setId((long) invId);
            ecnomicInventory.setUpdatetime(new Date());
            ecnomicInventory.setType(String.valueOf(type));
            int update = ecnomicInventoryMapper.updateByPrimaryKeySelective(ecnomicInventory);
            if (update <= 0) {
                log.error("企业类型更新失败");
            }
        }
    }*/

    @Override
    public void updateType(Integer invId) {
        CecBaseExample cecBaseExample = new CecBaseExample();
        cecBaseExample.createCriteria().andStatusEqualTo(0).andInvIdEqualTo(invId);
        List<CecBase> cecBaseList = cecBaseMapper.selectByExample(cecBaseExample);
        //查到base表则更新
        if (!ListUtils.isEmpty(cecBaseList)) {
            CecBase cecBase = cecBaseList.get(0);
            boolean one = false;
            boolean two = false;
            boolean three = false;
            boolean four = false;
            boolean five = false;
            boolean six = false;
            boolean seven = false;
            //110项选择1
            if (cecBase.getUnitType() != null && cecBase.getUnitType() == 1) {
                //因为选择1 之后的每套表都包含2 ，4 所以直接在这里设置成ture;
                two = true;
                four = true;
                //209选择1
                if (cecBase.getAccountingStandardCategory() != null && cecBase.getAccountingStandardCategory() == 1) {
                    //214项判断是否有下属产业活动单位  1为是2位否
                    if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 1) {
                        three = true;
                        one = true;
                    } else if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 2) {
                        three = true;
                    }
                }
                //209项选择2 或者3
                if ((cecBase.getAccountingStandardCategory() != null && cecBase.getAccountingStandardCategory() == 2) || (cecBase.getAccountingStandardCategory() != null && cecBase.getAccountingStandardCategory() == 3)) {
                    //214项判断是否有下属产业活动单位  1为是2位否 是选择第套表（611  2 4 5 1） 否选择第4套表（ 611 2 4 5 ）
                    if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 1) {
                        one = true;
                        five = true;
                    } else if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 2) {
                        five = true;
                    }
                }
                //209项选择4
                if (cecBase.getAccountingStandardCategory() != null && cecBase.getAccountingStandardCategory() == 4) {
                    //214项判断是否有下属产业活动单位  1为是2位否 是选择第5套表（611  2 4 5 1） 否选择第4套表（ 611 2 4 5 ）
                    if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 1) {
                        one = true;
                        five = true;
                    } else if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 2) {
                        five = true;
                    }
                }
                //209项选择9
                if (cecBase.getAccountingStandardCategory() != null && cecBase.getAccountingStandardCategory() == 4) {
                    //211项选择 10 / 55 /56 /90
                    if (cecBase.getMechanismType() != null && (cecBase.getMechanismType() == 10 || cecBase.getMechanismType() == 55 || cecBase.getMechanismType() == 56 || cecBase.getMechanismType() == 90)) {
                        //214项判断是否有下属产业活动单位  1为是2位否 是选择第3套表（611 1 2 3 4 ） 否选择第2套表（ 611 2 3 4 ）
                        if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 1) {
                            one = true;
                            three = true;
                        } else if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 2) {
                            three = true;
                        }
                    }
                    //211项选择 40 / 51 /52
                    if (cecBase.getMechanismType() != null && (cecBase.getMechanismType() == 40 || cecBase.getMechanismType() == 51 || cecBase.getMechanismType() == 52)) {
                        //214项判断是否有下属产业活动单位  1为是2位否 是选择第7套表（611 1 2  4 6 ） 否选择第6套表（ 611 2 4 6）
                        if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 1) {
                            one = true;
                            six = true;
                        } else if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 2) {
                            six = true;
                        }
                    }
                    //211项选择 20 / 30 /53/54
                    if (cecBase.getMechanismType() != null && (cecBase.getMechanismType() == 20 || cecBase.getMechanismType() == 30 || cecBase.getMechanismType() == 53 || cecBase.getMechanismType() == 54)) {
                        //214项判断是否有下属产业活动单位  1为是2位否 是选择第5套表（611 1 2  4 5 ） 否选择第4套表（ 611 2 4 5）
                        if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 1) {
                            one = true;
                            five = true;
                        } else if (cecBase.getIsHaveIndustrial() != null && cecBase.getIsHaveIndustrial() == 2) {
                            five = true;
                        }
                    }
                }

                //110项选择2
            } else if (cecBase.getUnitType() != null && cecBase.getUnitType() == 2) {
                //只有611表所以 可以不同选 （因为 611表的type字段默认是 不填 或者 null）
            }
            StringBuilder type = new StringBuilder();
            if (one) {
                type.append("1,");
            }
            if (two) {
                type.append("2,");
            }
            if (three) {
                type.append("3,");
            }
            if (four) {
                type.append("4,");
            }
            if (five) {
                type.append("5,");
            }
            if (six) {
                type.append("6,");
            }
            EcnomicInventory ecnomicInventory = new EcnomicInventory();
            ecnomicInventory.setId((long) invId);
            ecnomicInventory.setUpdatetime(new Date());
            ecnomicInventory.setType(String.valueOf(type));
            int update = ecnomicInventoryMapper.updateByPrimaryKeySelective(ecnomicInventory);
            if (update <= 0) {
                log.error("企业类型更新失败");
            }
        }
    }

    @Override
    public Map getCount(String street) {
        //企业的总数
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andStatusEqualTo(1).andReferenceAddress02EqualTo(street);
        int totalCount = ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);
       //已认证企业的数目
        EcnomicInventoryExample ecnomicInventoryExample1 = new EcnomicInventoryExample();
        ecnomicInventoryExample1.createCriteria().andUserIdGreaterThan(0).andStatusEqualTo(1).andReferenceAddress02EqualTo(street);
        int signCount = ecnomicInventoryMapper.countByExample(ecnomicInventoryExample1);
        //把查询到的数据放在 map中
        Map<String, Integer> map = new HashMap<>();
        map.put("totalCount",totalCount);
        map.put("signCount",signCount);
        return map;
    }

    @Override
    public void insert(EcnomicInventory ecnomicInventory,Integer adminId) {
        ecnomicInventory.setCreatetime(new Date());
        ecnomicInventory.setUpdatetime(new Date());
        //查询企业是否已存在
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andUnitDetailedNameEqualTo(ecnomicInventory.getUnitDetailedName());
        List<EcnomicInventory> ecnomicInventories = ecnomicInventoryMapper.selectByExample(ecnomicInventoryExample);
        if (!ListUtils.isEmpty(ecnomicInventories)){
            throw new CECException(501,"企业已存在");
        }
        int insertSelective = ecnomicInventoryMapper.insertSelective(ecnomicInventory);
        //添加操作记录
        recordService.insert(adminId,ecnomicInventory.getId().intValue(),null,13);
        if (insertSelective <= 0){
            throw new CECException(501,"添加企业失败");
        }
    }
}
