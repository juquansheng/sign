package net.yunxinyong.sign.service.impl;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.Status;
import net.yunxinyong.sign.entity.StatusExample;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.mapper.StatusMapper;
import net.yunxinyong.sign.service.RecordService;
import net.yunxinyong.sign.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 *@describe 状态值管理
 *@author  ttbf
 *@date  2018/10/30
 */
@Service
public class StatusServiceImpl implements StatusService {

    @Autowired
    private StatusMapper statusMapper;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private RecordService recordService;


    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = CECException.class)
    public boolean insert(Status status,Integer id) {
        //判断权限，普查员不可修改
        if (signAdminMapper.selectByPrimaryKey(id).getRole().equals("普查员") || (status.getParentId() != null && status.getParentId() == 0)){
            throw new CECException(505,"没有操作权限");
        }

        status.setAdminId(id);
        status.setCreateTime(new Date());
        status.setUpdateTime(new Date());
        status.setType(1);
        status.setStatusName(statusMapper.selectByPrimaryKey(status.getParentId()).getStatusValue());
        boolean b = statusMapper.insertSelective(status) > 0;
        //添加操作记录
        boolean insert = recordService.insert(id, null, status.getId(), 6);
        if (!b || !insert){
            throw new CECException(505,"操作失败");
        }
        return true;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = CECException.class)
    public boolean update(Status status,Integer id) {
        //判断权限，普查员不可修改
        if (signAdminMapper.selectByPrimaryKey(id).getRole().equals("普查员")|| (status.getParentId() != null && status.getParentId() == 0)){
            throw new CECException(505,"没有操作权限");
        }
        status.setUpdateTime(new Date());
        status.setParentId(null);
        status.setType(null);
        status.setStatusName(null);
        boolean b = statusMapper.updateByPrimaryKeySelective(status) > 0;
        //添加操作记录
        boolean insert = recordService.insert(id, null, status.getId(), 7);
        if (!b || !insert){
            throw new CECException(505,"操作失败");
        }
        return true;
    }

    @Override
    public List<Status> getList(Integer parentId, Integer id) {
        //判断权限，普查员不可修改
        if (signAdminMapper.selectByPrimaryKey(id).getRole().equals("普查员")){
            throw new CECException(505,"没有操作权限");
        }
        StatusExample statusExample = new StatusExample();
        statusExample.createCriteria().andParentIdEqualTo(parentId);
        return statusMapper.selectByExample(statusExample);
    }
}
