package net.yunxinyong.sign.service.impl;


import net.yunxinyong.sign.entity.Street;
import net.yunxinyong.sign.entity.StreetCommunity;
import net.yunxinyong.sign.entity.StreetCommunityExample;
import net.yunxinyong.sign.entity.StreetExample;
import net.yunxinyong.sign.mapper.StreetCommunityMapper;
import net.yunxinyong.sign.mapper.StreetMapper;
import net.yunxinyong.sign.service.StreetService;
import net.yunxinyong.sign.utils.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StreetServiceImpl implements StreetService {
    @Autowired
    private StreetMapper streetMapper;
    @Autowired
    private StreetCommunityMapper streetCommunityMapper;
    @Override
    public List<Street> getAllStreet() {
        StreetExample streetExample = new StreetExample();
        List<Street> streetList = streetMapper.selectByExample(streetExample);
        return streetList;
    }

    @Override
    public List<StreetCommunity> getCoummunity(String street) {
        //超级管理员的street 是unkown ,直接返回所有社区。
        if (street != null && !street.equals("") && street.equals("unknow")){
            StreetCommunityExample streetCommunityExample = new StreetCommunityExample();
            streetCommunityExample.createCriteria().andIsLastLevelEqualTo("1");
            return streetCommunityMapper.selectByExample(streetCommunityExample);
        }
        //下面是普通的管理员，查到的社区只是该街道下的社区
        StreetCommunityExample streetCommunityExample = new StreetCommunityExample();
        streetCommunityExample.createCriteria().andAreaNameEqualTo(street);
        List<StreetCommunity> streetCommunities = streetCommunityMapper.selectByExample(streetCommunityExample);
        if (!ListUtils.isEmpty(streetCommunities)){
            StreetCommunity streetCommunity = streetCommunities.get(0);
            //取出街道的area_code 作为社区的parent_code
            String parent_code = streetCommunity.getAreaCode();
            StreetCommunityExample streetCommunityExample1 = new StreetCommunityExample();
            streetCommunityExample1.createCriteria().andParentCodeEqualTo(parent_code);
            return streetCommunityMapper.selectByExample(streetCommunityExample1);
        }
        return null;
    }
}
