package net.yunxinyong.sign.service.impl;


import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecThreePlatform;
import net.yunxinyong.sign.entity.CecThreePlatformExample;
import net.yunxinyong.sign.mapper.CecThreePlatformMapper;
import net.yunxinyong.sign.service.CecThreePlatformService;
import net.yunxinyong.sign.utils.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CecThreePlatformServiceImpl implements CecThreePlatformService {
    @Autowired
    private CecThreePlatformMapper cecThreePlatformMapper;

    @Override
    public void insertOrUpdate(List<CecThreePlatform> cecThreePlatformList) {

    }

    @Override
    public List<CecThreePlatform> get(Integer threeId) {
        CecThreePlatformExample cecThreePlatformExample = new CecThreePlatformExample();
        cecThreePlatformExample.createCriteria().andThreeIdEqualTo(threeId).andStatusEqualTo(0);
        List<CecThreePlatform> cecThreePlatforms = cecThreePlatformMapper.selectByExample(cecThreePlatformExample);
        if(!ListUtils.isEmpty(cecThreePlatforms)){
            return cecThreePlatforms;
        }
        return null;
    }

    @Override
    public void insert(CecThreePlatform cecThreePlatform,Integer threeId) {
        Date date = new Date();
        cecThreePlatform.setCreateTime(date);
        cecThreePlatform.setUpdateTime(date);
        cecThreePlatform.setThreeId(threeId);
        cecThreePlatform.setStatus(0);
        int i = cecThreePlatformMapper.insert(cecThreePlatform);
        if(i <= 0){
            throw new CECException(501,"分支表添加失败");
        }
    }

    @Override
    public void update(CecThreePlatform cecThreePlatform) {
        Date date = new Date();
        cecThreePlatform.setUpdateTime(date);
        int i = cecThreePlatformMapper.updateByPrimaryKey(cecThreePlatform);
        if(i <= 0){
            throw new CECException(501,"分支表更新失败");
        }
    }

    @Override
    public void delete(Integer id) {
        CecThreePlatformExample cecThreePlatformExample = new CecThreePlatformExample();
        cecThreePlatformExample.createCriteria().andIdEqualTo(id);
        CecThreePlatform cecThreePlatform = new CecThreePlatform();
        cecThreePlatform.setStatus(-1);
        int i = cecThreePlatformMapper.updateByExampleSelective(cecThreePlatform, cecThreePlatformExample);
        if(i <= 0){
            throw new CECException(501,"分分支表更新失败");
        }
    }
}
