package net.yunxinyong.sign.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import net.yunxinyong.sign.entity.*;
import net.yunxinyong.sign.mapper.CecBaseMapper;
import net.yunxinyong.sign.mapper.EcnomicInventoryMapper;
import net.yunxinyong.sign.mapper.NewMapper;
import net.yunxinyong.sign.mapper.RegisterStatusMapper;
import net.yunxinyong.sign.newEntity.RequestRecent30;
import net.yunxinyong.sign.newEntity.ResultRecent30;
import net.yunxinyong.sign.service.StatisticsService;
import net.yunxinyong.sign.utils.DateUtils;
import net.yunxinyong.sign.utils.ListUtils;
import net.yunxinyong.sign.utils.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    private RegisterStatusMapper registerStatusMapper;
    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;
    @Autowired
    private NewMapper newMapper;
    @Autowired
    private CecBaseMapper cecBaseMapper;

    @Override
    public Integer getAmountByAdd(String area) {
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andStatusEqualTo(1).andUiteStatusEqualTo(1);
        return ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);

    }

    /**
     * 查询状态总数
     * @param valueId
     * @param area
     * @return
     */
    @Override
    public Integer getAmountByStatus(Integer valueId, String area) {

        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        EcnomicInventoryExample.Criteria criteria = ecnomicInventoryExample.createCriteria();
        if ( area != null && !area.equals("unknow") && !area.equals("")){
            criteria.andReferenceAddress02EqualTo(area);
        }

        //状态查询企业id列表
        List<Integer> idList = Lists.newArrayList();

        RegisterStatusExample registerStatusExample = new RegisterStatusExample();
        registerStatusExample.createCriteria().andValueIdEqualTo(valueId);
        List<RegisterStatus> registerStatusList = registerStatusMapper.selectByExample(registerStatusExample);
        if (!ListUtils.isEmpty(registerStatusList)){
            for (RegisterStatus registerStatus:registerStatusList){
                idList.add(registerStatus.getInvId());
            }
        }
        //list转换
        List<Long> idListToLong = Lists.newArrayList();
        idListToLong.add(-1L);
        for (Integer integer:idList){
            idListToLong.add((long)integer);
        }
        criteria.andIdIn(idListToLong);
        return ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);
    }

    @Override
    public List<ResultRecent30> getList(RequestRecent30 requestRecent30) {
        /*Date dateEnd =new Date();
        //获取三十天前日期
        Calendar theCa = Calendar.getInstance();
        theCa.setTime(dateEnd);
        theCa.add(theCa.DATE, -30);//最后一个数字30可改，30天的意思
        Date dateStart = theCa.getTime();*/
        requestRecent30.setStartTime(requestRecent30.getStartTime());
        requestRecent30.setEndTime(requestRecent30.getEndTime());
        List<ResultRecent30> resultRecent30s = newMapper.selectRecent30(requestRecent30);

        return createList(resultRecent30s,DateUtils.differentDays(requestRecent30.getStartTime(),
                requestRecent30.getEndTime())+1,requestRecent30.getEndTime());
    }

    @Override
    public List<ResultRecent30> getAddList(RequestRecent30 requestRecent30) {
        /*Date dateEnd =new Date();
        //获取三十天前日期
        Calendar theCa = Calendar.getInstance();
        theCa.setTime(dateEnd);
        theCa.add(theCa.DATE, -30);//最后一个数字30可改，30天的意思
        Date dateStart = theCa.getTime();*/

        requestRecent30.setStartTime(requestRecent30.getStartTime());
        requestRecent30.setEndTime(requestRecent30.getEndTime());
        List<ResultRecent30> resultRecent30s = newMapper.selectAdd30(requestRecent30);
        return createList(resultRecent30s,DateUtils.differentDays(requestRecent30.getStartTime(),
                requestRecent30.getEndTime())+1,requestRecent30.getEndTime());
    }

    @Override
    public Integer getNotRegister(RequestRecent30 requestRecent30) {
        return  newMapper.selectNotRegister(requestRecent30);
    }

    @Override
    public Integer getNotCheck(RequestRecent30 requestRecent30) {
        return getAmountByStatus(23,requestRecent30.getArea()) - newMapper.selectNotCheck(requestRecent30);
    }

    @Override
    public Integer getNotUpload(String area) {
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        EcnomicInventoryExample.Criteria criteria = ecnomicInventoryExample.createCriteria();
        if ( area != null && !area.equals("unknow") && !area.equals("")){
            criteria.andReferenceAddress02EqualTo(area);
        }

        //已提交状态查询企业id列表
        List<Integer> idList = Lists.newArrayList();
        RegisterStatusExample registerStatusExample = new RegisterStatusExample();
        registerStatusExample.createCriteria().andValueIdEqualTo(15);
        List<RegisterStatus> registerStatusList = registerStatusMapper.selectByExample(registerStatusExample);
        if (!ListUtils.isEmpty(registerStatusList)){
            for (RegisterStatus registerStatus:registerStatusList){
                idList.add(registerStatus.getInvId());
            }
        }

        //已上传状态查询企业id列表
        List<Integer> idUploadList = Lists.newArrayList();
        RegisterStatusExample registerStatusExampleUpload = new RegisterStatusExample();
        registerStatusExampleUpload.createCriteria().andValueIdEqualTo(17);
        List<RegisterStatus> registerStatusListUpload = registerStatusMapper.selectByExample(registerStatusExampleUpload);
        if (!ListUtils.isEmpty(registerStatusListUpload)){
            for (RegisterStatus registerStatus:registerStatusListUpload){
                idUploadList.add(registerStatus.getInvId());
            }
        }
        //list转换
        //已提交状态查询企业id列表
        List<Long> idListToLong = Lists.newArrayList();
        idListToLong.add(-1L);
        for (Integer integer:idList){
            idListToLong.add((long)integer);
        }

        //list转换
        //已上传状态查询企业id列表
        List<Long> idUploadListToLong = Lists.newArrayList();
        idUploadListToLong.add(-1L);
        for (Integer integer:idUploadList){
            idUploadListToLong.add((long)integer);
        }
        criteria.andIdIn(idListToLong);
        //已提交企业数量
        int countSubmission = ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);

        //求交集
        idListToLong.retainAll(idUploadListToLong);

        //已提交且已上传企业数量
        criteria.andIdIn(idListToLong);
        int countSubmissionUpload = ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);
        return countSubmission-countSubmissionUpload;
    }

    /**
     * 已上传未录入
     * @param area
     * @return
     */
    @Override
    public Integer getNotSign(String area) {
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        EcnomicInventoryExample.Criteria criteria = ecnomicInventoryExample.createCriteria();
        if ( area != null && !area.equals("unknow") && !area.equals("")){
            criteria.andReferenceAddress02EqualTo(area);
        }

        //已上传状态查询企业id列表
        List<Integer> idList = Lists.newArrayList();
        RegisterStatusExample registerStatusExample = new RegisterStatusExample();
        registerStatusExample.createCriteria().andValueIdEqualTo(19);
        List<RegisterStatus> registerStatusList = registerStatusMapper.selectByExample(registerStatusExample);
        if (!ListUtils.isEmpty(registerStatusList)){
            for (RegisterStatus registerStatus:registerStatusList){
                idList.add(registerStatus.getInvId());
            }
        }

        //已录入状态查询企业id列表
        List<Integer> idSigninList = Lists.newArrayList();
        RegisterStatusExample registerStatusExampleSignin = new RegisterStatusExample();
        registerStatusExampleSignin.createCriteria().andValueIdEqualTo(17);
        List<RegisterStatus> registerStatusListSignin = registerStatusMapper.selectByExample(registerStatusExampleSignin);
        if (!ListUtils.isEmpty(registerStatusListSignin)){
            for (RegisterStatus registerStatus:registerStatusListSignin){
                idSigninList.add(registerStatus.getInvId());
            }
        }
        //list转换
        //已上传状态查询企业id列表
        List<Long> idListToLong = Lists.newArrayList();
        idListToLong.add(-1L);
        for (Integer integer:idList){
            idListToLong.add((long)integer);
        }

        //list转换
        //已录入状态查询企业id列表
        List<Long> idSigninListToLong = Lists.newArrayList();
        idSigninListToLong.add(-1L);
        for (Integer integer:idSigninList){
            idSigninListToLong.add((long)integer);
        }
        criteria.andIdIn(idListToLong);
        //已上传企业数量
        int countSubmission = ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);

        //求交集
        idListToLong.retainAll(idSigninListToLong);

        //已上传且录入企业数量
        criteria.andIdIn(idListToLong);
        int countUploadsign = ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);
        return countSubmission-countUploadsign;
    }

    @Override
    public PageBean notContact(String area, int page, int rows,Integer isAssign) {
        //查询区域内企业
        List<Long> listRegister = Lists.newArrayList();
        listRegister.add(-1L);
        if (isAssign == -1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreet(area);
        }else if (isAssign == 0){
            //查询区域内未分配企业
            listRegister = newMapper.selectIdListByStreetAndNotAssign(area);
        }else if (isAssign == 1){
            //查询区域内已分配企业
            listRegister = newMapper.selectIdListByStreetAndAssign(area);
        }

        //查询区域内已注册企业
        RegisterStatusExample registerStatusExample23 = new RegisterStatusExample();
        registerStatusExample23.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList23 = registerStatusMapper.selectByExample(registerStatusExample23);
        List<Integer> list23 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList23){
            list23.add(registerStatus.getInvId());
        }
        //查询区域内填写中企业
        RegisterStatusExample registerStatusExample14 = new RegisterStatusExample();
        registerStatusExample14.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList14 = registerStatusMapper.selectByExample(registerStatusExample14);
        List<Integer> list14 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList14){
            list14.add(registerStatus.getInvId());
        }
        //查询区域内已提交企业
        RegisterStatusExample registerStatusExample15 = new RegisterStatusExample();
        registerStatusExample15.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList15 = registerStatusMapper.selectByExample(registerStatusExample15);
        List<Integer> list15 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList15){
            list15.add(registerStatus.getInvId());
        }
        listRegister.removeAll(list14);
        listRegister.removeAll(list15);
        listRegister.removeAll(list23);
        listRegister.add(-1L);
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andStatusEqualTo(1).andIdIn(listRegister);
        if (page != 0 && rows != 0){
            PageHelper.startPage(page, rows);
        }
        List<EcnomicInventory> ecnomicInventoryList = ecnomicInventoryMapper.selectByExample(ecnomicInventoryExample);
        PageBean<EcnomicInventory> pageBean = new PageBean<>();
        PageInfo<EcnomicInventory> pageInfo = new PageInfo<>(ecnomicInventoryList);
        pageBean.setTotal(pageInfo.getTotal());
        pageBean.setTotalPages(pageInfo.getPages());
        pageBean.setPageNumber(pageInfo.getPageNum());
        pageBean.setPageSize(pageInfo.getSize());
        pageBean.setPageDatas(ecnomicInventoryList);
        return pageBean;
    }

    @Override
    public PageBean notWrite(String area, int page, int rows,Integer isAssign) {
        //查询区域内企业
        List<Long> listRegister = Lists.newArrayList();
        listRegister.add(-1L);
        if (isAssign == -1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreet(area);
        }else if (isAssign == 0){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndNotAssign(area);
        }else if (isAssign == 1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndAssign(area);
        }
        //查询区域内已注册企业
        RegisterStatusExample registerStatusExample23 = new RegisterStatusExample();
        registerStatusExample23.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList23 = registerStatusMapper.selectByExample(registerStatusExample23);
        List<Integer> list23 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList23){
            list23.add(registerStatus.getInvId());
        }
        //查询区域内填写中企业
        RegisterStatusExample registerStatusExample14 = new RegisterStatusExample();
        registerStatusExample14.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList14 = registerStatusMapper.selectByExample(registerStatusExample14);
        List<Integer> list14 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList14){
            list14.add(registerStatus.getInvId());
        }
        //查询区域内已提交企业
        RegisterStatusExample registerStatusExample15 = new RegisterStatusExample();
        registerStatusExample15.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList15 = registerStatusMapper.selectByExample(registerStatusExample15);
        List<Integer> list15 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList15){
            list15.add(registerStatus.getInvId());
        }
        //所有区域内已注册企业
        listRegister.retainAll(list23);
        //已注册企业中去除填写中和已填写的
        listRegister.removeAll(list14);
        listRegister.removeAll(list15);
        listRegister.add(-1L);

        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andStatusEqualTo(1).andIdIn(listRegister);
        if (page != 0 && rows != 0){
            PageHelper.startPage(page, rows);
        }
        List<EcnomicInventory> ecnomicInventoryList = ecnomicInventoryMapper.selectByExample(ecnomicInventoryExample);
        PageBean<EcnomicInventory> pageBean = new PageBean<>();
        PageInfo<EcnomicInventory> pageInfo = new PageInfo<>(ecnomicInventoryList);
        pageBean.setTotal(pageInfo.getTotal());
        pageBean.setTotalPages(pageInfo.getPages());
        pageBean.setPageNumber(pageInfo.getPageNum());
        pageBean.setPageSize(pageInfo.getSize());
        pageBean.setPageDatas(ecnomicInventoryList);
        return pageBean;
    }

    @Override
    public PageBean notDownload(String area, int page, int rows,Integer isAssign) {
        //查询区域内企业
        List<Long> listRegister = Lists.newArrayList();
        listRegister.add(-1L);
        if (isAssign == -1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreet(area);
        }else if (isAssign == 0){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndNotAssign(area);
        }else if (isAssign == 1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndAssign(area);
        }
        //查询区域内填写中企业
        RegisterStatusExample registerStatusExample14 = new RegisterStatusExample();
        registerStatusExample14.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList14 = registerStatusMapper.selectByExample(registerStatusExample14);
        List<Integer> list14 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList14){
            list14.add(registerStatus.getInvId());
        }
        //查询区域内已提交企业
        RegisterStatusExample registerStatusExample15 = new RegisterStatusExample();
        registerStatusExample15.createCriteria().andValueIdEqualTo(15);
        List<RegisterStatus> registerStatusList15 = registerStatusMapper.selectByExample(registerStatusExample15);
        List<Integer> list15 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList15){
            list15.add(registerStatus.getInvId());
        }

        //查询区域内已下载企业
        CecBaseExample cecBaseExample = new CecBaseExample();
        cecBaseExample.createCriteria().andStateEqualTo(-1).andStatusEqualTo(0);
        List<CecBase> cecBaseList = cecBaseMapper.selectByExample(cecBaseExample);
        List<Integer> listNotDownload = Lists.newArrayList();
        for (CecBase cecBase:cecBaseList){
            listNotDownload.add(cecBase.getInvId());
        }

        //填写中和已填写并集
        list14.addAll(list15);
        //区域内已填写企业
        listRegister.retainAll(list14);
        //区域内已填写未下载企业
        listRegister.removeAll(listNotDownload);

        listRegister.add(-1L);
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andStatusEqualTo(1).andIdIn(listRegister);
        if (page != 0 && rows != 0){
            PageHelper.startPage(page, rows);
        }
        List<EcnomicInventory> ecnomicInventoryList = ecnomicInventoryMapper.selectByExample(ecnomicInventoryExample);
        PageBean<EcnomicInventory> pageBean = new PageBean<>();
        PageInfo<EcnomicInventory> pageInfo = new PageInfo<>(ecnomicInventoryList);
        pageBean.setTotal(pageInfo.getTotal());
        pageBean.setTotalPages(pageInfo.getPages());
        pageBean.setPageNumber(pageInfo.getPageNum());
        pageBean.setPageSize(pageInfo.getSize());
        pageBean.setPageDatas(ecnomicInventoryList);
        return pageBean;
    }

    @Override
    public PageBean notUpload(String area, int page, int rows,Integer isAssign) {
        //查询区域内企业
        List<Long> listRegister = Lists.newArrayList();
        listRegister.add(-1L);
        if (isAssign == -1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreet(area);
        }else if (isAssign == 0){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndNotAssign(area);
        }else if (isAssign == 1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndAssign(area);
        }
        //查询区域内已提交企业
        RegisterStatusExample registerStatusExample15 = new RegisterStatusExample();
        registerStatusExample15.createCriteria().andValueIdEqualTo(15);
        List<RegisterStatus> registerStatusList15 = registerStatusMapper.selectByExample(registerStatusExample15);
        List<Integer> list15 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList15){
            list15.add(registerStatus.getInvId());
        }
        //查询区域内已上传文件企业
        RegisterStatusExample registerStatusExample17 = new RegisterStatusExample();
        registerStatusExample17.createCriteria().andValueIdEqualTo(17);
        List<RegisterStatus> registerStatusList17 = registerStatusMapper.selectByExample(registerStatusExample17);
        List<Integer> list17 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList17){
            list17.add(registerStatus.getInvId());
        }

        //已完成企业
        listRegister.retainAll(list15);
        //已完成未上传
        listRegister.removeAll(list17);
        listRegister.add(-1L);
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andStatusEqualTo(1).andIdIn(listRegister);
        if (page != 0 && rows != 0){
            PageHelper.startPage(page, rows);
        }
        List<EcnomicInventory> ecnomicInventoryList = ecnomicInventoryMapper.selectByExample(ecnomicInventoryExample);
        PageBean<EcnomicInventory> pageBean = new PageBean<>();
        PageInfo<EcnomicInventory> pageInfo = new PageInfo<>(ecnomicInventoryList);
        pageBean.setTotal(pageInfo.getTotal());
        pageBean.setTotalPages(pageInfo.getPages());
        pageBean.setPageNumber(pageInfo.getPageNum());
        pageBean.setPageSize(pageInfo.getSize());
        pageBean.setPageDatas(ecnomicInventoryList);
        return pageBean;
    }

    @Override
    public List<Long> notContactList(String area, Integer isAssign) {
        //查询区域内企业
        List<Long> listRegister = Lists.newArrayList();
        listRegister.add(-1L);
        if (isAssign == -1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreet(area);
        }else if (isAssign == 0){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndNotAssign(area);
        }else if (isAssign == 1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndAssign(area);
        }

        //查询区域内已注册企业
        RegisterStatusExample registerStatusExample23 = new RegisterStatusExample();
        registerStatusExample23.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList23 = registerStatusMapper.selectByExample(registerStatusExample23);
        List<Integer> list23 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList23){
            list23.add(registerStatus.getInvId());
        }
        //查询区域内填写中企业
        RegisterStatusExample registerStatusExample14 = new RegisterStatusExample();
        registerStatusExample14.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList14 = registerStatusMapper.selectByExample(registerStatusExample14);
        List<Integer> list14 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList14){
            list14.add(registerStatus.getInvId());
        }
        //查询区域内已提交企业
        RegisterStatusExample registerStatusExample15 = new RegisterStatusExample();
        registerStatusExample15.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList15 = registerStatusMapper.selectByExample(registerStatusExample15);
        List<Integer> list15 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList15){
            list15.add(registerStatus.getInvId());
        }
        listRegister.removeAll(list14);
        listRegister.removeAll(list15);
        listRegister.removeAll(list23);
        return listRegister;
    }

    @Override
    public List<Long> notWriteList(String area, Integer isAssign) {
        //查询区域内企业
        List<Long> listRegister = Lists.newArrayList();
        listRegister.add(-1L);
        if (isAssign == -1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreet(area);
        }else if (isAssign == 0){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndNotAssign(area);
        }else if (isAssign == 1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndAssign(area);
        }
        //查询区域内已注册企业
        RegisterStatusExample registerStatusExample23 = new RegisterStatusExample();
        registerStatusExample23.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList23 = registerStatusMapper.selectByExample(registerStatusExample23);
        List<Integer> list23 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList23){
            list23.add(registerStatus.getInvId());
        }
        //查询区域内填写中企业
        RegisterStatusExample registerStatusExample14 = new RegisterStatusExample();
        registerStatusExample14.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList14 = registerStatusMapper.selectByExample(registerStatusExample14);
        List<Integer> list14 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList14){
            list14.add(registerStatus.getInvId());
        }
        //查询区域内已提交企业
        RegisterStatusExample registerStatusExample15 = new RegisterStatusExample();
        registerStatusExample15.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList15 = registerStatusMapper.selectByExample(registerStatusExample15);
        List<Integer> list15 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList15){
            list15.add(registerStatus.getInvId());
        }
        //所有区域内已注册企业
        listRegister.retainAll(list23);
        //已注册企业中去除填写中和已填写的
        listRegister.removeAll(list14);
        listRegister.removeAll(list15);
        return listRegister;
    }

    @Override
    public List<Long> notDownloadList(String area, Integer isAssign) {
        //查询区域内企业
        List<Long> listRegister = Lists.newArrayList();
        listRegister.add(-1L);
        if (isAssign == -1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreet(area);
        }else if (isAssign == 0){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndNotAssign(area);
        }else if (isAssign == 1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndAssign(area);
        }
        //查询区域内填写中企业
        RegisterStatusExample registerStatusExample14 = new RegisterStatusExample();
        registerStatusExample14.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList14 = registerStatusMapper.selectByExample(registerStatusExample14);
        List<Integer> list14 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList14){
            list14.add(registerStatus.getInvId());
        }
        //查询区域内已提交企业
        RegisterStatusExample registerStatusExample15 = new RegisterStatusExample();
        registerStatusExample15.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList15 = registerStatusMapper.selectByExample(registerStatusExample15);
        List<Integer> list15 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList15){
            list15.add(registerStatus.getInvId());
        }

        //查询区域内已下载企业
        CecBaseExample cecBaseExample = new CecBaseExample();
        cecBaseExample.createCriteria().andStateEqualTo(-1).andStatusEqualTo(0);
        List<CecBase> cecBaseList = cecBaseMapper.selectByExample(cecBaseExample);
        List<Integer> listNotDownload = Lists.newArrayList();
        for (CecBase cecBase:cecBaseList){
            listNotDownload.add(cecBase.getInvId());
        }

        //填写中和已填写并集
        list14.addAll(list15);
        //区域内已填写企业
        listRegister.retainAll(list14);
        //区域内已填写未下载企业
        listRegister.removeAll(listNotDownload);
        return listRegister;
    }

    @Override
    public List<Long> notUploadList(String area,Integer isAssign) {
        //查询区域内企业
        List<Long> listRegister = Lists.newArrayList();
        listRegister.add(-1L);
        if (isAssign == -1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreet(area);
        }else if (isAssign == 0){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndNotAssign(area);
        }else if (isAssign == 1){
            //查询区域内企业
            listRegister = newMapper.selectIdListByStreetAndAssign(area);
        }
        //查询区域内已提交企业
        RegisterStatusExample registerStatusExample15 = new RegisterStatusExample();
        registerStatusExample15.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList15 = registerStatusMapper.selectByExample(registerStatusExample15);
        List<Integer> list15 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList15){
            list15.add(registerStatus.getInvId());
        }
        //查询区域内已上传文件企业
        RegisterStatusExample registerStatusExample17 = new RegisterStatusExample();
        registerStatusExample17.createCriteria().andValueIdEqualTo(14);
        List<RegisterStatus> registerStatusList17 = registerStatusMapper.selectByExample(registerStatusExample17);
        List<Integer> list17 = Lists.newArrayList();
        for (RegisterStatus registerStatus:registerStatusList17){
            list17.add(registerStatus.getInvId());
        }

        //已完成企业
        listRegister.retainAll(list15);
        //已完成未上传
        listRegister.removeAll(list17);
        return null;
    }

    private List<ResultRecent30> createList(List<ResultRecent30> resultRecent30s,int days,Date dateEnd){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //Date dateEnd =new Date();
        //生成最近三十天时间列表
        List<ResultRecent30> recent30List = Lists.newArrayList();
        for (int i=0; i<days; i++){
            ResultRecent30 resultRecent = new ResultRecent30();
            resultRecent.setAmount(0);
            Calendar newCa = Calendar.getInstance();
            newCa.setTime(dateEnd);
            newCa.add(newCa.DATE, -i);
            resultRecent.setDateTime(newCa.getTime());
            //循环查出来的数据放入相应的时间内
            for (ResultRecent30 resultRecent30:resultRecent30s){
                if ((resultRecent30.getYear() + "-"+String.format("%02d",resultRecent30.getMonth()) + "-"+ String.format("%02d",resultRecent30.getDay())).equals(df.format(resultRecent.getDateTime()))){
                    resultRecent.setAmount(resultRecent30.getAmount());
                }
            }
            recent30List.add(resultRecent);

        }
        Collections.reverse(recent30List);
        return recent30List;
    }
}
