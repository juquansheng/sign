package net.yunxinyong.sign.service.impl;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecSeven;
import net.yunxinyong.sign.entity.CecSevenExample;
import net.yunxinyong.sign.mapper.CecSevenMapper;
import net.yunxinyong.sign.service.CecSevenService;
import net.yunxinyong.sign.service.RecordService;
import net.yunxinyong.sign.vo.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = CECException.class)
public class CecSevenServiceImpl implements CecSevenService {
    @Autowired
    private CecSevenMapper cecSevenMapper;
    @Autowired
    private RecordService recordService;

    @Override
    public CecSeven select(int invid) {
        CecSevenExample cecSevenExample = new CecSevenExample();
        //id和企业状态为正常作为查询条件
        cecSevenExample.createCriteria().andInvIdEqualTo(invid).andStatusEqualTo(0);
        List<CecSeven> list = cecSevenMapper.selectByExample(cecSevenExample);
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public void update(CecSeven cecSeven, Integer adminId) {
        cecSeven.setUpdateTime(new Date());
        int count = cecSevenMapper.updateByPrimaryKey(cecSeven);
        boolean insert = recordService.insert(adminId, cecSeven.getInvId(), null, 12);
        if (count <= 0 || !insert) {
            throw new CECException(501, "更新失败!");
        }
    }

    @Override
    public void insert(CecSeven cecSeven, Integer adminId) {
        cecSeven.setUpdateTime(new Date());
        cecSeven.setCreateTime(new Date());
        cecSeven.setStatus(0);
        int count = cecSevenMapper.insert(cecSeven);
        boolean insert = recordService.insert(adminId, cecSeven.getInvId(), null, 12);
        if (count <= 0 || !insert) {
            throw new CECException(501, "添加失败!");
        }
    }
}
