package net.yunxinyong.sign.service.impl;



import com.github.pagehelper.PageInfo;
import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.AppointmentTime;
import net.yunxinyong.sign.entity.AppointmentTimeExample;
import net.yunxinyong.sign.entity.EcnomicInventory;
import net.yunxinyong.sign.mapper.AppointmentTimeMapper;
import net.yunxinyong.sign.service.AppointmentTimeService;
import net.yunxinyong.sign.utils.ListUtils;
import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.vo.AppointmentTimeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class AppointmentTimeServiceImpl implements AppointmentTimeService {

    @Autowired
    private AppointmentTimeMapper appointmentTimeMapper;
    @Override
    public AppointmentTime insertOrUpdate(AppointmentTime appointmentTime) {
        appointmentTime.setUpdateTime(new Date());
        int i = 0;
        if (appointmentTime.getId() == null){
            appointmentTime.setCreateTime(new Date());
            i = appointmentTimeMapper.insertSelective(appointmentTime);

        }else {
            i = appointmentTimeMapper.updateByPrimaryKeySelective(appointmentTime);
        }

        if (i <= 0){
            throw new CECException(501,"更新失败");
        }
        return appointmentTime;
    }

    @Override
    public AppointmentTime get(Integer invId) {

        AppointmentTimeExample appointmentTimeExample = new AppointmentTimeExample();
        appointmentTimeExample.createCriteria().andInvIdEqualTo(invId);
        List<AppointmentTime> appointmentTimeList = appointmentTimeMapper.selectByExample(appointmentTimeExample);
        if (!ListUtils.isEmpty(appointmentTimeList)){
            return appointmentTimeList.get(0);
        }
        return null;
    }

    @Override
    public PageBean getList(AppointmentTimeVo appointmentTimeVo) {
        AppointmentTimeExample appointmentTimeExample = new AppointmentTimeExample();
        AppointmentTimeExample.Criteria criteria = appointmentTimeExample.createCriteria();
        //上下午
        if (appointmentTimeVo.getBookingTime() != null){
            criteria.andBookingTimeEqualTo(appointmentTimeVo.getBookingTime());
        }
        //开始时间
        if (appointmentTimeVo.getStartTime() != null){
            criteria.andBookingDayGreaterThanOrEqualTo(appointmentTimeVo.getStartTime());
        }
        //结束时间
        if (appointmentTimeVo.getEndTime() != null){
            criteria.andBookingDayLessThanOrEqualTo(appointmentTimeVo.getEndTime());
        }
        List<AppointmentTime> appointmentTimeList = appointmentTimeMapper.selectByExample(appointmentTimeExample);
        PageBean<AppointmentTime> pageBean = new PageBean<>();
        PageInfo<AppointmentTime> pageInfo = new PageInfo<>(appointmentTimeList);
        pageBean.setTotal(pageInfo.getTotal());
        pageBean.setTotalPages(pageInfo.getPages());
        pageBean.setPageNumber(pageInfo.getPageNum());
        pageBean.setPageSize(pageInfo.getSize());
        pageBean.setPageDatas(appointmentTimeList);
        return pageBean;
    }
}
