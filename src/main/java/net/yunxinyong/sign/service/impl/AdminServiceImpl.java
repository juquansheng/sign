package net.yunxinyong.sign.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.entity.SignAdminExample;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.AdminService;
import net.yunxinyong.sign.utils.ListUtils;
import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.utils.PasswordUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Override
    public SignAdmin getAdminByPhone(String phone) {
        SignAdminExample signAdminExample = new SignAdminExample();
        signAdminExample.createCriteria().andStateEqualTo(0).andPhoneEqualTo(phone);
        List<SignAdmin> signAdmins = signAdminMapper.selectByExample(signAdminExample);
        if (!ListUtils.isEmpty(signAdmins)){
            return signAdmins.get(0);
        }else {
            return null;
        }

    }

    @Override
    public SignAdmin getAdminById(Integer id) {
        return signAdminMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageBean getList(SignAdmin signAdmin,Integer id,int page, int rows) {
        SignAdminExample signAdminExample = new SignAdminExample();
        SignAdminExample.Criteria criteria = signAdminExample.createCriteria().andStateEqualTo(0);
        SignAdmin adminById = getAdminById(id);
        if (adminById != null && adminById.getArea() != null){
            if (!adminById.getArea().equals("unknow")){
                criteria.andAreaEqualTo(adminById.getArea());
            }else {
                criteria.andAreaEqualTo(signAdmin.getArea());
            }
        }else {
            //查不到
            criteria.andAreaEqualTo("unknow1");
        }

        //不能查到root
        criteria.andRoleNotEqualTo("root");
        if (signAdmin.getPhone() != null){
            criteria.andPhoneLike("%"+signAdmin.getPhone()+"%");
        }else {
            criteria.andPhoneEqualTo("");
        }
        if (page != 0 && rows != 0){
            PageHelper.startPage(page, rows);
        }
        List<SignAdmin> signAdminList = signAdminMapper.selectByExample(signAdminExample);
        PageBean<SignAdmin> pageBean = new PageBean<>();
        PageInfo<SignAdmin> pageInfo = new PageInfo<>(signAdminList);
        pageBean.setTotal(pageInfo.getTotal());
        pageBean.setTotalPages(pageInfo.getPages());
        pageBean.setPageNumber(pageInfo.getPageNum());
        pageBean.setPageSize(pageInfo.getSize());
        pageBean.setPageDatas(signAdminList);
        return pageBean;
    }

    @Override
    public void update(SignAdmin signAdmin) throws Exception {
        String alphanumeric = RandomStringUtils.randomAlphanumeric(10);
        signAdmin.setHash(alphanumeric);
        signAdmin.setUpdateTime(new Date());
        signAdmin.setPassword(PasswordUtil.encrypt(signAdmin.getPassword(),alphanumeric));
        int update = signAdminMapper.updateByPrimaryKeySelective(signAdmin);
        if (update <= 0){
            throw new CECException(501,"修改失败");
        }
    }
}
