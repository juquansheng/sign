package net.yunxinyong.sign.service.impl;

import com.google.common.collect.Lists;
import com.sun.org.apache.bcel.internal.generic.NEW;
import net.yunxinyong.sign.constants.CommonConst;
import net.yunxinyong.sign.entity.EcnomicInventory;
import net.yunxinyong.sign.entity.EcnomicInventoryExample;
import net.yunxinyong.sign.mapper.EcnomicInventoryMapper;
import net.yunxinyong.sign.mapper.NewMapper2;
import net.yunxinyong.sign.service.ExcelService;
import net.yunxinyong.sign.utils.DateUtils;
import net.yunxinyong.sign.utils.POIutils;
import net.yunxinyong.sign.vo.EcnomicInventoryVo;
import net.yunxinyong.sign.vo.TestExcelVo;
import net.yunxinyong.sign.vo.TestExcelVo2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.OutputStream;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ExcelServiceImpl implements ExcelService {
    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;
    @Autowired
    private NewMapper2 newMapper2;
    @Override
    public void test(OutputStream outputStream) {
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andUnitDetailedNameLike("%"+"跃跃"+"%");
        List<EcnomicInventory> ecnomicInventoryList = ecnomicInventoryMapper.selectByExample(ecnomicInventoryExample);
        List<TestExcelVo> testExcelVos = Lists.newArrayList();
        for (EcnomicInventory ecnomicInventory:ecnomicInventoryList){
            TestExcelVo testExcelVo = new TestExcelVo();
            testExcelVo.setAddress(ecnomicInventory.getReferenceAddress());
            testExcelVo.setCede(ecnomicInventory.getAreaCode());
            testExcelVo.setMenber(ecnomicInventory.getCellPhoneNumber());
            testExcelVo.setName(ecnomicInventory.getUnitDetailedName());
            testExcelVo.setSocialCode(ecnomicInventory.getSocialCreditCode());
            testExcelVo.setUpdateTime(DateUtils.format(ecnomicInventory.getUpdatetime()));
            testExcelVos.add(testExcelVo);
        }
        new POIutils<TestExcelVo>()
                .exportExcel("test表", testExcelVos, outputStream);
    }
    
    @Override
    public void listEnterprise(OutputStream outputStream) {

        List<EcnomicInventoryVo> voList = newMapper2.listRegisterEnterprise();
        //List<EcnomicInventoryVo> voList1 = voList.subList(0, 10);
        List<TestExcelVo2> testExcelVo2 = Lists.newArrayList();
        for (EcnomicInventoryVo ecnomicInventoryVo : voList){
            TestExcelVo2 excelVo2 = new TestExcelVo2();
            excelVo2.setOrganizationCode(ecnomicInventoryVo.getOrganizationCode());
            excelVo2.setSocialCreditCode(ecnomicInventoryVo.getSocialCreditCode());
            excelVo2.setUnitDetailedName(ecnomicInventoryVo.getUnitDetailedName());
            excelVo2.setLegalRepresentative(ecnomicInventoryVo.getLegalRepresentative());
            excelVo2.setPhone(ecnomicInventoryVo.getPhone());
            testExcelVo2.add(excelVo2);
        }
        new POIutils<TestExcelVo2>().exportExcel("公司名单",testExcelVo2,outputStream);
    }



}
