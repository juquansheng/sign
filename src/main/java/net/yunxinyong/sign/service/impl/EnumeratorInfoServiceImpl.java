package net.yunxinyong.sign.service.impl;

import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j2;
import net.yunxinyong.sign.entity.*;
import net.yunxinyong.sign.mapper.EcnomicInventoryMapper;
import net.yunxinyong.sign.mapper.NewMapper;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.newEntity.RequestRecent30;
import net.yunxinyong.sign.service.EnumeratorInfoService;
import net.yunxinyong.sign.vo.StatusStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Log4j2
public class EnumeratorInfoServiceImpl implements EnumeratorInfoService {
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private EcnomicInventoryMapper ecnomicInventoryMapper;
    @Autowired
    private NewMapper newMapper;

    @Override
    public StatusStatisticsVo getAllInfo(int adminId) {
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        //负责的企业总数
        int enterpriseTotal = getEnterpriseTotal(signAdmin);
        //其中新增企业总数
        int newEnterpriseTotal = getNewEnterpriseTotal(signAdmin);
        //其中已通知的企业总数
        int informed = getInformed(signAdmin.getArea(), signAdmin.getId());
        //其中已注册的企业总数
        int registered = getRegistered(signAdmin.getArea(), signAdmin.getId());
        //其中已完成的企业总数
        int completed = getCompleted(signAdmin.getArea(), signAdmin.getId());
        //其中录入的企业总数
        int entered = getEntered(signAdmin.getArea(), signAdmin.getId());
        //其中已上传的企业总数
        int uploaded = getUploaded(signAdmin.getArea(), signAdmin.getId());
        //log.info("账户:" + signAdmin.getPhone() + "\n级别:" + signAdmin.getRole() + "\n负责的企业总数:" + enterpriseTotal + "\n其中已通知的企业总数" + informed + "\n其中已注册的企业总数" + registered + "\n其中已完成的企业总数" + completed + "\n其中录入的企业总数" + entered + "\n其中已上传的企业总数" + uploaded);
        StatusStatisticsVo statusStatisticsVo = new StatusStatisticsVo();
        statusStatisticsVo.setTotal(enterpriseTotal);
        statusStatisticsVo.setNewUnit(newEnterpriseTotal);
        statusStatisticsVo.setInformed(informed);
        statusStatisticsVo.setRegistered(registered);
        statusStatisticsVo.setCompleted(completed);
        statusStatisticsVo.setEntered(entered);
        statusStatisticsVo.setUploaded(uploaded);
        return statusStatisticsVo;
    }

    //负责的企业总数
    public int getEnterpriseTotal(SignAdmin signAdmin) {
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andAdminIdEqualTo(signAdmin.getId()).andReferenceAddress02EqualTo(signAdmin.getArea());
        int count = ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);
        return count;
    }
    //负责的企业总数
    public int getNewEnterpriseTotal(SignAdmin signAdmin) {
        EcnomicInventoryExample ecnomicInventoryExample = new EcnomicInventoryExample();
        ecnomicInventoryExample.createCriteria().andAdminIdEqualTo(signAdmin.getId()).andReferenceAddress02EqualTo(signAdmin.getArea()).andUiteStatusEqualTo(2);
        int count = ecnomicInventoryMapper.countByExample(ecnomicInventoryExample);
        return count;
    }

    //已通知
    public int getInformed(String area, int adminId) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setAdminId(adminId);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(8);
        return newMapper.select(requestRecent30);
    }

    //已注册
    public int getRegistered(String area, int adminId) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setAdminId(adminId);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(23);
        return newMapper.select(requestRecent30);
    }

    //已完成
    public int getCompleted(String area, int adminId) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setAdminId(adminId);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(15);
        return newMapper.select(requestRecent30);
    }

    //已录入
    public int getEntered(String area, int adminId) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setAdminId(adminId);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(19);
        return newMapper.select(requestRecent30);
    }

    //已上传
    public int getUploaded(String area, int adminId) {
        RequestRecent30 requestRecent30 = new RequestRecent30();
        requestRecent30.setAdminId(adminId);
        requestRecent30.setArea(area);
        requestRecent30.setValueId(17);
        return newMapper.select(requestRecent30);
    }


}
