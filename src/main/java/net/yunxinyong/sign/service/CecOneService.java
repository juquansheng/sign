package net.yunxinyong.sign.service;


import net.yunxinyong.sign.entity.CecOne;

import java.util.List;

public interface CecOneService {
    /**
     * 添加,修改企业信息
     */
    void insertOrUpdate(List<CecOne> cecOneList);
    /**
     * 查询企业信息
     * @return
     */
    List<CecOne> get(Integer invId);

}
