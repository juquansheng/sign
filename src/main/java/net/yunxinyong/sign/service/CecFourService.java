package net.yunxinyong.sign.service;


import net.yunxinyong.sign.entity.CecFour;

import java.util.List;

public interface CecFourService {
    /**
     * 添加,修改企业信息
     */
    void insertOrUpdate(List<CecFour> cecFourList);
    /**
     *查询企业信息
     */
    List<CecFour> get(Integer invId);

}
