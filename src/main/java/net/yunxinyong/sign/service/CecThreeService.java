package net.yunxinyong.sign.service;

import net.yunxinyong.sign.vo.CecThreeVo;

public interface CecThreeService {
    /**
     * 添加,修改企业信息
     */
    void insertOrUpdate(CecThreeVo CecThreeVo);
    /**
     *查询企业信息
     */
    CecThreeVo get(Integer invId);

}
