package net.yunxinyong.sign.service;

import java.util.List;

/**
 * 分配任务接口
 * 11/28
 * 李跃
 */
public interface AssignTasksService {

    /**
     * 给普查员分配任务
     * @param idList 企业id列表
     * @param censusTakerId 普查员id
     * @param type 判断是取消企业，还是添加企业 type=1 代表添加任务 type=2 代表删除任务
     */
    void AssignTasksToCunsus(List<Long> idList , Integer censusTakerId, Integer type,Integer managerId);


}
