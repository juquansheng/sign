package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.AppointmentTime;
import net.yunxinyong.sign.entity.AppointmentTimeExample;
import org.apache.ibatis.annotations.Param;

public interface AppointmentTimeMapper {
    int countByExample(AppointmentTimeExample example);

    int deleteByExample(AppointmentTimeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(AppointmentTime record);

    int insertSelective(AppointmentTime record);

    List<AppointmentTime> selectByExample(AppointmentTimeExample example);

    AppointmentTime selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") AppointmentTime record, @Param("example") AppointmentTimeExample example);

    int updateByExample(@Param("record") AppointmentTime record, @Param("example") AppointmentTimeExample example);

    int updateByPrimaryKeySelective(AppointmentTime record);

    int updateByPrimaryKey(AppointmentTime record);
}