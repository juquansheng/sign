package net.yunxinyong.sign.mapper;




import net.yunxinyong.sign.newEntity.RequestRecent30;
import net.yunxinyong.sign.newEntity.ResultRecent30;

import java.util.List;

public interface NewMapper {


    List<ResultRecent30> selectRecent30(RequestRecent30 requestRecent30);

    /**
     * 新增企业
     */
    List<ResultRecent30> selectAdd30(RequestRecent30 requestRecent30);

    /**
     * 已通知未注册
     * @param requestRecent30
     * @return
     */
    int selectNotRegister(RequestRecent30 requestRecent30);

    /**
     * 已注册未提交
     * @param requestRecent30
     * @return
     */
    int selectNotCheck(RequestRecent30 requestRecent30);

    /**
     * 查询当天数据
     * @param requestRecent30
     * @return
     */
    int selectToday(RequestRecent30 requestRecent30);

    /**
     * 查询某个街道内是否已经 通知、提交、注册、上传 的企业
      * @param requestRecent30
     * @return  返回值是id的list
     */
    List<Long> getStatus(RequestRecent30 requestRecent30);

    /**
     * 查出指定街道的id列表
     * @param street
     * @return
     */


    /**
     * 查询数据
     * @param requestRecent30
     * @return
     */
    int select(RequestRecent30 requestRecent30);

    List<Long> selectIdListByStreet(String street);
    /**
     * 查出指定街道的id列表(未分配任务)
     * @param street
     * @return
     */
    List<Long> selectIdListByStreetAndNotAssign(String street);
    /**
     * 查出指定街道的id列表(未分配任务)
     * @param street
     * @return
     */
    List<Long> selectIdListByStreetAndAssign(String street);
}