package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.CecTwo;
import net.yunxinyong.sign.entity.CecTwoExample;
import org.apache.ibatis.annotations.Param;

public interface CecTwoMapper {
    int countByExample(CecTwoExample example);

    int deleteByExample(CecTwoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecTwo record);

    int insertSelective(CecTwo record);

    List<CecTwo> selectByExample(CecTwoExample example);

    CecTwo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecTwo record, @Param("example") CecTwoExample example);

    int updateByExample(@Param("record") CecTwo record, @Param("example") CecTwoExample example);

    int updateByPrimaryKeySelective(CecTwo record);

    int updateByPrimaryKey(CecTwo record);
}