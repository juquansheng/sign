package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.StreetCommunity;
import net.yunxinyong.sign.entity.StreetCommunityExample;
import org.apache.ibatis.annotations.Param;

public interface StreetCommunityMapper {
    int countByExample(StreetCommunityExample example);

    int deleteByExample(StreetCommunityExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(StreetCommunity record);

    int insertSelective(StreetCommunity record);

    List<StreetCommunity> selectByExample(StreetCommunityExample example);

    StreetCommunity selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") StreetCommunity record, @Param("example") StreetCommunityExample example);

    int updateByExample(@Param("record") StreetCommunity record, @Param("example") StreetCommunityExample example);

    int updateByPrimaryKeySelective(StreetCommunity record);

    int updateByPrimaryKey(StreetCommunity record);
}