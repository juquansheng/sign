package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.CecUser;
import net.yunxinyong.sign.entity.CecUserExample;
import org.apache.ibatis.annotations.Param;

public interface CecUserMapper {
    int countByExample(CecUserExample example);

    int deleteByExample(CecUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecUser record);

    int insertSelective(CecUser record);

    List<CecUser> selectByExample(CecUserExample example);

    CecUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecUser record, @Param("example") CecUserExample example);

    int updateByExample(@Param("record") CecUser record, @Param("example") CecUserExample example);

    int updateByPrimaryKeySelective(CecUser record);

    int updateByPrimaryKey(CecUser record);
}