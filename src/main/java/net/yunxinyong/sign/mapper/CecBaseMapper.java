package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.CecBase;
import net.yunxinyong.sign.entity.CecBaseExample;
import org.apache.ibatis.annotations.Param;

public interface CecBaseMapper {
    int countByExample(CecBaseExample example);

    int deleteByExample(CecBaseExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecBase record);

    int insertSelective(CecBase record);

    List<CecBase> selectByExample(CecBaseExample example);

    CecBase selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecBase record, @Param("example") CecBaseExample example);

    int updateByExample(@Param("record") CecBase record, @Param("example") CecBaseExample example);

    int updateByPrimaryKeySelective(CecBase record);

    int updateByPrimaryKey(CecBase record);
}