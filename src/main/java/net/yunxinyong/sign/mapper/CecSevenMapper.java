package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.CecSeven;
import net.yunxinyong.sign.entity.CecSevenExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface CecSevenMapper {
    int countByExample(CecSevenExample example);

    int deleteByExample(CecSevenExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecSeven record);

    int insertSelective(CecSeven record);

    List<CecSeven> selectByExample(CecSevenExample example);

    CecSeven selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecSeven record, @Param("example") CecSevenExample example);

    int updateByExample(@Param("record") CecSeven record, @Param("example") CecSevenExample example);

    int updateByPrimaryKeySelective(CecSeven record);

    int updateByPrimaryKey(CecSeven record);
}