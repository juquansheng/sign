package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.EcnomicInventory;
import net.yunxinyong.sign.entity.EcnomicInventoryExample;
import org.apache.ibatis.annotations.Param;

public interface EcnomicInventoryMapper {
    int countByExample(EcnomicInventoryExample example);

    int deleteByExample(EcnomicInventoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(EcnomicInventory record);

    int insertSelective(EcnomicInventory record);

    List<EcnomicInventory> selectByExample(EcnomicInventoryExample example);

    EcnomicInventory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") EcnomicInventory record, @Param("example") EcnomicInventoryExample example);

    int updateByExample(@Param("record") EcnomicInventory record, @Param("example") EcnomicInventoryExample example);

    int updateByPrimaryKeySelective(EcnomicInventory record);

    int updateByPrimaryKey(EcnomicInventory record);


}