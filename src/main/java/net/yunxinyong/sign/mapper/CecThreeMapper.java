package net.yunxinyong.sign.mapper;

import java.util.List;
import net.yunxinyong.sign.entity.CecThree;
import net.yunxinyong.sign.entity.CecThreeExample;
import org.apache.ibatis.annotations.Param;

public interface CecThreeMapper {
    int countByExample(CecThreeExample example);

    int deleteByExample(CecThreeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(CecThree record);

    int insertSelective(CecThree record);

    List<CecThree> selectByExample(CecThreeExample example);

    CecThree selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") CecThree record, @Param("example") CecThreeExample example);

    int updateByExample(@Param("record") CecThree record, @Param("example") CecThreeExample example);

    int updateByPrimaryKeySelective(CecThree record);

    int updateByPrimaryKey(CecThree record);
}