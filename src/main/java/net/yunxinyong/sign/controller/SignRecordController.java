package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.RecordService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import net.yunxinyong.sign.vo.SignRecordReqVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "record")
public class SignRecordController {

    @Autowired
    private RecordService recordService;
    @Autowired
    private SignAdminMapper signAdminMapper;


    @RequestMapping(value = "getList",method = RequestMethod.POST)
    public ResponseVo getList(@RequestBody SignRecordReqVo signRecordReqVo, @Param("page") Integer page, @Param("rows") Integer rows){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        return CECResultUtil.success("获取成功",recordService.getList(signRecordReqVo,adminId,page,rows));
    }
}
