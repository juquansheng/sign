package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.Street;
import net.yunxinyong.sign.entity.StreetCommunity;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.StreetService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.utils.ListUtils;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *@describe 查询街道相关
 * @date 18/11/14
 * @author  李跃
 *
 */

@RequestMapping("street")
@RestController
public class StreetController {
    @Autowired
    private StreetService streetService;
    @Autowired
    private SignAdminMapper signAdminMapper;

    /**
     * 查询所有的街道
     * @return
     */
    @RequestMapping(value = "getAll",method = RequestMethod.GET)
    public ResponseVo getAllStreet(){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        List<Street> streetList = streetService.getAllStreet();
        if(!ListUtils.isEmpty(streetList)){
            return CECResultUtil.success("查询到了街道",streetList);
        }else {
            return CECResultUtil.error(501,"没有查询到街道信息");
        }
    }

    /**
     * 查询街道下对应的社区
     *  返回值为streetCommunity实体类的集合
     */
    @RequestMapping(value = "getCommunity",method = RequestMethod.GET)
    public ResponseVo getCommunity(){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        String street = signAdminMapper.selectByPrimaryKey(adminId).getArea();
        List<StreetCommunity> streetCommunityList = streetService.getCoummunity(street);
        if (ListUtils.isEmpty(streetCommunityList)){
            return CECResultUtil.error(501,"当前街道下没有社区信息");
        }
        return CECResultUtil.success("查询到了街道信息",streetCommunityList);
    }
}
