package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecBase;
import net.yunxinyong.sign.entity.CecOne;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.CecBaseService;
import net.yunxinyong.sign.service.EcnomicService;
import net.yunxinyong.sign.service.RegisterStatusService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "base")
public class CecBaseController {
    @Autowired
    private CecBaseService cecBaseService;
    @Autowired
    private EcnomicService ecnomicService;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private RegisterStatusService registerStatusService;
    @RequestMapping(value = "update")
    public ResponseVo addAndupdate(@RequestBody CecBase cecBase) {
        //获取用户id
        Object principals = SecurityUtils.getSubject().getPrincipals();
        Integer adminId = Integer.parseInt(principals.toString());

        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecBase cecBase1 = cecBaseService.select(cecBase.getInvId());
        if (cecBase1 != null) {
            if (cecBase1.getState() != null && cecBase1.getState() == -1) {
                return CECResultUtil.error("企业状态为不可修改");
            }
            cecBase.setId(cecBase1.getId());
            cecBaseService.update(cecBase,adminId);
        } else {
            cecBaseService.insert(cecBase,adminId);
        }
        //更新状态
        ecnomicService.updateType(cecBase.getInvId());
        //更新可下载状态
        registerStatusService.updadeDownloadStatus(cecBase.getInvId());
        return CECResultUtil.success("操作成功", cecBase);
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ResponseVo get(@RequestParam Integer invId) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int id = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(id) != null && signAdminMapper.selectByPrimaryKey(id).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecBase select = cecBaseService.select(invId);
        return CECResultUtil.success("获取成功", select);
    }
}
