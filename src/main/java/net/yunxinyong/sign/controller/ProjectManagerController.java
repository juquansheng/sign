package net.yunxinyong.sign.controller;


import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.ProjectManagerService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

/**
 *@describe 账号管理
 *@author  ttbf
 *@date  2018/10/24
 */
@RestController
@RequestMapping(value = "manager")
public class ProjectManagerController {
    @Autowired
    private ProjectManagerService projectManagerService;
    @Autowired
    private SignAdminMapper signAdminMapper;


    /**
     * 添加管理人员
     * @return
     */
    @RequestMapping(value = "insert",method = RequestMethod.POST)
    public ResponseVo add(@RequestBody SignAdmin signAdmin){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }

        if (projectManagerService.insert(signAdmin,adminId) != null){
            return CECResultUtil.success("添加成功");
        }
        return CECResultUtil.error("添加失败");
    }

    /**
     * 修改权限
     * @return
     */
    @RequestMapping(value = "update",method = RequestMethod.POST)
    public ResponseVo update(@RequestBody SignAdmin signAdmin){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        if (projectManagerService.update(signAdmin,adminId) != null){
            return CECResultUtil.success("修改成功");
        }
        return CECResultUtil.error("修改失败");
    }

    /**
     * 获取普查员，管理员列表
     * @return
     */
    @RequestMapping(value = "list",method = RequestMethod.POST)
    public ResponseVo getList(@RequestBody SignAdmin signAdmin, @Param("page") Integer page,@Param("rows") Integer rows){
        //获取用户id
        Object principals = SecurityUtils.getSubject().getPrincipals();
        Integer id = Integer.parseInt(principals.toString());
        return CECResultUtil.success("查询成功",projectManagerService.getList(signAdmin,id,page,rows));

    }

}
