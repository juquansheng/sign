package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecFour;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.CecFourService;
import net.yunxinyong.sign.service.RecordService;
import net.yunxinyong.sign.service.RegisterStatusService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "four")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = CECException.class)
public class CecFourController {
    @Autowired
    private CecFourService cecFourService;
    @Autowired
    private RecordService recordService;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private RegisterStatusService registerStatusService;

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseVo add(@RequestBody List<CecFour> cecFourList) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        cecFourService.insertOrUpdate(cecFourList);
        //更新可下载状态
        registerStatusService.updadeDownloadStatus(cecFourList.get(0).getInvId());

        boolean insert = recordService.insert(adminId, null, null, 12);
        if(!insert){
            throw new CECException(501,"操作失败");
        }
        return CECResultUtil.success("操作成功");
    }

    /**
     * 查询企业信息
     *
     * @return
     */
    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ResponseVo get(@RequestParam Integer invId) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        List<CecFour> cecFourList = cecFourService.get(invId);
        return CECResultUtil.success("获取成功", cecFourList);
    }

}
