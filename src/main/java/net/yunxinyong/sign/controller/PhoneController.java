package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.Phone;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.PhoneService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.vo.PhoneAndStatusVo;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "phone")
public class PhoneController {
    @Autowired
    private PhoneService phoneService;
    @Autowired
    private SignAdminMapper signAdminMapper;

    @RequestMapping(value = "getPhoneList", method = RequestMethod.POST)
    public ResponseVo getPhoneList(@RequestBody Phone phone , int page, int rows) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        PageBean list = phoneService.getList(phone, adminId, page, rows);
        return CECResultUtil.success("获取成功!", list);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseVo update(@RequestBody Phone phone) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        Phone update = phoneService.update(phone, adminId);
        return CECResultUtil.success("修改成功!", update);
    }

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    public ResponseVo insert(@RequestBody Phone phone) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        Phone insert = phoneService.insert(phone, adminId);
        return CECResultUtil.success("添加成功!", insert);
    }
}
