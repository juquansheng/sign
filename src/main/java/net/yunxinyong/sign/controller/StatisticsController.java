package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.entity.Status;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.newEntity.RequestRecent30;
import net.yunxinyong.sign.newEntity.ResultRecent30;
import net.yunxinyong.sign.service.StatisticsService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.utils.PageBean;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(value = "statistics")
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;
    @Autowired
    private SignAdminMapper signAdminMapper;

    /**
     * 添加企业总数
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "getAdd",method = RequestMethod.POST)
    public ResponseVo getAdd(@RequestBody RequestRecent30 requestRecent30){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        Integer amountByAdd = 0;
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            amountByAdd = statisticsService.getAmountByAdd(requestRecent30.getArea());
        }else {
            return CECResultUtil.error("用户不存在");
        }

        return CECResultUtil.success("获取成功",amountByAdd);
    }

    /**
     * 某状态企业总数
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "getStatus",method = RequestMethod.POST)
    public ResponseVo getStatus(@RequestBody RequestRecent30 requestRecent30){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        Integer amountByStatus = 0;
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            amountByStatus = statisticsService.getAmountByStatus(requestRecent30.getValueId(), requestRecent30.getArea());
        }else {
            return CECResultUtil.error("用户不存在");
        }

        return CECResultUtil.success("获取成功",amountByStatus);
    }

    /**
     * 最近三十天某状态企业数量
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "recent30",method = RequestMethod.POST)
    public ResponseVo recent30(@RequestBody RequestRecent30 requestRecent30){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            List<ResultRecent30> list = statisticsService.getList(requestRecent30);
            return CECResultUtil.success("获取成功",list);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }

    /**
     * 最近三十天新增企业数量
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "recentAdd30",method = RequestMethod.POST)
    public ResponseVo recentAdd30(@RequestBody RequestRecent30 requestRecent30){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            List<ResultRecent30> list = statisticsService.getAddList(requestRecent30);
            return CECResultUtil.success("获取成功",list);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }


    /**
     * 已通知未注册
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "getNotRegister",method = RequestMethod.POST)
    public ResponseVo getNotRegister(@RequestBody RequestRecent30 requestRecent30){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            Integer notRegister = statisticsService.getNotRegister(requestRecent30);
            return CECResultUtil.success("获取成功",notRegister);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }

    /**
     * 已注册未提交
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "getNotCheck",method = RequestMethod.POST)
    public ResponseVo getNotCheck(@RequestBody RequestRecent30 requestRecent30){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            Integer notCheck = statisticsService.getNotCheck(requestRecent30);
            return CECResultUtil.success("获取成功",notCheck);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }


    /**
     * 已审核未上传
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "getNotUpload",method = RequestMethod.POST)
    public ResponseVo getNotUpload(@RequestBody RequestRecent30 requestRecent30){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            Integer notUpload = statisticsService.getNotUpload(requestRecent30.getArea());
            return CECResultUtil.success("获取成功",notUpload);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }


    /**
     * 已上传未录入
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "getNotSign",method = RequestMethod.POST)
    public ResponseVo getNotSign(@RequestBody RequestRecent30 requestRecent30){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            Integer notSign = statisticsService.getNotSign(requestRecent30.getArea());
            return CECResultUtil.success("获取成功",notSign);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }


    /**
     * 未联系上（未注册，未填写）
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "notContact",method = RequestMethod.POST)
    public ResponseVo notContact(@RequestBody RequestRecent30 requestRecent30,
                                 @Param("page") Integer page,
                                 @Param("rows") Integer rows){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            PageBean pageBean = statisticsService.notContact(requestRecent30.getArea(), page, rows, requestRecent30.getIsAssign());
            return CECResultUtil.success("获取成功",pageBean);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }

    /**
     * 已注册未填写
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "notWrite",method = RequestMethod.POST)
    public ResponseVo notWrite(@RequestBody RequestRecent30 requestRecent30,
                                 @Param("page") Integer page,
                                 @Param("rows") Integer rows){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            PageBean pageBean = statisticsService.notWrite(requestRecent30.getArea(), page, rows, requestRecent30.getIsAssign());
            return CECResultUtil.success("获取成功",pageBean);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }

    /**
     * 已填写未未下载
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "notDownload",method = RequestMethod.POST)
    public ResponseVo notDownload(@RequestBody RequestRecent30 requestRecent30,
                                 @Param("page") Integer page,
                                 @Param("rows") Integer rows){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            PageBean pageBean = statisticsService.notDownload(requestRecent30.getArea(), page, rows, requestRecent30.getIsAssign());
            return CECResultUtil.success("获取成功",pageBean);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }

    /**
     * 已完成未上传文件
     * @param requestRecent30
     * @return
     */
    @RequestMapping(value = "notUpload",method = RequestMethod.POST)
    public ResponseVo notUpload(@RequestBody RequestRecent30 requestRecent30,
                                 @Param("page") Integer page,
                                 @Param("rows") Integer rows){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(adminId);
        if (signAdmin != null){
            if (!signAdmin.getArea().equals("unknow")){
                requestRecent30.setArea(signAdmin.getArea());
            }
            PageBean pageBean = statisticsService.notUpload(requestRecent30.getArea(), page, rows, requestRecent30.getIsAssign());
            return CECResultUtil.success("获取成功",pageBean);
        }else {
            return CECResultUtil.error("用户不存在");
        }
    }
}
