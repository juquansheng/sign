package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.CecTwo;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.CecTwoService;
import net.yunxinyong.sign.service.RegisterStatusService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "two")
public class CecTwoController {
    @Autowired
    private CecTwoService cecTwoService;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private RegisterStatusService registerStatusService;
    @RequestMapping(value = "update")
    public ResponseVo addAndupdate(@RequestBody CecTwo cecTwo) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecTwo cecTwo1 = cecTwoService.select(cecTwo.getInvId());
        if (cecTwo1 != null) {
            if (cecTwo1.getState() != null && cecTwo1.getState() == -1) {
                return CECResultUtil.error("企业状态为不可修改");
            }
            cecTwo.setId(cecTwo1.getId());
            cecTwoService.update(cecTwo,adminId);
        } else {
            cecTwoService.insert(cecTwo,adminId);
        }
        //更新可下载状态
        registerStatusService.updadeDownloadStatus(cecTwo.getInvId());
        return CECResultUtil.success("操作成功", cecTwo);
    }

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public ResponseVo get(@RequestParam Integer invId) {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        CecTwo select = cecTwoService.select(invId);
        return CECResultUtil.success("获取成功", select);
    }
}
