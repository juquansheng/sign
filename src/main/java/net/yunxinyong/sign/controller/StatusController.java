package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.Status;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.StatusService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "status")
public class StatusController {

    @Autowired
    private StatusService statusService;
    @Autowired
    private SignAdminMapper signAdminMapper;


    /**
     * 添加状态
     * @return
     */
    @RequestMapping(value = "insert",method = RequestMethod.POST)
    public ResponseVo add(@RequestBody Status status){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        if (statusService.insert(status,adminId)){
            return CECResultUtil.success("添加成功");
        }
        return CECResultUtil.error("添加失败");
    }

    /**
     * 修改状态
     * @return
     */
    @RequestMapping(value = "update",method = RequestMethod.POST)
    public ResponseVo update(@RequestBody Status status){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        if (statusService.update(status,adminId)){
            return CECResultUtil.success("修改成功");
        }
        return CECResultUtil.error("修改失败");
    }

    @RequestMapping(value = "getList",method = RequestMethod.GET)
    public ResponseVo getList(@Param("parentId") Integer parentId){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }

        return CECResultUtil.success("获取成功",statusService.getList(parentId,adminId));
    }
}
