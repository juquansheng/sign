package net.yunxinyong.sign.controller;




import net.yunxinyong.sign.service.VerificationCodeService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *@describe 验证码相关接口
 *@author  ttbf
 *@date  2018/9/28
 */
@RestController
@RequestMapping(value = "/code")
public class VerificationCodeController {
    @Autowired
    private VerificationCodeService verificationCodeService;
    private static final Logger logger = LoggerFactory.getLogger(VerificationCodeController.class);

    @RequestMapping(value = "/getcode",method = RequestMethod.GET)
    public ResponseVo getCode(@Param("phone") String phone){
        String code = verificationCodeService.getAndSaveVerificationCode(phone);
        logger.info(code);
        if (code!=null){
            return CECResultUtil.success("获取验证码成功");
        }else {
            return CECResultUtil.error("获取验证码失败");
        }
    }

    @RequestMapping(value = "/check",method = RequestMethod.GET)
    public ResponseVo check(@Param("phone") String phone, @Param("code") String code){
        boolean checkVerifyCode = verificationCodeService.checkVerifyCode(phone, code);
        if (checkVerifyCode){
            return CECResultUtil.success("验证码正确");
        }else {
            return CECResultUtil.error("验证码错误");
        }
    }
}
