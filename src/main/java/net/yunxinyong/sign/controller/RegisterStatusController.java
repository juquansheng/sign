package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.RegisterStatus;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.RegisterStatusService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.utils.SignResult;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *@describe 标记相关
 *@author  ttbf
 *@date  2018/10/24
 */
@RestController
@RequestMapping(value = "registerStatus")
public class RegisterStatusController {
    @Autowired
    private RegisterStatusService registerStatusService;
    @Autowired
    private SignAdminMapper signAdminMapper;

    /**
     * 添加标记
     * @return
     */
    @RequestMapping(value = "insert",method = RequestMethod.POST)
    public ResponseVo add(@RequestBody RegisterStatus registerStatus){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }

        if (registerStatusService.insert(registerStatus,adminId)){
            return CECResultUtil.success("添加成功");
        }
        return CECResultUtil.error("添加失败");
    }

    /**
     * 修改标记
     * @return
     */
    @RequestMapping(value = "update",method = RequestMethod.POST)
    public ResponseVo update(@RequestBody RegisterStatus registerStatus){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        if (registerStatusService.update(registerStatus,adminId)){
            return CECResultUtil.success("修改成功");
        }
        return CECResultUtil.error("修改失败");
    }

    /**
     * 获取企业状态
     * @return
     */
    @RequestMapping(value = "get",method = RequestMethod.GET)
    public ResponseVo get(@Param("invId") Integer invId){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }

        return CECResultUtil.success("查询",registerStatusService.getRegisterStatusList(invId));

    }
}
