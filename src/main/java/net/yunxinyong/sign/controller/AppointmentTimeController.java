package net.yunxinyong.sign.controller;


import net.yunxinyong.sign.entity.AppointmentTime;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.AppointmentTimeService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *@describe 预约上门
 *@author  ttbf
 *@date  2018/9/28
 */
@RestController
@RequestMapping(value = "appoint")
public class AppointmentTimeController {

    @Autowired
    private AppointmentTimeService appointmentTimeService;
    @Autowired
    private SignAdminMapper signAdminMapper;

    @RequestMapping(value = "update",method = RequestMethod.POST)
    public ResponseVo insert(@RequestBody AppointmentTime appointmentTime){
        AppointmentTime insert = appointmentTimeService.insertOrUpdate(appointmentTime);
        return CECResultUtil.success("预约成功",insert);
    }

    @RequestMapping(value = "get",method = RequestMethod.GET)
    public ResponseVo update(@RequestParam Integer invId){
        AppointmentTime appointmentTime = appointmentTimeService.get(invId);

        return CECResultUtil.success("获取成功",appointmentTime);
    }
}
