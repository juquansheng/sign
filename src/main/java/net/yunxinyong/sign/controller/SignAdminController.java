package net.yunxinyong.sign.controller;

import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.AdminService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "admin")
public class SignAdminController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private SignAdminMapper signAdminMapper;


    @RequestMapping(value = "getList",method = RequestMethod.POST)
    public ResponseVo getList(@RequestBody SignAdmin signAdmin, @Param("page") Integer page, @Param("rows") Integer rows){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        return CECResultUtil.success("获取成功",adminService.getList(signAdmin,adminId,page,rows));
    }

    @RequestMapping(value = "get",method = RequestMethod.GET)
    public ResponseVo getList(@Param("id") Integer id){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        return CECResultUtil.success("获取成功",adminService.getAdminById(id));
    }

    @RequestMapping(value = "password",method = RequestMethod.POST)
    public ResponseVo update(@RequestBody SignAdmin signAdmin) throws Exception {
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int adminId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(adminId) != null && signAdminMapper.selectByPrimaryKey(adminId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        signAdmin.setId(adminId);
        adminService.update(signAdmin);
        return CECResultUtil.success("修改成功");
    }


}
