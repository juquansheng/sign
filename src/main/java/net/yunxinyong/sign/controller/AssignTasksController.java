package net.yunxinyong.sign.controller;

import com.google.common.collect.Lists;
import net.yunxinyong.sign.aop.CECException;
import net.yunxinyong.sign.entity.SignAdmin;
import net.yunxinyong.sign.mapper.SignAdminMapper;
import net.yunxinyong.sign.service.AssignTasksService;
import net.yunxinyong.sign.service.StatisticsService;
import net.yunxinyong.sign.utils.CECResultUtil;
import net.yunxinyong.sign.utils.ListUtils;
import net.yunxinyong.sign.vo.ResponseVo;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.geom.Area;
import java.util.List;

@RestController
@RequestMapping("tasks")
public class AssignTasksController {
    @Autowired
    private AssignTasksService assignTasksService;
    @Autowired
    private SignAdminMapper signAdminMapper;
    @Autowired
    private StatisticsService statisticsService;

    @RequestMapping(value = "assignTasks",method = RequestMethod.POST)
    public ResponseVo assignTasksToCensus(@RequestBody List<Long> idList, @RequestParam(required = false) Integer censusTakerId, @RequestParam Integer type){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int managerId = Integer.parseInt(principal.toString());
        if (signAdminMapper.selectByPrimaryKey(managerId) != null && signAdminMapper.selectByPrimaryKey(managerId).getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        assignTasksService.AssignTasksToCunsus(idList,censusTakerId,type,managerId);
        return  CECResultUtil.success("操作成功");
    }


    @RequestMapping(value = "conditionAssign",method = RequestMethod.GET)
    public ResponseVo conditionAssign(@RequestParam Integer condition,
                                      @RequestParam(required = false) Integer censusTakerId,
                                      @RequestParam Integer type,
                                      @RequestParam(required = false) String area){
        //获取用户id
        Object principal = SecurityUtils.getSubject().getPrincipal();
        int managerId = Integer.parseInt(principal.toString());
        SignAdmin signAdmin = signAdminMapper.selectByPrimaryKey(managerId);
        if (signAdmin == null){
            throw new CECException(501,"用户不存在");
        }
        if (signAdmin.getState() == 1){
            throw new CECException(402,"当前用户已被禁用");
        }
        if (!signAdmin.getArea().equals("unknow")){
            area = signAdmin.getArea();
        }

        Integer isAssign = 0;
        if (type == 2){
            isAssign = 1;
        }
        List<Long> idList = Lists.newArrayList();
        if (condition == 1){//未联系上
            idList = statisticsService.notContactList(area, isAssign);
        }else if (condition == 2){//已注册未填写
            idList = statisticsService.notWriteList(area, isAssign);
        }else if (condition == 3){//已填写未未下载
            idList = statisticsService.notDownloadList(area, isAssign);
        }else if (condition == 4){//已完成未上传文件
            idList = statisticsService.notUploadList(area, isAssign);
        }
        if (!ListUtils.isEmpty(idList)){
            assignTasksService.AssignTasksToCunsus(idList,censusTakerId,type,managerId);
        }
        return  CECResultUtil.success("操作成功");
    }
}
