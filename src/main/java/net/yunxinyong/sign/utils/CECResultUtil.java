package net.yunxinyong.sign.utils;


import com.github.pagehelper.PageInfo;
import net.yunxinyong.sign.constants.CommonConst;
import net.yunxinyong.sign.enums.ResponseStatus;
import net.yunxinyong.sign.vo.PageResult;
import net.yunxinyong.sign.vo.ResponseVo;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 接口返回工具类，支持ModelAndView、ResponseVO、PageResult
 */
public class CECResultUtil {

    public static ModelAndView view(String view) {
        return new ModelAndView(view);
    }

    public static ModelAndView view(String view, Map<String, Object> model) {
        return new ModelAndView(view, model);
    }

    public static ModelAndView redirect(String view) {
        return new ModelAndView("redirect:" + view);
    }

    public static ResponseVo error(int code, String message) {
        return vo(code, message, null);
    }

    public static ResponseVo error(ResponseStatus status) {
        return vo(status.getCode(), status.getMessage(), null);
    }

    public static ResponseVo error(String message) {
        return vo(CommonConst.DEFAULT_ERROR_CODE, message, null);
    }

    public static ResponseVo unLogin(String message) {
        return vo(CommonConst.UN_LOGIN, message, null);
    }
    public static ResponseVo unLogin(String message, Object data) {
        return vo(CommonConst.UN_LOGIN, message, data);
    }

    public static ResponseVo unAuth(String message) {
        return vo(CommonConst.UN_AUTH, message, null);
    }
    public static ResponseVo unAuth(String message, Object data) {
        return vo(CommonConst.UN_AUTH, message, data);
    }

    public static ResponseVo success(String message, Object data) {
        return vo(CommonConst.DEFAULT_SUCCESS_CODE, message, data);
    }

    public static ResponseVo success(String message) {
        return success(message, null);
    }

    public static ResponseVo success(ResponseStatus status) {
        return vo(status.getCode(), status.getMessage(), null);
    }

    public static ResponseVo vo(int code, String message, Object data) {
        return new ResponseVo<>(code, message, data);
    }

    public static PageResult tablePage(Long total, List<?> list) {
        return new PageResult(total, list);
    }

    public static PageResult tablePage(PageInfo info) {
        if (info == null) {
            return new PageResult(0L, new ArrayList());
        }
        return tablePage(info.getTotal(), info.getList());
    }

}
