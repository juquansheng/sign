package net.yunxinyong.sign.utils;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.security.KeyStore;

/**
 * HTTP 工具类
 */
public class CECHTTPUtils {
    private static final Logger logger = Logger.getLogger(CECHTTPUtils.class);

    /**
     * Content-Type: text/xml
     */
    public static final String CONTENTTYPEXML = "text/xml";
    /**
     * Content-Type: application/json
     */
    public static final String CONTENTTYPEJSON = "application/json";

    private final static int CONNECT_TIMEOUT = 5000; // in milliseconds
    private final static String DEFAULT_ENCODING = "UTF-8";

    public static String WXPostData(String urlStr, String data) {
        return WXPostData(urlStr, data, null);
    }

    public static String WXPostData(String urlStr, String data, String contentType) {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlStr);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setConnectTimeout(CONNECT_TIMEOUT);
            conn.setReadTimeout(CONNECT_TIMEOUT);
            if (contentType != null)
                conn.setRequestProperty("content-type", contentType);
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), DEFAULT_ENCODING);
            if (data == null)
                data = "";
            writer.write(data);
            writer.flush();
            writer.close();

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), DEFAULT_ENCODING));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\r\n");
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
            }
        }
        return null;
    }

    /**
     * HTTP Get 方法请求URL
     *
     * @param url URL
     * @return Response字符串
     * @throws IOException
     */
    public static String getHttp(String url, String charset) throws IOException {
        String responseMsg = "";

        // 创建HTTP客户端对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // 创建HTTP Get方法对象
        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader("Content-Type", "application/json;charset=" + charset);

        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
            responseMsg = EntityUtils.toString(response.getEntity(), Charset.forName(charset));
            System.out.println(responseMsg);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (response != null)
                response.close();
            httpClient.close();
        }

        return responseMsg;
    }

    /**
     * POST方式访问 HTTP服务
     *
     * @param url         URL
     * @param postString  提交的内容
     * @param contenttype HTTP Content-Type 参数
     * @return HTTP返回内容
     */
    public static String postHttp(String url, String postString, String contenttype, String charset) {
        logger.info("post request url:" + url + ",para:" + postString);
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(url);
        String result = "";
        try {
            StringEntity stringEntity = new StringEntity(postString, Charset.forName(charset));
            stringEntity.setContentEncoding("UTF-8");
            Header header = new BasicHeader("Content-Type", contenttype);
            stringEntity.setContentType(header);
            post.setEntity(stringEntity);
            HttpResponse res = client.execute(post);
            if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = res.getEntity();
                result = EntityUtils.toString(entity, Charset.forName(charset));// 返回，字符串
            }
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        logger.info("post response:" + result);
        return result;
    }

    /**
     * 获取完整URL行，含参数
     *
     * @param request
     * @return 完整URL行
     */
    public static String getFullURLString(HttpServletRequest request, Boolean needURLEncoding) {
        String resultURL = "";
        try {
            String fullURLString = request.getRequestURL().toString();
            String queryString = request.getQueryString();
            if (queryString != null) {
                fullURLString += "?" + queryString;
            }

            if (needURLEncoding) {
                resultURL = URLEncoder.encode(fullURLString, "UTF-8");
            } else {
                resultURL = fullURLString;
            }
        } catch (Exception e) {
            e.getSuppressed();
        }

        return resultURL;
    }

    public static String postHttpSSL(String url, String postString, String contenttype, String charset) {

        String result = "";
        CloseableHttpClient client = null;
        String certKey = "1444656202"; // 初始是商户ID

        try {
            // 指定读取证书格式为PKCS12
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            // 读取本机存放的PKCS12证书文件
            FileInputStream instream = new FileInputStream(new File("/ycjcert/cert/apiclient_cert.p12"));
            try {
                // 指定PKCS12的密码(初始是商户ID)
                keyStore.load(instream, certKey.toCharArray());
            } finally {
                instream.close();
            }
            SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, certKey.toCharArray()).build();
            // 指定TLS版本
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[]{"TLSv1"},
                    null, SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
            // 设置httpclient的SSLSocketFactory
            client = HttpClients.custom().setSSLSocketFactory(sslsf).build();

            HttpPost post = new HttpPost(url);

            StringEntity stringEntity = new StringEntity(postString, Charset.forName(charset));
            stringEntity.setContentEncoding("UTF-8");
            Header header = new BasicHeader("Content-Type", contenttype);
            stringEntity.setContentType(header);
            post.setEntity(stringEntity);
            HttpResponse res = client.execute(post);
            if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = res.getEntity();
                result = EntityUtils.toString(entity, Charset.forName(charset));// 返回，字符串
            }
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        logger.info("post response:" + result);
        return result;
    }

    /**
     * 获取客户端IP地址
     *
     * @param request
     * @return
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
            if (ip.equals("127.0.0.1")) {
                // 根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ip = inet.getHostAddress();
            }
        }
        // 多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ip != null && ip.length() > 15) {
            if (ip.indexOf(",") > 0) {
                ip = ip.substring(0, ip.indexOf(","));
            }
        }
        return ip;
    }



}
