package net.yunxinyong.sign.utils;

import net.yunxinyong.sign.aop.CECException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.*;
import java.net.SocketException;

public class FTPUtils {
    /*private static final String FTP_HOST = "123.57.35.105";
    private static final String FTP_USER_NAME = "Administrator";
    private static final String FTP_PASSWORD =  "bbcF789&*(";*/
    private static final String FTP_HOST = "192.168.0.177";
    private static final String FTP_USER_NAME = "ttbf";
    private static final String FTP_PASSWORD =  "3331209";
    private static final int FTP_PORT = 21;
    //* @param ftpPath  FTP服务器中文件所在路径 格式： ftptest/aa
    private static final String FTP_PATH = "F:\\ftp\\";

    /**
     * 获取FTPClient对象
     * @return
     */
    public static FTPClient getFTPClient() {
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient = new FTPClient();
            ftpClient.connect(FTP_HOST, FTP_PORT);// 连接FTP服务器
            ftpClient.login(FTP_USER_NAME, FTP_PASSWORD);// 登陆FTP服务器
            if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
                System.out.println("未连接到FTP，用户名或密码错误。");
                ftpClient.disconnect();
            } else {
                System.out.println("FTP连接成功。");
            }
        } catch (SocketException e) {
            e.printStackTrace();
            System.out.println("FTP的IP地址可能错误，请正确配置。");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("FTP的端口错误,请正确配置。");
        }
        return ftpClient;
    }

    /**
     * 从FTP服务器获取文件流
     * @param fileName 文件名称
     */
    public static InputStream getInputStream(String fileName) {

        FTPClient ftpClient = null;

        try {
            ftpClient = getFTPClient();
            ftpClient.setControlEncoding("UTF-8"); // 中文支持
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftpClient.enterLocalPassiveMode();
            ftpClient.changeWorkingDirectory(FTP_PATH);

            //获取文件流
            InputStream in = ftpClient.retrieveFileStream(fileName);
            ftpClient.logout();
            return in;

        } catch (FileNotFoundException e) {
            System.out.println("没有找到" + FTP_PATH + "文件");
            e.printStackTrace();
            return null;
        } catch (SocketException e) {
            System.out.println("连接FTP失败.");
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("文件读取错误。");
            e.printStackTrace();
            return null;
        }

    }
    /**
     * 从FTP服务器下载文件
     * @param localPath 下载到本地的位置 格式：H:/download
     * @param fileName 文件名称
     */
    public static void downloadFtpFile( String localPath,
                                       String fileName) {

        FTPClient ftpClient = null;

        try {
            ftpClient = getFTPClient();
            ftpClient.setControlEncoding("UTF-8"); // 中文支持
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftpClient.enterLocalPassiveMode();
            ftpClient.changeWorkingDirectory(FTP_PATH);

            //获取文件流
            InputStream in = ftpClient.retrieveFileStream(fileName);

            File localFile = new File(localPath + File.separatorChar + fileName);
            OutputStream os = new FileOutputStream(localFile);
            ftpClient.retrieveFile(fileName, os);
            os.close();
            ftpClient.logout();

        } catch (FileNotFoundException e) {
            System.out.println("没有找到" + FTP_PATH + "文件");
            e.printStackTrace();
        } catch (SocketException e) {
            System.out.println("连接FTP失败.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("文件读取错误。");
            e.printStackTrace();
        }

    }

    /**
     * Description: 向FTP服务器上传文件
     * @param fileName ftp文件名称
     * @param input 文件流
     * @return 成功返回true，否则返回false
     */
    public static boolean uploadFile(String fileName,InputStream input) {
        boolean success = false;
        FTPClient ftpClient = null;
        try {
            int reply;
            ftpClient = getFTPClient();
            reply = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftpClient.disconnect();
                return success;
            }

            ftpClient.setControlEncoding("UTF-8"); // 中文支持
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftpClient.enterLocalPassiveMode();
            ftpClient.changeWorkingDirectory(FTP_PATH);

            boolean storeFile = ftpClient.storeFile(fileName, input);

            input.close();
            ftpClient.logout();
            success = storeFile;
            if (!success){
                throw  new CECException(504,"上传失败");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (IOException ioe) {
                }
            }
        }
        return success;
    }
}
