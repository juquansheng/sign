package net.yunxinyong.sign.utils;


import lombok.extern.slf4j.Slf4j;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;
import javax.xml.rpc.ParameterMode;

import javax.xml.namespace.QName;
import java.net.URL;


public class ZHYXYAPI {

    /**
     * 接口 UID
     */
    public static String ZHYXYUID = "HHJASSUO6KTL6NSX";
    /**
     * 接口 密码
     */
    public static String ZHYXYPWD = "12345678@hx";

    /**
     * 接口 KEYTYPE - 企业名称
     */
    public static String ZHYXYKEYTYPECOMPANYNAME = "2";
    /**
     * 接口 KEYTYPE - 工商注册号
     */
    public static String ZHYXYKEYTYPEREGNO = "3";
    /**
     * 接口 KEYTYPE - 自然人证件号
     */
    public static String ZHYXYKEYTYPEPERSON = "4";

    /**
     * 企业详细信息查询
     *
     * @param uid
     *            唯一标识（之前用的）
     * @param pwd
     *            密码
     * @param key
     *            查询关键字
     * @param keytype
     *            关键字类型（2-企业名称，3-注册号）
     * @return 查询接口返回数据（XML）
     */
    public static String getSaic(String key) {
        // API URL, 来自eiss源代码
//		String wsdlUrl = "http://183.166.59.107/services/NewEntInfoService?wsdl";
        String wsdlUrl = "http://60.166.20.133:1080/services/NewEntInfoService?wsdl";
        // 逆追溯IP地址得到的域名：（可能对吧）
        // String wsdlUrl =
        // "http://www.zhyxy.net/services/NewEntInfoService?wsdl";

        // 以下都是抄来的，到底是个啥意思？？？
        try {
            // 创建 axis 类
            Service service = new Service();
            // 创建URL对象
            URL url = new URL(wsdlUrl);
            // 创建CALL调用对象
            Call call = (Call) service.createCall();

            // 设置请求目标端点地址
            call.setTargetEndpointAddress(url);

            // 设置调用方法名
            call.setOperationName(new QName("NewEntInfoService", "getSaic"));
            // NewEntInfoService getSaicSingle
            // 设置方法的参数名、参数类型、参数模式
            call.addParameter("uid", XMLType.XSD_STRING, ParameterMode.IN);
            // 设置方法的参数名、参数类型、参数模式
            call.addParameter("pwd", XMLType.XSD_STRING, ParameterMode.IN);
            // 设置方法的参数名、参数类型、参数模式
            call.addParameter("key", XMLType.XSD_STRING, ParameterMode.IN);
            // 设置方法的参数名、参数类型、参数模式
            call.addParameter("keytype", XMLType.XSD_STRING, ParameterMode.IN);
            // 设置方法的返回类型
            call.setReturnType(XMLType.XSD_STRING);

            // 调用服务方法
            String result = (String) call.invoke(new Object[] { "HHJASSUO6KTL6NSX", "12345678@hx", key, "2" });
            System.out.println(result);

            System.out.println("key = " + key + ", 调用 ZHYXY 接口，返回：");
            System.out.println(result);
            // 暂时在这儿返回
            return result;
        } catch (Exception e) {
            // 使用记录
            String keywordrecord = key + "|" + "2";
            // apiRecordService.addApiRecord("ZHYXY", "getSaic", keywordrecord,
            // "-1");
            e.printStackTrace();

        }

        return null;
    }
}
