package net.yunxinyong.sign.utils;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import java.util.List;

public class ListUtils {

    public static boolean isEmpty(List list) {
        return list == null || list.size() == 0;
    }

    /**
     * 把integer 类型的集合转化为long类型的集合
     * @param list
     * @return
     */
    public static List<Long> integerToLong(List<Integer> list){
        List<Long> longList = Lists.transform(list, new Function<Integer, Long>() {
            @Nullable
            @Override
            public Long apply(@Nullable Integer integer) {
                return (long) integer;
            }
        });
        return longList;
    }
}
