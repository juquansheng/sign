package net.yunxinyong.sign.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import net.yunxinyong.sign.entity.EcnomicInventory;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public class EcnomicReqVo extends EcnomicInventory {
    //开始时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    //结束时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    //企业状态id列表
    private List<Integer> statusIdList;
    //0未分配  1已分配
    private Integer isAssign;
    /**
     * 通知
     */
    private Integer inform;
    /**
     * 注册
     */
    private Integer register;
    /**
     * 提交
     */
    private Integer submit;
    /**
     * 上传
     */
    private Integer uploaded;
    /**
     * 普查员的名字
     */
    private String cunsusTakerName;

    public String getCunsusTakerName() {
        return cunsusTakerName;
    }

    public void setCunsusTakerName(String cunsusTakerName) {
        this.cunsusTakerName = cunsusTakerName;
    }

    public List<Integer> getStatusIdList() {
        return statusIdList;
    }

    public void setStatusIdList(List<Integer> statusIdList) {
        this.statusIdList = statusIdList;
    }

    public Integer getIsAssign() {
        return isAssign;
    }

    public void setIsAssign(Integer isAssign) {
        this.isAssign = isAssign;
    }


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public Integer getInform() {
        return inform;
    }

    public void setInform(Integer inform) {
        this.inform = inform;
    }

    public Integer getUploaded() {
        return uploaded;
    }

    public void setUploaded(Integer uploaded) {
        this.uploaded = uploaded;
    }

    public Integer getRegister() {
        return register;
    }

    public void setRegister(Integer register) {
        this.register = register;
    }

    public Integer getSubmit() {
        return submit;
    }

    public void setSubmit(Integer submit) {
        this.submit = submit;
    }
}
