package net.yunxinyong.sign.vo;

public class PhoneAndStatusVo {
    //phone表中的主键id
    private int phoneId;
    //电话号
    private String phone;
    //企业id（清查表）
    private int invId;
    //状态表中的id主项
    private int statusId;
    //状态表中的id分项
    private int valueId;
    //电话备注
    private String remark;
    //电话类型
    private int Phonetype;
    //菜单id
    private int parentId;
    //状态菜单名称
    private String statusName;
    //状态值
    private String statusValue;
    //类型
    private int statusType;
    //创建者id
    private int adminId;

    public int getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(int phoneId) {
        this.phoneId = phoneId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getInvId() {
        return invId;
    }

    public void setInvId(int invId) {
        this.invId = invId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getValueId() {
        return valueId;
    }

    public void setValueId(int valueId) {
        this.valueId = valueId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getPhonetype() {
        return Phonetype;
    }

    public void setPhonetype(int phonetype) {
        Phonetype = phonetype;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public int getStatusType() {
        return statusType;
    }

    public void setStatusType(int statusType) {
        this.statusType = statusType;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }
}
