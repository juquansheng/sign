package net.yunxinyong.sign.vo;

public class InventoryVo {
    private Long id;
    private String unitDetailedName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnitDetailedName() {
        return unitDetailedName;
    }

    public void setUnitDetailedName(String unitDetailedName) {
        this.unitDetailedName = unitDetailedName;
    }
}
