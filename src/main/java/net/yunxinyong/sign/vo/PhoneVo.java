package net.yunxinyong.sign.vo;

import net.yunxinyong.sign.entity.Phone;

public class PhoneVo extends Phone {
    private String valueName;

    public String getValueName() {
        return valueName;
    }

    public void setValueName(String valueName) {
        this.valueName = valueName;
    }
}
