package net.yunxinyong.sign.vo;

import net.yunxinyong.sign.entity.EcnomicInventory;

public class ProgressVo extends EcnomicInventory {
    private double pojTotalProgress;
    private Integer state;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPojTotalProgress() {
        return pojTotalProgress;
    }

    public void setPojTotalProgress(double pojTotalProgress) {
        this.pojTotalProgress = pojTotalProgress;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
