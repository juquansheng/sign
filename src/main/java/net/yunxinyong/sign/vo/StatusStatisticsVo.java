package net.yunxinyong.sign.vo;

public class StatusStatisticsVo {

    //企业总数
    public int total;
    //当天新增企业
    public int newUnit;
    //已通知
    public int informed;
    //已注册
    public int registered;
    //已完成
    public int completed;
    //已录入
    public int entered;
    //已上传
    public int uploaded;
    public int getNewUnit() {
        return newUnit;
    }

    public void setNewUnit(int newUnit) {
        this.newUnit = newUnit;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getInformed() {
        return informed;
    }

    public void setInformed(int informed) {
        this.informed = informed;
    }

    public int getRegistered() {
        return registered;
    }

    public void setRegistered(int registered) {
        this.registered = registered;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public int getEntered() {
        return entered;
    }

    public void setEntered(int entered) {
        this.entered = entered;
    }

    public int getUploaded() {
        return uploaded;
    }

    public void setUploaded(int uploaded) {
        this.uploaded = uploaded;
    }
}
