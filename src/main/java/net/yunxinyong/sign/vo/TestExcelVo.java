package net.yunxinyong.sign.vo;

import net.yunxinyong.sign.utils.ExcelAnno;

public class TestExcelVo {

    @ExcelAnno(head = "名称")
    private String name;
    @ExcelAnno(head = "编号")
    private String menber;
    @ExcelAnno(head = "机构代码")
    private String cede;
    @ExcelAnno(head = "信用代码")
    private String socialCode;
    @ExcelAnno(head = "地址")
    private String address;
    @ExcelAnno(head = "更新时间")
    private String updateTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMenber() {
        return menber;
    }

    public void setMenber(String menber) {
        this.menber = menber;
    }

    public String getCede() {
        return cede;
    }

    public void setCede(String cede) {
        this.cede = cede;
    }

    public String getSocialCode() {
        return socialCode;
    }

    public void setSocialCode(String socialCode) {
        this.socialCode = socialCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
