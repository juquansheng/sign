package net.yunxinyong.sign.constants;

/**
 *@describe
 *@author  ttbf
 *@date  2018/8/15
 */
public class CommonConst {
    /**
     * 安全密码，作为盐值用于用户密码的加密
     */
    public static final String SECURITY_KEY = "register";

    /**
     * 程序默认的错误状态码
     */
    public static final int DEFAULT_ERROR_CODE = 500;

    /**
     * 程序默认的成功状态码
     */
    public static final int DEFAULT_SUCCESS_CODE = 200;

    /**
     * 程序未登录
     */
    public static final int UN_LOGIN = 401;

    /**
     * 操作未授权
     */
    public static final int UN_AUTH = 401;

    /**
     * 时间格式
     */
    public static final String TIME_12 = "yyyy-MM-dd hh:mm:ss";
    public static final String TIME_24 = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_DAY = "yyyy-MM-dd";

}
