package net.yunxinyong.sign.aop;


import net.yunxinyong.sign.utils.CECResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class CECExceptionHandler {

    @ExceptionHandler(value = CECException.class)
    @ResponseBody
    public CECResult exceptionHandler(CECException e) throws Exception {
        CECResult tjbResult = new CECResult();
        tjbResult.setMessage(e.getMessage());
        tjbResult.setStatus(e.getStatus());
        tjbResult.setData("");
        return tjbResult;
    }
}